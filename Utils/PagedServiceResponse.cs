using Transport.WEB.Models;

namespace Transport.WEB.Utils
{
    internal class PagedServiceResponse<TResponse> : ServiceResponse<PagedResponse<TResponse>>, IPagedServiceResponse<TResponse>
    { }
}