﻿namespace Transport.WEB.Utils
{
    public class Constants
    {
        public const string ClientNoToken = "webapi_no_token";
        public const string ClientWithToken = "webapi_with_token";
        public const string HumanDateFormat = "dd MMMM, yyyy";
        public const string SystemDateFormat = "dd/MM/yyyy";

        public class Keys
        {
            public const string JwtBearerSection = "Authentication:JwtBearer";
            public const string ApiBaseUrl = "App:ApiBaseUrl";
            public const string Alerts = "_Alerts";
        }

        public class URL
        {
            public const string Default = "/home/index";
            public const string LoginPath = "/account/login";
            public const string AccessDeniedPath = "/error/accessdenied";
        }

        public class ClientRoutes
        {
            public const string Token = "/api/token";
            public const string RefreshToken = "/api/token/refreshtoken";

            public const string AccountGetClaims = "/api/account/getcurrentuserclaims";
            public const string AccountGetProfile = "/api/account/getprofile";
            public const string AccountUpdateProfile = "/api/account/updateprofile";
            public const string AccountChangePassword = "/api/account/changepassword";
            public const string AccountResetPassword = "/api/account/ForgotPassword";
            public const string AccountResetNewPassword = "/api/account/ResetPassword";          

            public const string Drivers = "/api/driver/get";
            public const string Driver = "/api/driver";
            public const string DriverGet = "/api/driver/get/{0}";
            public const string DriverDelete = "/api/driver/delete/{0}";
            public const string DriverCreate = "/api/driver/add";
            public const string DriverUpdate = "/api/driver/update/{0}";
            public const string GetDriverByVehicleReg = "/api/driver/GetDriverbyVehicleRegistrationNumber";
            public const string GetAvailableDriverAsync = "api/driver/getavailabledriverasync";
            public const string GetDriverByVehicleId = "api/driver/getadriverbyvehicleId/{0}";
            public const string UpdateDriverWallet = "/api/driver/updateDriverWallet/{0}/{1}";
            public const string PayDriver = "/api/driver/paydriver";
            public const string GetDriverWalletTransactionById = "/api/driver/getDriverWalletTransactionById/{0}";
            public const string UpdateDriverWalletTransaction = "/api/driver/updateDriverWalletTransaction/{0}";
            public const string UpdateDriverPayment = "/api/driver/updateDriverPayment/{0}";
            public const string RemovePayment = "/api/driver/removePayment/{0}";
            public const string DriverDetails = "/api/driver/DriverDetails";
            public const string PilotApproval = "/api/driver/PilotApproval";
            public const string GetPilotTransactionDetail = "/api/driver/getPilotTransactionDetail";
            public const string VerifyPilotTransaction = "/api/driver/verifyPilotTransaction/{0}";
            public const string VerifyAllPilotTransaction = "/api/driver/verifyAllPilotTransaction";
            public const string ApprovePilotTransaction = "/api/driver/approvePilotTransaction/{0}";
            public const string ApproveAllPilotTransaction = "/api/driver/approveAllPilotTransaction";
            public const string RevertToCapturedTransaction = "/api/driver/RevertToCapturedTransaction/{0}";
            public const string RevertToUncapturedTransaction = "/api/driver/RevertToUncapturedTransaction/{0}";    
            public const string GetDeductedWalletBalance = "/api/driver/GetDeductedWalletBalance/{0}";
            public const string ResetWalletBalance = "/api/driver/ResetWalletBalance/{0}/{1}";
            public const string GetPilotApproval = "/api/driver/pilotapprovalreport";
            public const string GetPilotApprovalDetail = "/api/driver/getPilotApprovalDetails";
            public const string GetPilotsCapturedPayment = "/api/driver/GetPilotsCapturedPayment";


            public const string Employees = "/api/employee/get";
            public const string Employee = "/api/employee";
            public const string EmployeeGet = "/api/employee/get/{0}";
            public const string EmployeeDelete = "/api/employee/delete/{0}";
            public const string EmployeeCreate = "/api/employee/add";
            public const string EmployeeUpdate = "/api/employee/update/{0}";
            public const string GetEmployeeTerminalId = "/api/employee/getemployeeterminal";
            public const string activateaccount = "/api/employee/activateaccount";

            public const string Roles = "/api/role/get";
            public const string RoleGet = "/api/role/get/{0}";
            public const string RoleDelete = "/api/role/delete/{0}";
            public const string RoleCreate = "/api/role/add";
            public const string RoleUpdate = "/api/role/update/{0}";

            public const string Route = "/api/route";
            public const string Routes = "/api/route/get";
            public const string RouteGet = "/api/route/get/{0}";
            public const string RouteDelete = "/api/route/delete/{0}";
            public const string RouteCreate = "/api/route/add";
            public const string RouteUpdate = "/api/route/update/{0}";
            public const string RouteGetDestinationTerminals = "/api/route/getdestinationterminals/{0}";
            public const string RoutesGetByTerminalId = "/api/route/terminals/routes/{0}";

            public const string SubRoutes = "/api/subroute/get";
            public const string SubRouteGet = "/api/subroute/get/{0}";
            public const string SubRouteDelete = "/api/subroute/delete/{0}";
            public const string SubRouteCreate = "/api/subroute/add";
            public const string SubRouteUpdate = "/api/subroute/update/{0}";

            public const string Manifests = "/api/Manifest";
            public const string OpenManifest = "/api/Manifest/OpenManifest";
            public const string UpdateManifest = "/api/Manifest/GetByVehicleTripId/{0}";
            public const string UpdateManifestAmount = "/api/Manifest/UpdateFare/{0}";
            public const string GetVehicleDetails = "/api/Manifest/getvehicletrip/{0}";

            public const string Terminals = "/api/terminal/get";
            public const string TerminalRoutes = "/api/terminal/getroutes/{0}";
            public const string TerminalGet = "/api/terminal/get/{0}";
            public const string TerminalDelete = "/api/terminal/delete/{0}";
            public const string TerminalCreate = "/api/terminal/add";
            public const string TerminalUpdate = "/api/terminal/update/{0}";
            public const string StaffRoutes = "/api/terminal/getstaffterminalroutes";
            public const string LoginInTerminal = "/api/terminal/getloginemployeeterminal";
            public const string GetVirtualAndPhysicalTerminals = "/api/terminal/getVirtualAndPhysicalTerminals";


            public const string Trips = "/api/trip/get";
            public const string TripGet = "/api/trip/get/{0}";
            public const string TripDelete = "/api/trip/delete/{0}";
            public const string TripCreate = "/api/trip/add";
            public const string TripUpdate = "/api/trip/update/{0}";
            public const string RouteTrips = "/api/trip/getbyrouteid";
            public const string RouteBuses = "/api/route/route/physicalbuses";

            public const string Subroute = "/api/subroute/";

            public const string Regions = "/api/region/get";
            public const string RegionGet = "/api/region/get/{0}";
            public const string RegionDelete = "/api/region/delete/{0}";
            public const string RegionCreate = "/api/region/add";
            public const string RegionUpdate = "/api/region/update/{0}";

            public const string States = "/api/state/get";
            public const string StateGet = "/api/state/get/{0}";
            public const string StateDelete = "/api/state/delete/{0}";
            public const string StateCreate = "/api/state/add";
            public const string StateUpdate = "/api/state/update/{0}";

            public const string Discount = "/api/discount";
            public const string Discounts = "/api/discount/get";
            public const string DiscountGet = "/api/discount/get/{0}";
            public const string DiscountDelete = "/api/discount/delete/{0}";
            public const string DiscountCreate = "/api/discount/add";
            public const string DiscountUpdate = "/api/discount/update/{0}";


            public const string FareCalendar = "/api/farecalendar";
            public const string FareCalendars = "/api/farecalendar/get";
            public const string FareCalendarGet = "/api/farecalendar/get/{0}";
            public const string FareCalendarDelete = "/api/farecalendar/delete/{0}";
            public const string FareCalendarCreate = "/api/farecalendar/add";
            public const string FareCalendarUpdate = "/api/farecalendar/update/{0}";


            public const string Fares = "/api/fare/get";
            public const string FareGet = "/api/fare/get/{0}";
            public const string FareDelete = "/api/fare/delete/{0}";
            public const string FareCreate = "/api/fare/add";
            public const string FareUpdate = "/api/fare/update/{0}";
            public const string FareByRouteIdAndModelId = "/api/fare/GetFareByRouteId/{0}";

            public const string Booking = "api/booking";
            public const string BookingDetails = "api/booking/alldetails/{0}";
            public const string Bookings = "api/booking/search";
            public const string PostBookings = "api/booking/postbooking";
            public const string UpdateBookings = "api/booking/updatebooking";
            public const string GetBookingDetails = "api/booking/details/{0}";
            public const string RescheduleTicketSearch = "api/booking/rescheduleticketsearch";
            public const string RescheduleBooking = "/api/Booking/RescheduleBooking";
            public const string GetSeatsAvailable = "api/booking/getseatsavailable/{0}";
            public const string GetTripHistory = "/api/booking/gettriphistory/{0}";
            public const string GetTraveledCustomers = "/api/booking/GetTraveledCustomers";
            public const string GetAllTraveledCustomers = "/api/booking/GetAllTraveledCustomers";
            public const string GetNewRouteFareByModel = "/api/booking/GetNewRoutefare/{0}/{1}";
            public const string GetNonIdAmount = "/api/booking/GetNewRoutefare/{0}/{1}";
            public const string CancelSeat = "/api/booking/cancelticket/{0}";
            
            public const string SeatManagement = "api/seatmanagement";

            public const string VehicleModels = "/api/vehiclemodel/get";
            public const string VehicleModelGet = "/api/vehiclemodel/get/{0}";
            public const string VehicleModelDelete = "/api/vehiclemodel/delete/{0}";
            public const string VehicleModelCreate = "/api/vehiclemodel/add";
            public const string VehicleModelUpdate = "/api/vehiclemodel/update/{0}";

            public const string VehicleMakes = "/api/vehiclemake/get";
            public const string VehicleMakeGet = "/api/vehiclemake/get/{0}";
            public const string VehicleMakeDelete = "/api/vehiclemake/delete/{0}";
            public const string VehicleMakeCreate = "/api/vehiclemake/add";
            public const string VehicleMakeUpdate = "/api/vehiclemake/update/{0}";

            //add available vehicle
            public const string Vehicles = "/api/vehicle/get";
            public const string Vehicle = "/api/vehicle";
            public const string GetVehicleByIdWithDriverName = "/api/vehicle/getVehicleByIdWithDriverName/{0}";
            public const string VehicleGet = "/api/vehicle/get/{0}";
            public const string VehicleDelete = "/api/vehicle/delete/{0}";
            public const string VehicleCreate = "/api/vehicle/add";
            public const string VehicleUpdate = "/api/vehicle/update/{0}";
            public const string VehiclesInTerminal = "/api/vehicle/getavailablevehiclesinterminal";
            public const string VehiclesByTerminal = "/api/vehicle/GetAvailableVehiclesByTerminal/{0}";
            public const string TerminalToAllocate = "/api/vehicle/GetTerminalHeader";
            public const string VechicleToAllocate = "/api/vehicle";
            public const string Vehiclesdropdown = "/api/vehicle/GetVehicleslist";
            public const string Allocatebuses = "/api/vehicle/allocatebuses";
            public const string VehicleInTerminal = "/api/Vehicle/GetVehiclesByTerminalHeader";
            public const string VehicleByRegNum = "/api/Vehicle/GetByRegNumber/{0}";
            public const string AvailableVehicles = "/api/vehicle/getavailablevehicles";
            public const string UnusedInTerminals = "/api/vehicle/getterminalremainingvehicle/{0}";
            public const string VehicleAllocationConfirm = "/api/vehicle/confirm/{0}";
            public const string DeleteFromVehicleAllocationConfirm = "/api/vehicle/deletefromvehicleallocationtable/{0}";
            public const string GetVehicleByStatus = "/api/vehicle/vehicleByStatus";
            public const string GetVehiclesInWorkshop = "/api/vehicle/GetVehiclesInWorkshop";
            public const string UpdateVehicleStatus = "/api/vehicle/UpdateVehicleStatus/{0}/{1}";

            public const string VehicleTripRegistration = "/api/vehicletripregistration";
            public const string VehicletripsCreateBus = "/api/vehicletripregistration/createphysicalbus";
            public const string UpdateVehicleTripId = "/api/vehicletripregistration/UpdateVehicleTripId";
            public const string RouteIdByDestinationAndTerminalId = "api/route/GetRouteIdByDestinationAndDepartureId/{0}/{1}";
            public const string CreateBlow = "/api/vehicletripregistration/createBlow";
            public const string GetBlownBuses = "/api/vehicletripregistration/getblown";

            public const string Feedbacks = "/api/Feedback/get";
            public const string Feedback = "/api/Feedback";
            public const string FeedbackGet = "/api/Feedback/get/{0}";
            public const string UpdateFeedback = "/api/Feedback/update/{0}";


            //Reports
            public const string BookingReports = "/api/report/search";
            public const string BookingSales = "/api/report/bookings";
            public const string AdvancedBookingSales = "/api/report/advancedbookings";
            public const string BookingByChannelReports = "/api/report/channelsalessummary";
            public const string BookedTrips = "/api/report/bookedtrips";
            public const string BookedSales = "/api/report/bookedsales";
            public const string PassengerReport = "/api/report/passengerReport";
            public const string SalesPerBus = "/api/report/salesbybus";
            public const string DriverSalary = "/api/report/driversalary";
            public const string HiredTrips = "/api/report/hiretrips";
            public const string JourneyCharts = "/api/report/journeyCharts"; 
            public const string JourneyChartsForStateTerminals = "/api/report/journeychartsforstateterminals";
            public const string journeyChartsForTerminals = "/api/report/journeyChartsForTerminals";
            public const string journeyChartsDetailsByState = "/api/report/journeyChartsDetailsByState";
            public const string TotalSalesReportPerState = "/api/report/totalSalesReportPerState";
            public const string TotalTerminalSalesInState = "/api/report/totalTerminalSalesInState";
            public const string TerminalSalesSummary = "/api/report/terminalSalesSummary";
            public const string TotalRevenueReportPerState = "/api/report/totalRevenueReportPerState";
            public const string TotalTerminalRevenueInState = "/api/report/totalTerminalRevenueInState";
            public const string TerminalRevenueSummary = "/api/report/terminalRevenueSummary";
            public const string PilotPaySlip = "/api/driver/searchpilotpaySlip";

            //GetHireTravelled
            public const string HireTravelled = "/api/report/HireTravelled";

            //GetHireTravelledPass
            public const string HireTravelledPass = "/api/report/GetAllHirePassengers/{0}";

            //dashboard reports
            public const string SalesSummaryUrl = "/api/report/getsalessummary";
            public const string BookingSummaryUrl = "/api/report/getbookingsummary";

            //trip management
            public const string InboundTrips = "/api/journey/JourneysIncoming";
            public const string OutboundTrips = "/api/journey/JourneysOutgoing";
            public const string ReceiveTrips = "/api/journey/receive";
            public const string ApproveTrips = "/api/journey/approve";
            public const string BlowJourneys = "/api/Journey/blowjourneys";


            //manifest management
            public const string ManifestByVehicleTrip = "/api/vehicletripregistration/GetByVehicleTripId";
            public const string ManifestByVehicleTripWithoutDispatch = "/api/vehicletripregistration/GetByVehicleTripIdWithoutManifest";

            //Manifest expensis
            
            //public const string GetTripExpenses = "/api/manifest/GetManifestExpenses";
            //public const string GetTripExpenses2 = "/api/manifest/GetTripExpenses2";
            public const string GetManifestExpenses = "/api/manifest/GetManifestExpenses";
            public const string GetManifestExpenses2 = "/api/manifest/GetManifestExpenses2";


            //bookings and trips
            public const string GetVehicleTripsByDriverCode = "/api/vehicletripregistration/vehicletripbydrivercode";

            //for franchise            //for franchise
            public const string Franchises = "/api/franchize/get";
            public const string Franchise = "/api/franchize";
            public const string FranchiseGet = "/api/franchize/get/{0}";
            public const string FranchiseDelete = "/api/franchize/delete/{0}";
            public const string FranchiseCreate = "/api/franchize/add";
            public const string FranchiseUpdate = "/api/franchize/update/{0}";

            //for blowing
            //public const string BlowVehicle = "/api/blowtrip/add";
            public const string Passport = "/api/PassportType/GetPassportTypeAsync";
            public const string GetPassportTypeById = "/api/passporttype/getbyrouteandid";
            public const string PassportTypeGet = "/api/passporttype/get/{0}";

            //for PatrolReports and Journey
            public const string AddReport = "/api/mtureport/add";
            public const string SearchReport = "/api/mtureport/getallreport";
            public const string GetMTUById = "/api/mtureport/get/{0}";
            public const string Transload = "/api/Journey/transloadbus";
            public const string GetTransload = "/api/Journey/GetTransloadedVehicles";
            public const string GetJourneyManagementByVehicleTripRegistrationId = "/api/Journey/GetJourneyManagementByVehicleTripRegistrationId/{0}";
            public const string Workshop = "/api/mtureport/workshop";
            //fleet History

            public const string GetFleetHistory = "/api/vehicletripregistration/getfleethistory";

            //Hire
            public const string HireABus = "/api/hirebus";

            //HirePassenger
            public const string HirePassenger = "/api/HirePassenger";
         
            //Agents
            public const string Agents = "api/agents";


            //AgentsTransaction
            public const string AgentTrans = "api/getAgentTransaction";

            //AgentLocation
            public const string AgentLocation = "api/agentlocation";

            //AgentCommission
            public const string AgentCommission = "api/agentcommission";

            //AgentReport
            public const string AgentReport = "api/agentreport";

            //Workshop
            public const string WorkshopBus = "api/workshop";
            public const string WorkshopBusGet = "api/workshop/Get";
            public const string WorkshopBusPostSearchModel = "api/workshop/PostSearchModel";
            public const string WorkshopBusCreate = "api/workshop/add";
            public const string WorkshopBusRelease = "api/workshop/release/{0}/{1}/{2}";

            //General Transaction
            public const string AddGeneralTransaction = "/api/GeneralTransaction/Add";
            public const string GetAllGeneralTransaction = "/api/GeneralTransaction/GetAll";
            public const string GetGeneralTransactionById = "/api/GeneralTransaction/GetTransaction/{0}";
            public const string EditGeneralTransaction = "/api/GeneralTransaction/Edit/{0}";
            public const string DeactivateGeneralTransaction = "/api/GeneralTransaction/Deactivate/{0}";

            //Company Info
            public const string CompanyInfo = "/api/CompanyInfo/get";
            public const string CompanyInfoGet = "/api/CompanyInfo/get/{0}";
            public const string CompanyInfoCreate = "/api/companyinfo/add";
            public const string CompanyInfoUpdate = "/api/CompanyInfo/update/{0}";
            public const string CompanyInfoDelete = "/api/CompanyInfo/Delete/{0}";

            //INVENTORY Items
            public const string InventorySetup = "/api/InventorySetup/GetAllInventoryItems";
            public const string InventorySetupGet = "/api/InventorySetup/GetInventoryItem/{0}";
            public const string InventorySetupCreate = "/api/InventorySetup/AddInventoryItem";
            public const string InventorySetupUpdate = "/api/InventorySetup/UpdateInventoryItem/{0}";
            public const string InventorySetupDelete = "/api/InventorySetup/DeleteInventoryItem/{0}";

            //Inventory Category
            public const string InventorySetupCategory = "/api/InventorySetup/GetAllItemCategories";
            public const string InventorySetupCategoryGet = "/api/InventorySetup/GetItemCategory/{0}";
            public const string InventorySetupCategoryCreate = "/api/InventorySetup/AddItemCategory";
            public const string InventorySetupCategoryUpdate = "/api/InventorySetup/UpdateItemCategory/{0}";
            public const string InventorySetupCategoryDelete = "/api/InventorySetup/DeleteItemCategory/{0}";

            //Inventory Item Type
            public const string InventorySetupItemType = "/api/InventorySetup/GetAllItemTypes";
            public const string InventorySetupItemTypeGet = "/api/InventorySetup/GetItemType/{0}";
            public const string InventorySetupItemTypeCreate = "/api/InventorySetup/AddItemType";
            public const string InventorySetupItemTypeUpdate = "/api/InventorySetup/UpdateItemType/{0}";
            public const string InventorySetupItemTypeDelete = "/api/InventorySetup/DeleteItemType/{0}";

            //Inventory Attributes
            public const string InventorySetupAttributes = "/api/InventorySetup/GetAllAttributes";
            public const string InventorySetupAttributesGet = "/api/InventorySetup/GetAttribute/{0}";
            public const string InventorySetupAttributesCreate = "/api/InventorySetup/AddAttribute";
            public const string InventorySetupAttributesUpdate = "/api/InventorySetup/UpdateAttribute/{0}";
            public const string InventorySetupAttributesDelete = "/api/InventorySetup/DeleteAttribute/{0}";

            //Inventory Family
            public const string InventorySetupFamilies = "/api/InventorySetup/GetAllItemFamilies";
            public const string InventorySetupFamiliesGet = "/api/InventorySetup/GetItemFamily/{0}";
            public const string InventorySetupFamiliesCreate = "/api/InventorySetup/AddItemFamily";
            public const string InventorySetupFamiliesUpdate = "/api/InventorySetup/UpdateItemFamily/{0}";
            public const string InventorySetupFamiliesDelete = "/api/InventorySetup/DeleteItemFamily/{0}";

            //Inventory Warehouse
            public const string InventorySetupWarehouse = "/api/InventorySetup/GetAllWarehouses";
            public const string InventorySetupWarehouseGet = "/api/InventorySetup/GetWarehouse/{0}";
            public const string InventorySetupWarehouseCreate = "/api/InventorySetup/AddWarehouse";
            public const string InventorySetupWarehouseUpdate = "/api/InventorySetup/UpdateWarehouse/{0}";
            public const string InventorySetupWarehouseDelete = "/api/InventorySetup/DeleteWarehouse/{0}";

            //Inventory WarehouseBin
            public const string InventorySetupWarehouseBin = "/api/InventorySetup/GetAllWarehouseBins";
            public const string InventorySetupWarehouseBinGet = "/api/InventorySetup/GetWarehouseBin/{0}";
            public const string InventorySetupWarehouseBinCreate = "/api/InventorySetup/AddWarehouseBin";
            public const string InventorySetupWarehouseBinUpdate = "/api/InventorySetup/UpdateWarehouseBin/{0}";
            public const string InventorySetupWarehouseBinDelete = "/api/InventorySetup/DeleteWarehouseBin/{0}";

            //Inventory AdjustmentType
            public const string InventorySetupAdjustmentType = "/api/InventorySetup/GetAllInventoryAdjustmentTypes";
            public const string InventorySetupAdjustmentTypeGet = "/api/InventorySetup/GetInventoryAdjustmentType/{0}";
            public const string InventorySetupAdjustmentTypeCreate = "/api/InventorySetup/AddInventoryAdjustmentType";
            public const string InventorySetupAdjustmentTypeUpdate = "/api/InventorySetup/UpdateInventoryAdjustmentType/{0}";
            public const string InventorySetupAdjustmentTypeDelete = "/api/InventorySetup/DeleteInventoryAdjustmentType/{0}";

            //Next Number
            public const string InventorySetupNextNumber = "/api/InventorySetup/GetAllNextNumbers";
            public const string InventorySetupNextNumberGet = "/api/InventorySetup/GetNextNumber/{0}";
            public const string InventorySetupNextNumberCreate = "/api/InventorySetup/AddNextNumber";
            public const string InventorySetupNextNumberUpdate = "/api/InventorySetup/UpdateNextNumber/{0}";
            public const string InventorySetupNextNumberDelete = "/api/InventorySetup/DeleteNextNumber/{0}";

            //LedgerChartOfAccounts
            public const string InventorySetupGeneralLedger = "/api/InventorySetup/GetAllLedgerChartOfAccounts";
            public const string InventorySetupGeneralLedgerGet = "/api/InventorySetup/GetLedgerChartOfAccount/{0}";
            public const string InventorySetupGeneralLedgerCreate = "/api/InventorySetup/AddLedgerChartOfAccount";
            public const string InventorySetupGeneralLedgerUpdate = "/api/InventorySetup/UpdateLedgerChartOfAccount/{0}";
            public const string InventorySetupGeneralLedgerrDelete = "/api/InventorySetup/DeleteLedgerChartOfAccount/{0}";
        }

    }
}