﻿using System.Collections.Generic;

namespace Transport.WEB.Utils
{
    public class PagedResponse<T>
    {
        public PagedResponse()
        {
            Items = new List<T>();
        }
        public List<T> Items { get; set; }

        public int Page { get; set; }
        public int Size { get; set; }
        public int Count { get; set; }
        public string SearchTerm { get; set; }
    }
}