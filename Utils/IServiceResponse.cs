using System;

namespace Transport.WEB.Utils
{
    //public interface IServiceResponse<TResponse>
    //{
    //    string Code { get; set; }
    //    string ShortDescription { get; set; }
    //    TResponse Object { get; set; }
    //    bool IsValidModelState { get; }
    //    bool IsValid { get; }
    //}

    public interface IPagedServiceResponse<TResponse>
    {
        string Code { get; set; }
        string ShortDescription { get; set; }
        PagedResponse<TResponse> Object { get; set; }
    }
}