﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace Transport.WEB.Utils.Helpers
{
    public static class MvcUtilities
    {
        public static IEnumerable<SelectListItem> GenerateEnumSelectList(Type enumType)
        {
            return (from object item in Enum.GetValues(enumType)

                    let fi = enumType.GetField(item.ToString())
                    let attribute = fi.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault()
                    let include = fi.GetCustomAttributes(typeof(BrowsableAttribute), true).FirstOrDefault()
                    let title = attribute == null ? item.ToString() : ((DescriptionAttribute)attribute).Description
                    where include == null ? true : ((BrowsableAttribute)include).Browsable ? true : false

                    select new SelectListItem
                    {
                        Value = ((int)(item)).ToString(),
                        Text = title,
                    });
        }
    }
}
