﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace Transport.WEB.Utils
{
    public static class CollectionExtensions
    {
        public static string ToDelimitedString<T>(this IEnumerable<T> items, string delimeter = ",")
        {
            var list = items.Select(item => Convert.ToString(item)).ToList();

            return 
                list.Count <= 1
                ? $"{list.FirstOrDefault()}"
                : list.Aggregate((previous, next) => $"{previous}{delimeter}{next}");
        }

        
        public static string SeperateWords(this string str)
        {

            if (string.IsNullOrWhiteSpace(str))
                return str;

            string output = "";
            char[] chars = str.ToCharArray();

            for (int i = 0; i < chars.Length; i++)
            {
                if (i == chars.Length - 1 || i == 0 || Char.IsWhiteSpace(chars[i]))
                {
                    output += chars[i];
                    continue;
                }

                if (char.IsUpper(chars[i]) && Char.IsLower(chars[i - 1]))
                    output += " " + chars[i];
                else
                    output += chars[i];
            }

            return output;
        }
    }
}
