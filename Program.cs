﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Transport.WEB
{
    public class Program
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public static void Main(string[] args)
        {
            CreateWebHost(args)
                //.ConfigureLog4net()
                .Build()
                .Run();
        }

        public static IWebHostBuilder CreateWebHost(string[] args) =>

            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}