﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class AgentTransactionDTO
    {
        public int AgentId { get; set; }
        public decimal Amount { get; set; }
        public DateTime? TransactionTime { get; set; }
        public TransactionType TransactionTypes { get; set; }
        public string Transaction { get; set; }
        public decimal WalletBalance { get; set; }
        public string Name { get; set; }
        public string BookingReferenceCode { get; set; }
    }
}
