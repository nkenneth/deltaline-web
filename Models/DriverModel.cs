﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;
using Transport.WEB.Utils;

namespace Transport.WEB.Models
{
    public class DriverModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string HandoverCode { get; set; }
        public string Status => DriverStatus.ToString();
        public string DateEmployed => DateOfEmployment?.ToString(Constants.HumanDateFormat);
        public string DateAssigned => AssignedDate.ToString(Constants.HumanDateFormat);
        public string Details { get; set; }
        //[JsonIgnore]
        public virtual DriverStatus DriverStatus { get; set; }
        //[JsonIgnore]
        public DateTime? DateOfEmployment { get; set; }
        //[JsonIgnore]
        public DateTime AssignedDate { get; set; }

        public virtual DriverType DriverType { get; set; }
        public int NoOfTrips { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Designation { get; set; }
        public string ResidentialAddress { get; set; }
        public string NextOfKin { get; set; }
        public DateTime DateCreated { get; set; }
        public string DriverDetails { get; set; }
        public string Picture { get; set; }
        public bool Active { get; set; }
        public string NextOfKinNumber { get; set; }
        public string BankName { get; set; }
        public string BankAccount { get; set; }
        public string DeactivationReason { get; set; }
        public string ActivationStatusChangedByEmail { get; set; }
        public int WalletId { get; set; }
        public int? MaintenanceWalletId { get; set; }
        public string WalletNumber { get; set; }
        public decimal WalletBalance { get; set; }
        public int VehId { get; set; }
        public string VehicleNumber { get; set; }
        public int AverageRating { get; set; }
        [Display(Name = "License Date")]
        [DataType(DataType.Date)]
       
        public DateTime? LicenseDate { get; set; }
        public string DateLicense => LicenseDate?.ToString(Constants.HumanDateFormat);
        public int? DuePeriod { get; set; }
        public DateTime? ExpiredLicenseDate { get; set; }
        public bool IsExpired { get; set; }
        public DateTime? VehicleInsuranceDate { get; set; }
        public DateTime? VehicleLicenseDate { get; set; }
        public DateTime? VehicleExpiredLicenseDate { get; set; }
        public string DateDriverLicense { get; set; }
        public string DateVehicleLicense { get; set; }
        public string DateVehicleLicenseExpired { get; set; }
        public string DateDriverLicenseExpired { get; set; }
        public int? UserId { get; set; }
        public string Email { get; set; }
        public string Alias { get; set; }
    }
}