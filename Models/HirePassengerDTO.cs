﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class HirePassengerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string NokName { get; set; }
        public string NokPhone { get; set; }
        public Guid HireBusId { get; set; }
    }
}
