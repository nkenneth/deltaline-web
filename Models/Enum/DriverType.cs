﻿namespace Transport.WEB.Models.Enum
{
    public enum DriverType
    {
        PermanentCaptain = 0,
        HandoverCaptain = 1,
        OwnerCaptain = 2,
        VirtualCaptain = 3
    }
}