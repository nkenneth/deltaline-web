﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models.Enum
{
    public enum PriorityLevel
    {
        High = 0,
        Low = 1,
        Normal = 2,
        Medium = 3
        
    }
}
