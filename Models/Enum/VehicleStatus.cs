﻿namespace Transport.WEB.Models.Enum
{
    public enum VehicleStatus
    {
        Idle = 0,
        Working = 1,
        InWorkshop = 2,
        SpecialAssignement = 3,
        OnSales = 4,
        MovedToEcobus = 5,
        Disabled = 6,
        SiezedByAuthority = 7,
        Accidented = 8,
        NoCaptain = 9,
        BrokenDown = 10,
        Patrol = 11,
        TerminalUse = 12,
        RepairRequested = 13,
        WorkshopReleased = 14,
        TripDenied = 15,
        PoliceStation = 16,
        InWorkshopAndAssigned = 17,
        Pickup = 18,
        DueForService = 19,
        InTransit = 20,
        Rescue = 21,
        All =22
    }
}