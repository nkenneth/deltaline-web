﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models.Enum
{
    public enum PayTypeDescription
    {
        Bonus = 0,
        Fine = 1,
        Lateness = 2,
        DrunkDriving = 3,
        Recklesness = 4,
        SalaryPayment = 5,
        WalletUpdate = 6,
        WalletDeduction = 7,
        Patrol= 8,
        Part = 9,
        Transload = 10,
        Loan = 11,

    }
}
