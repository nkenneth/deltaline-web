﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models.Enum
{
    public enum TransactionType
    {
        Debit = 0,
        Credit = 1,
        WalletUpdate = 2
    }
}
