﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models.Enum
{
    public enum PassengerType
    {
        Adult = 0,
        Child = 1
    }

    public enum RescheduleStatus
    {
        NotReshcdule = 0,
        Pending = 1,
        PayAtTerminal = 2,
        Accepted = 3,
        Declined = 4

    }

    public enum RerouteStatus
    {
        NotReroute = 0,
        Pending = 1,
        PayAtTerminal = 2,
        Accepted = 3,
        Declined = 4

    }

    public enum PickupStatus
    {
        NotForPickUp = 0,
        PendingPickup = 1,
        Pickedup =2,
        NoShow = 3
    }
    public enum UpgradeType
    {

        Downgrade = 0,
        Upgrade = 1

    }
}
