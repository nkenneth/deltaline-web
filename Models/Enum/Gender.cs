﻿namespace Transport.WEB.Models.Enum
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }
}
