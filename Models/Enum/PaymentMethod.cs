﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models.Enum
{
    public enum PaymentMethod
    {
        Cash = 0,
        Pos = 1,
        UnifiedPaymentSolutionsLtd = 2,
        InterswitchLtd = 3,
        Isonhold = 4,
        PayStack = 5,
        BankIt = 6,
        GtbUssd = 7,
        FlutterWave = 8,
        FlutterWaveUssd = 9,
        UnionBank = 10,
        DiamondBank = 11,
        FirstBank = 12,
        CashAndPos = 13,
        ZenithBank = 14,
        GlobalPay = 15,
        QuickTeller = 16,
        GlobalAccelerex = 17,
        SterlingBank = 18,
        EBillsPay = 19,
    }
}
