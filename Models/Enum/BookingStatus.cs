﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models.Enum
{
    public enum BookingStatus
    {
        Pending = 0,
        Approved = 1,
        Cancelled = 2,
        Created = 3,
        Declined = 4,
        Expired = 5,
        Failed = 6,
        OnLock = 7,
        OnPayment = 8,
        Ongoing = 9,
        Abandoned = 10,
        Refunded = 11,
        Reversed = 12,
        TransactionError = 13,
        Unsuccessful = 14,
        GtbCancelled = 15,
        Suspended = 16
    }
}
