﻿using System;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Utils;

namespace Transport.WEB.Models
{
    public class VehicleMModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a valid Name.")]
        public string Name { get; set; }

        public int NumberOfSeats { get; set; }

        public int VehicleMakeId { get; set; }
        public string VehicleMakeName { get; set; }

        public string Date => DateCreated?.ToString(Constants.HumanDateFormat);

        public DateTime? DateCreated { get; set; }
    }
}