﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class MtuReportModelDTO
    {
        public int Id { get; set; }
        public string VehicleId { get; set; }
        public string DriverCode { get; set; }
        public string CreatorUserId { get; set; }
        public VehicleStatus Status { get; set; }
        public string VehicleStatus { get; set; }
        public int TerminalId { get; set; }
        public DateTime Date { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Notes { get; set; }
        public string Keyword { get; set; }
        public string RegistrationNumber { get; set; }
        public string Picture { get; set; }
        public List<MtuPhoto> MtuPhotos { get; set; }
        public int Type { get; set; }
    }
}
