﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class JourneyChartModel
    {
        public int TotalOutgoingBlownCount { get; set; }
        public int TotalIncomingBlownCount { get; set; }
        public int ExpectedStartupVehicles { get; set; }
        public int TotalOutgoingCount { get; set; }
        public int TotalIncomingCount { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }


    }
}
