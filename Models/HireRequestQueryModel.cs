﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class HireRequestQueryModel
    {
        public string Keyword { get; set; }
        public DateTime? StartDate { get; set; } = DateTime.Now.Date;
        public DateTime? EndDate { get; set; } = DateTime.Now;
        public int? PageIndex { get; set; } = 1;
        public int? PageSize { get; set; } = 50;
        public int? DepartureTerminalId { get; set; }
    }
}
