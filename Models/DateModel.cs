﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class DateModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Keyword { get; set; }
        public BookingTypes BookingType { get; set; }
        public int? Id { get; set; }
        public VehicleStatus status { get; set; }
        public string Code { get; set; }
        //public int departureTerminalId { get; set; }

    }

    public class SearchDTO
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Driver { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public string Code { get; set; }
        public int Id { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public int? DepartureTerminalId { get; set; }
    }

    public class SearchGuidDTO
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Driver { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public JourneyStatus Status { get; set; }
        public string Code { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public int? DepartureTerminalId { get; set; }
        public Guid Id { get; set; }
    }
}
