﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class ItemTypeDTO
    {
        public int Id { get; set; }
        public string ItemTypeName { get; set; }
        public string ItemTypeDescription { get; set; }
        //public ICollection<InventoryItem> InventoryItems { get; set; }
    }
}
