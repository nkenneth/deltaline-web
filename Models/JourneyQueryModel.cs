﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class JourneyQueryModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int? TerminalId { get; set; }
    }
}
