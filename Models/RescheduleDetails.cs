﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class RescheduleDetails
    {
        public DateTime RescheduledDate { get; set; }
        public int RouteId { get; set; }
        public string VechicleRegistrationId { get; set; }
        public int DepartureTerminalId { get; set; }
        public int DestinationTerminalId { get; set; }
        public DateTime DepartureDate { get; set; }
        public string NewDepartureTime { get; set; }

        public long SeatManagementId { get; set; }
        public int SeatNumber { get; set; }
        public string BookingReferenceCode { get; set; }
        public int VehicleModel { get; set; }
        public Guid?  VehicleTripRegistrationId { get; set; }
        public decimal? Amount { get; set; }
        public decimal? NewRouteAmount { get; set; }
        public decimal? PreviousRouteAmount { get; set; }
        public string reroutestatus { get; set; }
        public int RerouteId { get; set; }
        public Guid? TripId { get; set; }
        public decimal? newRouteFee { get; set; }

    }
}
