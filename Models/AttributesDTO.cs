﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class AttributesDTO
    {
        public string AttributeName { get; set; }
        public string AttributeDescription { get; set; }
        public int Id { get; set; }
    }
}
