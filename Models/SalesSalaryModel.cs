﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class SalesSummaryModel
    {
        public decimal? TodaysSales { get; set; }
        public decimal? LastSales { get; set; }
        public int? TodaysBookings { get; set; }
    }
}
