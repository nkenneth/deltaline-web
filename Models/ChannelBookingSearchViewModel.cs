﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class BookingSearchViewModel
    {
        public int? ChannelId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Keyword { get; set; }
        public int PageIndex { get; set; }
        public int Pagesize { get; set; }
        public int? Status { get; set; }
        public int? TerminalId { get; set; }
    }

    public class AdvancedBookingSearchViewModel
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int PageIndex { get; set; }
        public int Pagesize { get; set; }
        public int? Status { get; set; }
        public int? TerminalId { get; set; }
    }

    public class ChannelSalesSearchViewModel
    {
        public int? ChannelId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int? Status { get; set; }
        public int? TerminalId { get; set; }
    }

    public class ChannelBookingSalesSummaryViewModel
    {
        public string BookingType { get; set; }
        public decimal SalesAmmount { get; set; }
        public decimal DiscountedAmount { get; set; }
    }

    public class BookingSalesSummaryDto<T>
    {
        public decimal TotalSales { get; set; }
        public decimal TotalDiscountedSales { get; set; }
        public PagedRecordModel<T> Records { get; set; }
    }

    public class AdvancedBookingSalesSummaryDto<T>
    {
        public decimal PosSales { get; set; }
        public decimal CashSales { get; set; }
        public decimal TotalSales { get; set; }
        public PagedRecordModel<T> Records { get; set; }
    }

    public class AdvancedBookingReportViewModel
    {
        public string BookingReferenceCode { get; set; }
        public string VehicleNumber { get; set; }
        public string Employee { get; set; }
        public string PaymentMethod { get; set; }
        public decimal Amount { get; set; }
        public decimal DiscountedAmount { get; set; }
        public string BookingDate { get; set; }
        public string DepartureDate { get; set; }
    }

    public class BookingReportViewModel
    {
        public int SeatNumber { get; set; }
        public string BookingReferenceCode { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string NextOfKinName { get; set; }
        public decimal Amount { get; set; }
        public decimal DiscountedAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal RerouteFeeDiff { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string Terminal { get; set; }
        public string Destination { get; set; }
    }
}