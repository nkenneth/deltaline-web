﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class GetTripFareDTO
    {
        public int routeId { get; set; }

        public Guid tripId { get; set; }
    }
}
