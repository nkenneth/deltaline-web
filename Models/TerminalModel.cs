﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Transport.WEB.Models
{
    public class TerminalModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter a valid terminal name.")]
        public string Name { get; set; }
        [StringLength(3)]
        [Required]
        public string Code { get; set; }
        public string Image { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonNo { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public bool IsNew { get; set; }
        public DateTime? StartDate { get; set; }
        public TerminalType TerminalType { get; set; }
        public BookingTypes BookingType { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int RouteId { get; set; }
        public bool IsCommision { get; set; }
        public decimal OnlineDiscount { get; set; }
        public bool IsOnlineDiscount { get; set; }
    }
}