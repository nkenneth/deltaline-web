﻿namespace Transport.WEB.Models
{
    public class StateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int RegionId { get; set; }
        public string RegionName { get; set; }
    }
}