﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class GeneralTransactionDTO
    {
        public long? CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletionTime { get; set; }
        public Guid id { get; set; }
        public TransactionType TransactionType { get; set; }
        public string TransType { get; set; }
        public PayTypeDescription PayTypeDiscription { get; set; }
        public string PayType { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactedBy { get; set; }
        public bool IsActive { get; set; }
        public string TransDescription { get; set; }
    }
}
