﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class VehicleTripRouteSearchDTO
    {
        public TripType TripType { get; set; }

        [Required, Display(Name = "Departure Terminal")]
        public int DepartureTerminalId { get; set; }

        [Required, Display(Name = "Destination Terminal")]
        public int DestinationTerminalId { get; set; }

        [Required, Display(Name = "Departure Date")]
        public DateTime DepartureDate { get; set; }

        [RequiredIf(nameof(TripType), TripType.Return), Display(Name = "Return Date")]
        public DateTime? ReturnDate { get; set; }

        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
        public string BookingReferenceCode { get; set; }
    }
}
