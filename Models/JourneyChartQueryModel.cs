﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class JourneyChartQueryModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? RegionId { get; set; }
        public int? StateId { get; set; }
        public JourneyType JourneyType { get; set; }
        public JourneyStatus JourneyStatus { get; set; }
        public int DepartureTerminalId { get; set; }
        public int DestinationTerminalId { get; set; }
    }
}
