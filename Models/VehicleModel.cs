﻿using System;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;
using Transport.WEB.Utils;

namespace Transport.WEB.Models
{
    public class VehicleModel
    {
        public int Id { get; set; }

        public string Details { get; set; }
        [Required(ErrorMessage = "Please enter a valid registration number.")]
        public string RegistrationNumber { get; set; }
        public string ChasisNumber { get; set; }
        public string EngineNumber { get; set; }
        public VehicleStatus VehicleStatus { get; set; }
        public string Status => VehicleStatus.ToString();

        public DateTime? DateCreated { get; set; }

        public int VehicleModelId { get; set; }
        public string VehicleModelName { get; set; }

        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        [Required]
        public int? DriverId { get; set; }
        public string DriverName { get; set; }

        public int? FranchizeId { get; set; }
        public string FranchiseName { get; set; }
        public string DriverNo { get; set; }

        [Display(Name = "License Date")]
        [DataType(DataType.Date)]
        public DateTime? LicenseDate { get; set; }
        public string DateLicense => LicenseDate?.ToString(Constants.HumanDateFormat);

        [Display(Name = "Insurance Date")]
        [DataType(DataType.Date)]
        public DateTime? InsuranceDate { get; set; }
        public string DateInsurance => InsuranceDate?.ToString(Constants.HumanDateFormat);
        public string Description { get; set; }
    }


    public class AllocationVehicleModel
    {
        public string RegistrationNumber { get; set; }
        public int? LocationId { get; set; }
        public int? TerminalId { get; set; }
        public int Id { get; set; }
        public int Type { get; set; }
        public string Terminals { get; set; }
    }

    public class VehicleAllocationConfirmation
    {
        public string RegistrationNumber { get; set; }
        public int? DriverId { get; set; }
        public DateTime TransactionDate { get; set; }
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }
        public int? DestinationTerminal { get; set; }
        public string UserEmail { get; set; }
    }
}