﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;
namespace Transport.WEB.Models
{
    public class VehicleTripRegistration
    {
        public Guid VehicleTripRegistrationId { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public DateTime DepartureDate { get; set; }
        public string DepartureTime { get; set; }
        public string RouteName { get; set; }
        public bool IsVirtualBus { get; set; }
        public bool IsBusFull { get; set; }
        public bool IsBlownBus { get; set; }
        public string VehicleCaptaindetails { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }

        // FK
        public string CaptainCode { get; set; }
        public string DriverCode { get; set; }

        public string OriginalCaptainCode { get; set; }

        public int? BookingTypeId { get; set; }
        public int? RouteId { get; set; }
        public Guid TripId { get; set; }
        public decimal DriverFee { get; set; }
        public int? CurrentModelId { get; set; }
        public int? OriginalModelId { get; set; }
        public int? VehicleModelId { get; set; }
        public string VehicleModel { get; set; }
        public decimal? AmountToAdd { get; set; }
        public JourneyStatus JourneyStatus { get; set; }

        public long? BookingId { get; set; }

        //
        public TripType TripType { get; set; }
        public virtual JourneyStatus Status { get; set; }
    }
}
