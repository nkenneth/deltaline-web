﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class VehicleAllocationDTO
    {
        public List<VehicleModel> VehicleModel { get; set;}

        public string RouteName { get; set; }
        public int RouteId { get; set; }
    }
}
