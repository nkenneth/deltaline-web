﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class ManifestDTO
    {
        public Guid Id { get; set; }
        public bool IsPrinted { get; set; }
        public string PrintedBy { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Dispatch { get; set; }
        public string DispatchSource { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }
        public string VehicleModel { get; set; }
        //public int? VehicleModelId { get; set; }
        //public string Employee { get; set; }
        //public string Refcode { get; set; }
        //public int? RefcodeSeatNumber { get; set; }
        //public int? RefcodeSubrouteId { get; set; }
        //public decimal? RefcodeAmount { get; set; }
        //public long? SeatManagementId { get; set; }
        //public DateTime? ManifestPrintedTime { get; set; }
        //public int? RouteId { get; set; }
        //public string RouteName { get; set; }

        public string DriverCode { get; set; }
        public string DriverPhone { get; set; }
        public string DriverName { get; set; }
        public string Route { get; set; }
        public string BusRegistrationNumber { get; set; }
        public IEnumerable<SeatManagementDTO> Passengers { get; set; }
        public int NumberOfSeats { get; set; }
        public int TotalSeats { get; set; }
        public DateTime DepartureDate { get; set; }
        public string DepartureTime { get; set; }
        public IList<int> RemainingSeat { get; set; }
        public IList<int> ClashingSeats { get; set; }
        public IList<int> BookSeat { get; set; }
        public IList<int> BookingTypes { get; set; }
        public IList<bool> TicketPrintStatus { get; set; }
        public int RescheduleFee { get; set; }
        public decimal? RerouteFee { get; set; }
        public decimal? TotalSold { get; set; }
        public int RemainingSeatCount { get; set; }
        public decimal? Commision { get; set; }
        public decimal? Patrol{ get; set; }
        public decimal? Transload { get; set; }
        public decimal? VAT { get; set; }
        public bool ManifestPrinted { get; set; }
        public long? SeatManagementId { get; set; }
        public DateTime? ManifestPrintedTime { get; set; }
        public int? RouteId { get; set; }
        public string RouteName { get; set; }
        public string Employee { get; set; }
        public string Refcode { get; set; }
        public int? RefcodeSeatNumber { get; set; }
        public int? RefcodeSubrouteId { get; set; }
        public decimal? RefcodeAmount { get; set; }
        public decimal? DriverFee { get; set; }
        public long? CreatorUserId { get; set; }
        public string Name { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public DateTime? CreationTime { get; set; }
        public int? DepartureTerminalId { get; set; }
    }
}
