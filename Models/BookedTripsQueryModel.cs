﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class BookedTripsQueryModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string PhysicalBusRegisterationNumber { get; set; }
        public int? BookingType { get; set; }
        public int TerminalId { get; set; }
        public string TerminalName { get; set; }
        public int DepartureTerminal { get; set; }
        public int DestinationTerminal { get; set; }
        public bool IsPrinted { get; set; }
        public int? DepartureTerminalId { get; set; }
    }
}
