﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class TicketSaleModel
    {
        public BookingDetailModel Booking { get; set; }
        public ManifestViewModel Manifest { get; set; }

    }
}
