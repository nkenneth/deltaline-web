﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class ManifestViewModel
    {

        public ManifestViewModel()
        {
            SeatBooking= new List<Tuple<int, int, bool>>();
        }
        public Guid ManifestManagementId { get; set; }

        public int NumberOfSeats { get; set; }
        public int TotalSeats { get; set; }
        public DateTime DepartureDate { get; set; }
        public string DepartureTime { get; set; }
        public decimal? Amount { get; set; }
        public decimal? TotalSold { get; set; }
        public decimal? Dispatch { get; set; }
        public string DispatchSource { get; set; }
        public decimal? RescheduleFee { get; set; }
        public decimal? RerouteFee { get; set; }
        public List<int> RemainingSeat { get; set; }
        public List<int> ClashingSeats { get; set; }
        public List<int> BookSeat { get; set; }
        public List<int> BookingTypes { get; set; }
        public List<bool> TicketPrintStatus { get; set; }
        public bool ManifestPrinted { get; set; }
        public bool IsPrinted { get; set; }
        public List<Tuple<int, int, bool>> SeatBooking { get; set; }
        public int RemainingSeatCount { get; set; }
        public int TicketUpdate { get; set; }
        public bool Update { get; set; }
        public bool WithAmount { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }
        public Guid? JourneyManagementId { get; set; }
        public string DriverCode { get; set; }
        public string DriverPhone { get; set; }
        public string VehicleAge { get; set; }
        public string DriverName { get; set; }
        public string Route { get; set; }
        public string VehicleModel { get; set; }
        public string BusRegistrationNumber { get; set; }
        public List<SeatManagement> Passengers { get; set; }
        public string printedBy { get; set; }
        public decimal? Commision { get; set; }
        public decimal? Patrol{ get; set; }
        public decimal? Transload { get; set; }
        public decimal? VAT { get; set; }

    }
    public class SeatManagement
    {
        public string refCode;

        public long Id { get; set; }
        public long SeatManagementId { get; set; }
        public Guid? TripId { get; set; }
        public long? BookingId { get; set; }
        public int SeatNumber { get; set; }
        public int RemainingSeat { get; set; }
        public string BookingReferenceCode { get; set; }
        public string TripCode { get; set; }
        public string MainBookerReferenceCode { get; set; }
        public string PhoneNumber { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByTerminal { get; set; }
        public string NextOfKinName { get; set; }
        public string NextOfKinPhoneNumber { get; set; }
        public string FullName { get; set; }
        public string DepartureTime { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public string VehicleName { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime NewDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? PartCash { get; set; }
        public string POSReference { get; set; }
        public decimal? UpgradeDowngradeDiff { get; set; }
        public decimal? Discount { get; set; }
        public bool IsMainBooker { get; set; }
        public bool IsPrinted { get; set; }
        public bool IsCrossSell { get; set; }
        public bool HasReturn { get; set; }
        public bool IsReturn { get; set; }
        public bool IsUpgradeDowngrade { get; set; }
        public int? NoOfTicket { get; set; }
        public string PickupPointImage { get; set; }
        public bool FromTransload { get; set; }
        public int? RouteId { get; set; }
        public int? PreviousRouteId { get; set; }
        public string RouteName { get; set; }
        public int? SubRouteId { get; set; }
        public string SubRouteName { get; set; }
        public int? PickUpPointId { get; set; }
        public string PickupPointName { get; set; }
        public int? VehicleModelId { get; set; }
        public bool IsRescheduled { get; set; }
        public bool IsRerouted { get; set; }
        //FK
        public BookingStatus BookingStatus { get; set; }
        public TravelStatus TravelStatus { get; set; }
        public PassengerType PassengerType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public BookingType BookingType { get; set; }
        public RescheduleStatus RescheduleStatus { get; set; }
        public PickupStatus PickupStatus { get; set; }
    
        public RerouteStatus RerouteStatus { get; set; }
        //public RerouteMode RerouteMode { get; set; }
        public UpgradeType UpgradeType { get; set; }
        //public LuggageType? LuggageType { get; set; }
        public decimal? RerouteFeeDiff { get; set; }
        public decimal? RescheduleFeeDiff { get; set; }
        public Gender Gender { get; set; }
        public Guid? VehicleTripRegistrationId { get; set; }
        public string VehicleTripRegistration { get; set; }
        public string DepartureTerminalName { get; set; }
        public string DestinationTerminalName { get; set; }
        public DateTime? LastModificationTime { get; set; }

        public bool IsGhanaRoute { get; set; }
        public string PassportType { get; set; }
        public string PassportId { get; set; }
        public string PlaceOfIssue { get; set; }
        public DateTime? IssuedDate { get; set; }
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //public DateTime? ExpiredDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ExpiredDate { get; set; }
        public string Nationality { get; set; }

    }

    public class Seat
    {
        public int SeatNumber { get; set; }
    }


    public class Manifest
    {

        public Guid ManifestManagementId { get; set; }

        public int NumberOfSeats { get; set; }
        public int? RouteId { get; set; }
        public long? Id { get; set; }
        public string BusRegNum { get; set; }
        public long? MainBookerId { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Dispatch { get; set; }
        public bool IsPrinted { get; set; }
        public bool ManifestPrinted { get; set; }
        // FK
        public Guid VehicleTripRegistrationId { get; set; }

        public string Employee { get; set; }
        public string Otp { get; set; }
        public string Refcode { get; set; }
        public int? RefcodeSeatNumber { get; set; }
        public int? RefcodeSubrouteId { get; set; }
        public decimal? RefcodeAmount { get; set; }
        public decimal? Commision { get; set; }
        public bool IsCommision { get; set; }
        public decimal? Patrol{ get; set; }
        public decimal? Transload { get; set; }
        public decimal? VAT { get; set; }
        public int NumberOfPassengers {get; set;}
    }
}
