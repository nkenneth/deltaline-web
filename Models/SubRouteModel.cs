﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Transport.WEB.Models
{
    public class SubRouteModel
    {
        public int? Id { get; set; }
        public int? NameId { get; set; }
        public string Name { get; set; }
        public int? SubRouteId { get; set; }
        public int? SubRouteNameId { get; set; }
        public string SubRouteName { get; set; }
        public int RouteId { get; set; }
        public string RouteName { get; set; }
    }


    public class SwapVehicle
    {
        public int VehicleId { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }

        [Required(ErrorMessage = "Please select Driver.")]
        public string DriverCode { get; set; }
        public string OriginalDriverCode { get; set; }

    }

}