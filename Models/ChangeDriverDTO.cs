﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class ChangeDriverDTO
    {
        public int DriverId { get; set; }
        public string RegistrationNumber { get; set; }
        public string DriverName { get; set; }
    }
}
