﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;


namespace Transport.WEB.Models
{
    public class GroupedTripsDetailDTO
    {
        public TripType TripType { get; set; }
        public List<AvailabileTripDetail> Departures { get; set; }
        public List<AvailabileTripDetail> Arrivals { get; set; }
        
    }
}
