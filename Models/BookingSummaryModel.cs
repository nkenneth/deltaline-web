﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class BookingSummaryModel
    {
        public int? OnlineChannelCount { get; set; }
        public int? AdvancedBookingCount { get; set; }
        public int? TerminalBookingCount { get; set; }
    }
}
