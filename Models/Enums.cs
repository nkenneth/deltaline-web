﻿namespace Transport.WEB.Models
{
    public enum RouteType
    {
        Short,
        Medium,
        Long
    }
    public enum JourneyType
    {
        Loaded,
        Blown,
        Pickup,
        Rescue,
        Transload,
        Hire
    }
    public enum TerminalType
    {
        Physical,
        Virtual
    }

    public enum BookingTypes
    {
        Terminal,
        Advanced,
        Online,
        All,
        BookOnHold,
        Agent
    }
}