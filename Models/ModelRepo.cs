﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;


namespace Transport.WEB.Models
{
    public class ModelRepo
    {
        public List<VehicleModel> VehicleModel { get; set; }
        //public List<AllocationVehicleModel> AllocationVehicleModel { get; set; }
    }
}