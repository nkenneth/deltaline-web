﻿using System;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class FareModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        [Required(ErrorMessage ="Amount is required")]
        public decimal Amount { get; set; }
        public float? ChildrenDiscountPercentage { get; set; }

        [Required(ErrorMessage ="Route is required")]
        public int RouteId { get; set; }
        public string RouteName { get; set; }

        [Required(ErrorMessage ="Vehicle model is required")]
        public int VehicleModelId { get; set; }
        public string VehicleModelName { get; set; }
    }

    public class DiscountModel
    {
        public Guid Id { get; set; }
        public BookingType BookingType { get; set; }
        public string BookingTypeName { get; set; }
        public decimal AdultDiscount { get; set; }
        public decimal MinorDiscount { get; set; }
        public decimal MemberDiscount { get; set; }
        public decimal ReturnDiscount { get; set; }
        public decimal AppDiscountIos { get; set; }
        public decimal AppDiscountAndroid { get; set; }
        public decimal AppDiscountWeb { get; set; }
        public decimal AppReturnDiscountIos { get; set; }
        public decimal AppReturnDiscountAndroid { get; set; }
        public decimal AppReturnDiscountWeb { get; set; }
        public decimal PromoDiscount { get; set; }
        public decimal CustomerDiscount { get; set; }
        public bool Active { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
    }


    public class FareCalendarModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int? RouteId { get; set; }
        public string RouteName { get; set; }

        public int? TerminalId { get; set; }
        public string TerminalName { get; set; }

        public int? VehicleModelId { get; set; }
        public string VehicleModelName { get; set; }
        public string FareAdjustmentTypeName { get; set; }
        public string FareParameterTypeName { get; set; }
        public string FareTypeName { get; set; }
        public FareType FareType { get; set; }
        public FareAdjustmentType FareAdjustmentType { get; set; }
        public FareParameterType FareParameterType { get; set; }
        public BookingType BookingTypes { get; set; }
        public decimal FareValue { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }
    }



}