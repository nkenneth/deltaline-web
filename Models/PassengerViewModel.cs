﻿using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class PassengerViewModel
    {
        public string BookingReferenceCode { get; set; }
        public decimal? Amount { get; set; }
        public string FullName { get; set; }
        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }
        public int SeatNumber { get; set; }
        public string Route { get; set; }
        public BookingTypes BookingType { get; set; }
        public decimal? Dispatch { get; set; }
        public string BookingTyped { get; set; }
        public decimal? Commision { get; set; }
        public decimal? Patrol{ get; set; }
        public decimal? Transload { get; set; }
        public decimal? VAT { get; set; }
    }
}