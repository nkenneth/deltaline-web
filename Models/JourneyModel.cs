﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class JourneyModel
    {
        public string ApprovedBy { get; set; }
        public string ReceivedBy { get; set; }
        public decimal DispatchFee { get; set; }
        public decimal DriverFee { get; set; }
        public decimal LoaderFee { get; set; }
        public JourneyStatus JourneyStatus { get; set; }
        public Guid JourneyManagementId { get; set; }


        public JourneyType JourneyType { get; set; }


        public bool ManifestPrinted { get; set; }
        public string DepartureTime { get; set; }
        public string TripCode { get; set; }
        public bool AvailableOnline { get; set; }


        public string RouteName { get; set; }
        public string DepartureTerminalName { get; set; }
        public string DestinationTerminalName { get; set; }
        public DateTime DepartureDate { get; set; }
        public string AppovedBy { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }

    }
}
