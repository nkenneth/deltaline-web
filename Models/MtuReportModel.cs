﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class MtuReportModel
    {
        public int Id { get; set; }
        public string VehicleId { get; set; }
        public string DriverCode { get; set; }
        public string CreatorUserId { get; set; }
        public VehicleStatus Status { get; set; }
        public string VehicleStatus { get; set; }
        public DateTime Date { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Notes { get; set; }
        public string Keyword { get; set; }
        public string RegistrationNumber { get; set; }
        public IFormFile Picture { get; set; }
        public string PicturePath { get; set; }
        public List<IFormFile> Pictures { get; set; }
    }
}
