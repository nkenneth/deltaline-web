﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class AgentCommissionDTO
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
        public decimal Discount { get; set; }
    }
}
