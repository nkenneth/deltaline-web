﻿using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class BookingResultModel
    {
        public int SeatNumber { get; set; }
        public string Response { get; set; }
        public decimal? Amount { get; set; }
        public long? MainBookerId { get; set; }
        public string BookingReferenceCode { get; set; }
        public string Route { get; set; }
        public string DepartureDate { get; set; }
        public string PickUpDetails { get; set; }
        public string SelectedSeats { get; set; }
        public string DepartureTime { get; set; }
    }
}