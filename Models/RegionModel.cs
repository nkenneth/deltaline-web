﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Transport.WEB.Models
{
    public class RegionModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string DateCreated { get; set; }
    }
}