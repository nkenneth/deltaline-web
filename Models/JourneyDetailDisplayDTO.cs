﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class JourneyDetailDisplayDTO
    {
        public string DriverCode { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public string Name { get; set; }
        public string DriverName { get; set; }
        public JourneyStatus journeyStatus { get; set; }
        public JourneyType JourneyType { get; set; }
        public DateTime ManifestPrintedTime { get; set; }
    }
}
