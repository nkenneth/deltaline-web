﻿using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class BeneficiaryDetailModel
    {
        [Required, Display(Name = "Beneficiary Name")]
        public string FullName { get; set; }

        [Required, Display(Name = "Gender")]
        public Gender Gender { get; set; }
        public PassengerType PassengerType { get; set; }
    }
}