﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class RemainingSeatDTO
    {
        public Guid VehicleTripRegistrationId { get; set; }
        public List<int> RemainingSeat { get; set; }
    }
}
