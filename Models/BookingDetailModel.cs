﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class BookingDetailModel
    {
        public BookingDetailModel()
        {
            Beneficiaries = new List<BeneficiaryDetailModel>();
            RemainingSeat = new List<int>();
            Subroutes = new List<SubRouteModel>();
            PhoneList = new List<string>();
            seatBooking = new List<Tuple<int, int, bool>>();
        }

        [Display(Name = "Trip Type")]
        public TripType TripType { get; set; }
        public PassengerType PassengerType { get; set; }
        public BookingType BookingType { get; set; }
        public long? TicketUpdate { get; set; }
        public bool Update { get; set; }

        [Required, Display(Name = "Payment Method")]
        public PaymentMethod PaymentMethod { get; set; }

        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required, Display(Name = "Gender")]
        public Gender Gender { get; set; }

        [Required, Display(Name = "Luggage Type")]

        [EmailAddress, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public int? RouteId { get; set; }
        public int TotalSeats { get; set; }
        public bool isSub { get; set; }
        public bool isSubReturn { get; set; }

        public int? RouteIdReturn { get; set; }
        public int? SubrouteId { get; set; }
        public int? SeatNumber { get; set; }
        public Guid? VehicleTripRegistrationId { get; set; }

        public decimal? Amount { get; set; }
        public decimal? PartCash { get; set; }
        public string PosRef { get; set; }
        public decimal? Discount { get; set; }

        public string BookingReference { get; set; }
        public string TicketNumber { get; set; }

        [Required, Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "POS Reference")]
        public string PosReference { get; set; }

        [Display(Name = "Home Address"), DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [Required, Display(Name = "Next-of-Kin Name")]
        public string NextOfKinName { get; set; }

        [Required, Display(Name = "Next-of-Kin Phone")]
        public string NextOfKinPhone { get; set; }

        public string SeatRegistrations { get; set; }
        public string Routes { get; set; }


        [Display(Name = "Beneficiaries"), UIHint("BeneficiaryDetail")]
        public List<BeneficiaryDetailModel> Beneficiaries { get; set; }
        public List<SubRouteModel> Subroutes { get; set; }
        public List<string> PhoneList { get; set; }
        public List<int> RemainingSeat { get; set; }
        public List<Tuple<int, int, bool>> seatBooking { get; set; }
        public bool ManifestPrinted { get; set; }
        public BookingStatus BookingStatus { get; set; }


        public bool IsLoggedIn { get; set; }

        public TravelStatus TravelStatus { get; set;}

        public bool IsGhanaRoute { get; set; }
        public string PassportType { get; set; }
        public string PassportId { get; set; }
        public string PlaceOfIssue { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? IssuedDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpiredDate { get; set; }
        public string Nationality { get; set; }
        public string PassportTypeId { get; set;}
        


    }

}