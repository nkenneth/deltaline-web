﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class HireBusDTO
    {
        public Guid Id { get; set; }
        public string DepartureAddress { get; set; }
        public string DestinationAddress { get; set; }
        public decimal Amount { get; set; }
        public int VehicleId { get; set; }
        public string RegistrationNo { get; set; }
        public string DriverCode { get; set; }
        public string DriverName { get; set; }
        public DateTime DepartureDate { get; set; }
        public int UserLocationId { get; set; }
        public string LocationName { get; set; }
        public int userId { get; set; }
        public int DepartureTerminalId { get; set; }
        //added IsManifestPrinted
        public bool? IsManifestPrinted { get; set; } = false;
        public DateTime? ArrivalDate { get; set; }
        public int? ReceivedLocationId { get; set; }
    }
}
