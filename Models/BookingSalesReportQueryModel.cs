﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class BookingSalesReportQueryModel
    {
        public int? RouteId { get; set; }
        public int? TerminalId { get; set; }
        public int? StateId { get; set; }
        public int? PaymentMethod { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
