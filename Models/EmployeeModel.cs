﻿using System;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;
using Transport.WEB.Utils;

namespace Transport.WEB.Models
{
    public class EmployeeModel
    {
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string EmployeeCode { get; set; }
        public DateTime? DateOfEmployment { get; set; }
        public string DateEmployed => DateOfEmployment?.ToString(Constants.HumanDateFormat);
        public string EmployeeGender => Gender.ToString();

        public string FullName => FirstName + " " + LastName;
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string EmployeePhoto { get; set; }
        public string NextOfKin { get; set; }
        public string NextOfKinPhone { get; set; }

        public int? WalletId { get; set; }
        public string WalletNumber { get; set; }

        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public int? TerminalId { get; set; }
        public string TerminalName { get; set; }
        public string UserId { get; set; }
        public string Otp { get; set; }
        public bool OtpIsUsed { get; set; }
        public string TicketRemovalOtp { get; set; }
        public bool TicketRemovalOtpIsUsed { get; set; }
        public DateTime? OTPLastUsedDate { get; set; }
        public int? OtpNoOfTimeUsed { get; set; }
        public bool LockoutEnabled { get; set; }
    }
}