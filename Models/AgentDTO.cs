﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class AgentDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string NextOfKin { get; set; }
        public string Username { get; set; }
        public string ReferralCode { get; set; }
        public string NextOfKinPhone { get; set; }
        public string AgentLocationName { get; set; }
        public Gender Gender { get; set; }
        public string Genders { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }
        public int AgentCommissionId { get; set; }
        public int AgentLocationId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public DateTime CreationTime { get; set; }
        public string AgentStatusText { get; set; }
        public string BookingReferenceCode { get; set; }
        public decimal Commission { get; set; }
        public int NoOfBookings { get; set; }
    }
}
