﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class WalletTransactionDTO
    {
        public TransactionType TransactionType { get; set; }
        public string TransType { get; set; }
        public Guid TransactionSourceId { get; set; }
        public string UserId { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal LineBalance { get; set; }
        public int WalletId { get; set; }
        public virtual WalletModel Wallet { get; set; }
        public string TransDescription { get; set; }
        public int DriverId { get; set; }
        public string PayTypeDes { get; set; }
        public PayTypeDescription PayTypeDiscription { get; set; }
        public string Notes { get; set; }
        public string TransactedBy { get; set; }
        public Guid Id { get; set; }
        public string Code { get; set; }
        public bool IsSum { get; set; }
        public bool IsCaptured { get; set; }
        public bool IsVerified { get; set; }
        public string IsVerifiedBy { get; set; }
        public DateTime? IsVerifiedDate { get; set; }
        public bool IsApproved { get; set; }
        public string IsApprovedBy { get; set; }
        public DateTime? IsApprovedDate { get; set; }
        public string DriverName { get; set; }
        public TransactionStatus Status { get; set; }
        public bool? IsReset { get; set; }
        public DateTime? LastResetDate { get; set; }
        public decimal OldBalance { get; set; }
        public string UpdatedBy { get; set; }
        public decimal Balance { get; set; }
        public string DriverBankName { get; set; }
        public string DriverBankAccount { get; set; }
    }
}
