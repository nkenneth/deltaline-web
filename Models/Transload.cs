﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class Transload
    {
        //public int TerminalId { get; set; }
        //public int RouteId { get; set; }
        //public int TripId { get; set; }
        //public int VehicleId { get; set; }
        //public int DriverId { get; set; }
        public Guid JourneyManagementId { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }
        public string PilotCode { get; set; }
        public decimal? Amount { get; set; }
        public DateTime JourneyDate { get; set; }
        public VehicleStatus VehicleStatus { get; set; }
        public string TransloadedPilot { get; set; }
        public string TransloadedVehicle { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public string Email { get; set; }
        public string TransloadedBy { get; set; }
        public string NewPilot { get; set; }
        public string NewVehicle { get; set; }
        public JourneyType TransloadedJourneyType { get; set; }
        public string TransloadJourneyType => TransloadedJourneyType.ToString();
        public JourneyType NewJourneyType { get; set; }
        public string NwJourneyType => NewJourneyType.ToString();
        public string Route { get; set; }
        public string NewPilots { get; set; }
        public string TransloadedPilots { get; set; }
        public int TransloadTerminal { get; set; }
        public decimal? NewDriverFee { get; set; }
    }
}
