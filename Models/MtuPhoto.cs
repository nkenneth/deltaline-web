﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class MtuPhoto
    {
        public int Id { get; set; }
        public string FileName { get; set; }
    }
}
