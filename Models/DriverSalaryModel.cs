﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class DriverSalaryModel
    {
        public decimal DriverFee { get; set; }
        public string  DriverCode { get; set; }
        public int NoofTrips { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string BankAccount { get; set; }
        public int DriverId { get; set; }

    }
}
