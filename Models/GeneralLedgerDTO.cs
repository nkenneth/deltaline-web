﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class GeneralLedgerDTO 
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public int TransactionTypeId { get; set; }
        public int TransactionSourceId { get; set; }
    }
}
