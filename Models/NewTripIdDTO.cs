﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class NewTripIdDTO
    {
        public Guid vehicleTripReg { get; set; }

        public Guid newTripId { get; set; }

        public int RouteId { get; set; }
    }
}
