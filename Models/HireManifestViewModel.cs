﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class HireManifestViewModel
    {
        public Guid ManifestManagementId { get; set; }
        public int NumberOfSeats { get; set; }
        public int TotalSeats { get; set; }
        public DateTime DepartureDate { get; set; }
        public Guid HireTripRegistrationId { get; set; }
        public Guid? JourneyManagementId { get; set; }
        public string DriverCode { get; set; }
        public string DriverPhone { get; set; }
        public string DriverName { get; set; }
        public string Route { get; set; }
        public string VehicleModel { get; set; }
        public string BusRegistrationNumber { get; set; }
        public List<HirePassengerDTO> Passengers { get; set; }
        public string printedBy { get; set; }
    }
}
