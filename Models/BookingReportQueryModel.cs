﻿using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class BookingReportQueryModel
    {
        public int? TerminalId { get; set; }
        public int? BookingType { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 30;
        public string Keyword { get; set; }
        public string CreatedBy { get; set; }
        public string ReferenceCode { get; set; }
        public int? BookingStatus { get; set; }
        public IDataTablesRequest request { get; set; }
    }
}
