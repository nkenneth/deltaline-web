﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;
using Transport.WEB.Utils;

namespace Transport.WEB.Models
{
    public class VehicleDTO
    {
        public string Details { get; set; }
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public string ChasisNumber { get; set; }
        public string EngineNumber { get; set; }
        public string IMEINumber { get; set; }
        public string Type { get; set; }
        public VehicleStatus VehicleStatus { get; set; }
        public string Status => VehicleStatus.ToString();
        public DateTime DateCreated { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public int VehicleModelId { get; set; }
        public string VehicleModelName { get; set; }
        public bool IsOperational { get; set; }
        public string DriverName { get; set; }
        public int? DriverId { get; set; }
        public string DriverCode { get; set; }

        //public int? FranchiseId { get; set; }
        public int? FranchizeId { get; set; }
        public string FranchiseName { get; set; }
        public DateTime? LicenseDate { get; set; }
        public DateTime? InsuranceDate { get; set; }
        public DateTime? ExpiredLicenseDate { get; set; }
        public string DateLicense { get; set; }
        public string DateLicenseExpired { get; set; }
        public string Description { get; set; }
    }
}
