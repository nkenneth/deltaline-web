﻿using System;
using System.Collections.Generic;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{

    public class AvailabileTripDetail
    {
        public string RouteName { get; set; }
        public int? RouteId { get; set; }
        public bool isSub { get; set; }
        public bool isSubReturn { get; set; }

        public int? RouteIdReturn { get; set; }
        public string VehicleName { get; set; }
        public string PhysicalBus { get; set; }
        public string DriverCode { get; set; }
        public string VehicleRegNumber { get; set; }
        public DateTime DepartureDate { get; set; }
        public string DepartureTime { get; set; }
        public int AvailableNumberOfSeats { get; set; }
        public IEnumerable<int> AvailableSeats { get; set; }
        public decimal FarePrice { get; set; }
        public bool HasPickup { get; set; }
        public decimal MemberFare { get; set; }
        public decimal ChildFare { get; set; }
        public decimal AdultFare { get; set; }
        public decimal ReturnFare { get; set; }
        public decimal PromoFare { get; set; }
        public decimal AppFare { get; set; }
        public decimal AppReturnFare { get; set; }
        public string VehicleFacilities { get; set; }
        public IEnumerable<int> BookedSeats { get; set; }
        public int TotalNumberOfSeats { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }
        //public virtual JourneyStatus Status { get; set; }
        public JourneyStatus JourneyStatus { get; set; }
        public JourneyType JourneyType { get; set; }
    }

    public class GroupedTripsDetail
    {
        public GroupedTripsDetail()
        {
            Departures = new List<AvailabileTripDetail>();
            Arrivals = new List<AvailabileTripDetail>();
        }

        public TripType TripType { get; set; }
        public List<AvailabileTripDetail> Departures { get; set; }
        public List<AvailabileTripDetail> Arrivals { get; set; }
    }
}
