﻿using System;
using System.ComponentModel.DataAnnotations;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class TerminalBookingModel
    {
        public TripType TripType { get; set; }
        public Guid TripId { get; set; }
        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public string HireFrom { get; set; }
        public string HireTo { get; set; }
        public double Distance { get; set; }
        public string DepartureTime { get; set; }
        public decimal DispatchFee { get; set; }
        public int DestinationTerminalId { get; set; }
        public int DepartureTerminalId { get; set; }

        [Display(Name = "Departure Date")]
        public DateTime? DepartureDate { get; set; }

        [Display(Name = "Return Date")]
        public DateTime? ReturnDate { get; set; }

        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }

        public int VehicleId { get; set; }
        public int VehicleModelId { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public string DriverCode { get; set; }
        public string OriginalCaptainCode { get; set; }


        public bool IsGhanaRoute { get; set; }
        public string PassportType { get; set; }
        public string PassportId { get; set; }
        public string PlaceOfIssue { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? IssuedDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpiredDate { get; set; }
        public string Nationality { get; set; }
    }
}