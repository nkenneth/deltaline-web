﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transport.WEB.Models.Enum;

namespace Transport.WEB.Models
{
    public class BookedTripsModel
    {
        public int SeatsBooked { get; set; }
        public int AvailableSeats { get; set; }
        public DateTime DepartureDate { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }
        public string DepartureTime { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public string RouteName { get; set; }
        public decimal Revenue { get; set; }
        public int TerminalId { get; set; }
        public string TerminalName { get; set; }
        public int DepartureTerminal { get; set; }
        public int DestinationTerminal { get; set; }
        public bool IsPrinted { get; set; }
        public string IsMPrinted { get; set; }
        public DateTime ManifestPrintedTime { get; set; }
        public VehicleStatus VehicleStatus { get; set; }
        public int TotalCount { get; set; }
        public int? DepartureTerminalId { get; set; }
    }

    public class Customer
    {
      
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string NextOfKinName { get; set; }
        public string NextOfKinPhoneNumber { get; set; }
 
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public bool IsGhanaRoute { get; set; }
        public string PassportType { get; set; }
        public string PassportId { get; set; }
        public string PlaceOfIssue { get; set; }
        public DateTime? IssuedDate { get; set; }
        public DateTime? ExpiredDate { get; set; }

    }
}
