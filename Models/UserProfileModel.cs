﻿using System.ComponentModel.DataAnnotations;

namespace Transport.WEB.Models
{
    public class UserProfileModel
    {
        public string NextOfKin { get; set; }

        public string NextOfKinPhone { get; set; }
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public string ReferralCode { get; set; }
        public string Address { get; set; }
        public string MiddleName { get; set; }
        public string DateJoined { get; set; }
        public string DateOfBirth { get; set; }
    }
}