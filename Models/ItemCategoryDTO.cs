﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class ItemCategoryDTO 
    {
        public string ItemCategoryCode { get; set; }
        public int ItemFamilyID { get; set; }
        public ItemFamilyDTO ItemFamily { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public string CategoryLongDescription { get; set; }
        public string CategoryPictureURL { get; set; }
        public int Id { get; set; }

        public ICollection<InventoryItemDTO> InventoryItems { get; set; }
    }
}
