﻿using System;
using Transport.WEB.Utils;

namespace Transport.WEB.Models
{
    public class VehicleMakeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Date => DateCreated?.ToString(Constants.HumanDateFormat);

        public DateTime? DateCreated { get; set; }
    }
}