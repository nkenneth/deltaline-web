﻿using System;
namespace Transport.WEB.Models
{
    public class WorkshopSummaryModel
    {
        public string Vehicle { get; set; }
        public string WorkshopReason { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string ReleasedBy { get; set; }
        public DateTime? ReleasedDate { get; set; }
    }
}
