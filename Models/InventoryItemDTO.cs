﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Transport.WEB.Models
{
    public class InventoryItemDTO
    {
        public string ItemCode { get; set; }
        public bool IsActive { get; set; }
        public string ItemTypeId { get; set; }
        //public ItemType ItemType { get; set; }
        public string ItemName { get; set; }
        public string itemDescription { get; set; }
        public int ItemCategoryID { get; set; }
        public ItemCategoryDTO ItemCategory { get; set; }
        public string ItemFamilyID { get; set; }
        //public ItemFamily ItemFamily { get; set; }
        public string PictureURL { get; set; }
        public string ItemWeight { get; set; }
        public string ItemUPCCode { get; set; }
        public string ItemColor { get; set; }
        public string ItemDefaultWarehouse { get; set; }
        public string ItemDefaultWarehouseBin { get; set; }
        public string ItemUOM { get; set; }
        public string GLItemSalesAccount { get; set; }
        public string GLItemCOGSAccount { get; set; }
        public string GLItemInventoryAccount { get; set; }
        public decimal Price { get; set; }
        public string ItemPricingCode { get; set; }
        public string VendorID { get; set; }
        public float ReOrderLevel { get; set; }
        public float ReOrderQty { get; set; }
        public decimal LIFO { get; set; }
        public decimal LIFOValue { get; set; }
        public decimal LIFOCost { get; set; }
        public decimal Average { get; set; }
        public decimal AverageValue { get; set; }
        public decimal AverageCost { get; set; }
        public decimal FIFOFIFOValue { get; set; }
        public decimal FIFOCost { get; set; }
        public decimal Expected { get; set; }
        public decimal ExpectedValue { get; set; }
        public decimal ExpectedCost { get; set; }
        public bool IsSerialLotItem { get; set; }
        public bool AllowPurchaseTrans { get; set; }
        public bool AllowSalesTrans { get; set; }
        public bool AllowInventoryTrans { get; set; }
        public int Id { get; set; }
    }
}
