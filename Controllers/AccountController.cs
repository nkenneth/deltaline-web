﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Controllers
{
    public class AccountController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        private readonly ILogger _logger;
        private readonly IHttpContextAccessor _context;
        private IDistributedCache _cache;
         
        public AccountController(IHttpClientFactory httpClient,
            ILogger<AccountController> logger,
            IHttpContextAccessor context)
        {
            _httpClientFactory = httpClient;
            _logger = logger;
            _context = context;
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            if (_context.HttpContext.User.Identity.IsAuthenticated)
                return RedirectOrHome();

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<bool> ResetPassword(string email)
        {
            var httpClient = _httpClientFactory.CreateClient(Constants.ClientNoToken);

            if (email == null)
                return false;

            var response = await httpClient.PostAsJsonAsync<string, bool>($"{Constants.ClientRoutes.AccountResetPassword}/{email}", email);

            return response.Object;
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<bool> ResetNewPassword(string email, string otp, string NPassword, PasswordReset reset)
        {
            var httpClient = _httpClientFactory.CreateClient(Constants.ClientNoToken);

            reset.Code = otp;
            reset.NewPassword = NPassword;
            reset.UserNameOrEmail = email;

            if (reset.UserNameOrEmail == null)
                return false;

            var result = await httpClient.PostAsJsonAsync<PasswordReset, bool>(Constants.ClientRoutes.AccountResetNewPassword, reset);
            if (result.Object == false)
                return false;

            return result.Object;
        }

        private async Task<ClaimsPrincipal> GetUserClaims(TokenModel tokenModel)
        {
            var httpClient = _httpClientFactory.CreateClient(Constants.ClientNoToken);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.Token);

            var responseMessage = await httpClient.GetAsync(Constants.ClientRoutes.AccountGetClaims);

            responseMessage.EnsureSuccessStatusCode();

            var responseText = await responseMessage.Content.ReadAsStringAsync();

            var response = JsonConvert.DeserializeObject<ServiceResponse<List<Claim>>>(responseText, new ClaimConverter());

            var claims = response.Object;

            claims.Add(new Claim("access_token", tokenModel.Token));
            claims.Add(new Claim("refresh_token", tokenModel.RefreshToken));

            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var principal = new ClaimsPrincipal(identity);

            return principal;
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model, string location)
        {
            if (_context.HttpContext.User.Identity.IsAuthenticated)
                return RedirectOrHome();

            if (ModelState.IsValid)
            {
                try
                {

                    var httpClient = _httpClientFactory.CreateClient(Constants.ClientNoToken);

                    var response = await httpClient.PostAsJsonAsync<LoginModel, TokenModel>(Constants.ClientRoutes.Token, model);

                    if (response != null && response.Object != null)
                    {

                        //get profile info with token
                        var principal = await GetUserClaims(response.Object);

                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal,
                            new AuthenticationProperties
                            {
                                IsPersistent = true,
                                AllowRefresh = true,
                                ExpiresUtc = response.Object.Expires,
                                IssuedUtc = DateTime.UtcNow
                            });

                        return RedirectOrHome(location);
                    }
                    else
                    {
                        ModelState.AddModelError("", response.ShortDescription);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);

                    ModelState.AddModelError("", "An error occured during login progress.");
                }
            }
            else
            {
                ModelState.AddModelError("", "Please check your inputs");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Logout()
        {

            if (User.Identity.IsAuthenticated)
            {
                Request.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return Redirect(Constants.URL.LoginPath);
            }

            return Redirect(Constants.URL.Default);
        }

        protected IActionResult RedirectOrHome(string location = null)
        {
            if (string.IsNullOrWhiteSpace(location) || !Url.IsLocalUrl(location))
                return Redirect(Constants.URL.Default);

            return Redirect(location);
        }

        public async Task<IActionResult> Profile()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var profile = await client.GetAsync<UserProfileModel>(Constants.ClientRoutes.AccountGetProfile);

            return View(profile.Object);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProfile(UserProfileModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var result = await client.PostAsJsonAsync<UserProfileModel, bool>(Constants.ClientRoutes.AccountUpdateProfile, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction(nameof(Profile));
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        public async Task<IActionResult> ChangePassword()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var profile = await client.GetAsync<PasswordChangeModel>(Constants.ClientRoutes.AccountGetProfile);

            return View(profile.Object);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(PasswordChangeModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var result = await client.PostAsJsonAsync<PasswordChangeModel, bool>(Constants.ClientRoutes.AccountChangePassword, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction(nameof(Profile));
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }
    }
}