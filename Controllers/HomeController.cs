﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Models.Enum;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;
namespace Transport.WEB.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public HomeController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<IActionResult> Index()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<EmployeeModel>($"{Constants.ClientRoutes.Employee}/getemployeebymail");

            //var dailySalesData = await _client.GetAsync<ServiceResponse<List<ChartDataItem>>>($"{ApiRoutes.Reports}/SalesChartData");
            //ViewBag.SalesChart = dailySalesData.Object;

            if (!response.IsValid || response.Object==null)
            {
                return View(nameof(Index), new EmployeeModel());
            }

            return View(response.Object);
        }
        [HttpGet]
        [Route("getbookingsummary")]
        public async Task<ServiceResponse<BookingSummaryModel>> GetBookingSummaryReports()
        {
            var model = new BookingSummaryModel();
            var serviceResponse = new ServiceResponse<BookingSummaryModel> { };
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                serviceResponse = (ServiceResponse<BookingSummaryModel>)
                    await client.GetAsync<BookingSummaryModel>($"{Constants.ClientRoutes.BookingSummaryUrl}");

                if (serviceResponse.IsValid)
                    return serviceResponse;

            }
            catch (Exception ex)
            {

            }
            return serviceResponse;
        }
        [HttpGet]
        [Route("getsalessummary")]
        public async Task<ServiceResponse<SalesSummaryModel>> GetSalesSummaryReports()
        {
            var model = new SalesSummaryModel();
            var serviceResponse = new ServiceResponse<SalesSummaryModel> { };

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var response = await client.GetAsync<SalesSummaryModel>($"{Constants.ClientRoutes.SalesSummaryUrl}");
                serviceResponse = (ServiceResponse<SalesSummaryModel>)
                    await client.GetAsync<SalesSummaryModel>($"{Constants.ClientRoutes.SalesSummaryUrl}");

                if (serviceResponse.IsValid)
                    return serviceResponse;
            }
            catch (Exception ex)
            {

            }
            return serviceResponse;
        }
    }
}