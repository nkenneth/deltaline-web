﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Models.Enum;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;

namespace Transport.WEB.Areas.Booking.Controllers
{
    [Area("Booking")]
    public class AdvancedBookingController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public AdvancedBookingController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name");

            ViewBag.SearchResult = new GroupedTripsDetail();

            if (!terminals.IsValid)
                return View(new TerminalBookingModel()).WithWarning(terminals.Code + ": " + terminals.ShortDescription);

            return View(new TerminalBookingModel { NumberOfAdults = 1 });

        }

        [HttpPost]
        public async Task<IActionResult> Index(TerminalBookingModel search)
         {
            if (search == null)
                return RedirectToAction(nameof(Index));

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

            if (!terminals.IsValid) {
                return RedirectToAction(nameof(Index)).WithWarning(terminals.Code + ": " + terminals.ShortDescription);
            }

            ViewBag.Terminals = new SelectList(terminals.Object.Items, "Id", "Name");
            var Passporttype = await client.GetAsync<List<PassportTypeModel>>($"{Constants.ClientRoutes.Passport}");
            ViewBag.Passporttype = new SelectList(Passporttype.Object, "Id", "Name");

            ViewBag.SearchResult = new GroupedTripsDetail();

            if (!search.DepartureDate.HasValue || search.DepartureDate?.Date <= DateTime.Today) {
                ModelState.AddModelError(nameof(search.DepartureDate), "Departure Date must be greater than today.");
            }

            if (search.TripType == TripType.Return && (!search.ReturnDate.HasValue || search.ReturnDate?.Date <= DateTime.Today)) {
                ModelState.AddModelError(nameof(search.ReturnDate), "Return Date must be greater than today.");
            }

            if (!ModelState.IsValid)
                return View(search);

            var response = await client.PostAsJsonAsync<TerminalBookingModel, GroupedTripsDetail>(Constants.ClientRoutes.Bookings, search);

            if (!response.IsValid) {
                return View(search).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            ViewBag.SearchResult = response.Object;

            return View(search);
        }

       
        [HttpPost]
        public async Task<IActionResult> CreateBooking(BookingDetailModel booking)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
          
            var routes = booking.Routes;
            List<string> tripList = new List<string>();
            int? routeId = null;
            bool isSub = false;
            bool isSubReturn = false;


            if (booking.PassportType != null)
            {
                booking.PassportTypeId = booking.PassportType;
                int id = Convert.ToInt32(booking.PassportType);
                var url = string.Format(Constants.ClientRoutes.PassportTypeGet, id);
                var result = await client.GetAsync<PassportTypeModel>(url);
                var items = result.Object.Name;
                booking.PassportType = items.ToString();
                booking.IsGhanaRoute = true;
               
            }


            int? RouteIdReturn = null;
            if (routes.Contains(";")) {
                foreach (var trip in booking.Routes.Split(';')) {
                    tripList.Add(trip);
                }
                var toTripDetail = tripList.FirstOrDefault().Split(':');
                routeId = Convert.ToInt32(toTripDetail[0]);
                isSub = Convert.ToBoolean(toTripDetail[1]);
                var returnTripDetail = tripList.LastOrDefault().Split(':');
                RouteIdReturn = Convert.ToInt32(returnTripDetail[0]);
                isSubReturn = Convert.ToBoolean(returnTripDetail[1]);
            }
            else {
                var toTripDetail = routes.Split(':');
                routeId = Convert.ToInt32(toTripDetail[0]);
                isSub = Convert.ToBoolean(toTripDetail[1]);
            }
            booking.RouteId = routeId;
            booking.isSub = isSub;
            booking.isSubReturn = isSubReturn;
            booking.RouteIdReturn = RouteIdReturn;
            //var seatRegistrations = JsonConvert.DeserializeObject<Dictionary<int, int[]>>(booking?.SeatRegistrations);
            if (booking != null) {
                booking.BookingType = BookingType.Advanced;

                booking.BookingStatus = BookingStatus.Approved;
            }

            if (string.IsNullOrWhiteSpace(booking.Email)) {
                booking.Email = "no-email@deltaline.com";
            }

            var response = await client.PostAsJsonAsync<BookingDetailModel, BookingResultModel>($"{Constants.ClientRoutes.PostBookings}", booking);

            if (ModelState.IsValid) {
                if (response.ValidationErrors.Count() < 1) {
                    return RedirectToAction(nameof(ViewReceipt), new { response.Object.BookingReferenceCode })
                        .WithSuccess("Your booking with refcode: " +
                                     response.Object.BookingReferenceCode + " was successful!");
                }

                if (response.Code == "Seat(s) no Longer Available") {
                    return RedirectToAction(nameof(Index))
                         .WithError(response.Code);
                }
                else {
                    return RedirectToAction(nameof(Index))
                          .WithError(response.ShortDescription);
                }
            }
            return RedirectToAction(nameof(Index))
                     .WithError(response.ShortDescription);
        }

        [Route("viewreceipt")]
        public async Task<IActionResult> ViewReceipt(string BookingReferenceCode)
        {
            ViewBag.refcode = BookingReferenceCode;
            if (ModelState.IsValid) {
                var bookingRef = BookingReferenceCode.Trim();
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.BookingDetails, bookingRef);

                var ticketresponse = await client.GetAsync<List<SeatManagement>>(url);

                if (!ticketresponse.IsValid) {
                    return RedirectToAction(nameof(Index)).WithError(ticketresponse.Code + ": " + ticketresponse.ShortDescription);
                }
                if (ticketresponse.Object is null) {
                    return RedirectToAction(nameof(Index)).WithError("No Ticket with the Refcode " + BookingReferenceCode + " was found");

                }
                return View(nameof(ViewReceipt), ticketresponse.Object);
            }
            return View(nameof(ViewReceipt), new List<SeatManagement>());
        }

        [HttpGet, Route("destination/{departureTerminalId}")]
        public async Task<IActionResult> GetDestinationTerminals(int departureTerminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientNoToken);

            var response = await client.GetAsync<ServiceResponse<List<TerminalModel>>>(string.Format(Constants.ClientRoutes.RouteGetDestinationTerminals, departureTerminalId));

            if (!response.IsValid) {
                return new JsonResult(new
                {
                    response.Code,
                    Message = response.ShortDescription,
                    IsSubmit = false,
                    RedirectTo = Url.Action(nameof(Index))
                }).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return new JsonResult(new
            {
                Terminals = response.Object
            });
        }

        public async Task<IActionResult> ReOpenBus()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name"); ;

            return View();
        }

        //[Route("TerminalRoutes/{terminalId}")]
        public async Task<List<RouteModel>> TerminalRoutes(int terminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var result = await client.GetAsync<List<RouteModel>>(string.Format(Constants.ClientRoutes.TerminalRoutes, terminalId));

            var items = result.Object;

            return items;
        }

        public async Task<List<AvailabileTripDetail>> RouteBuses(int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<AvailabileTripDetail>>($"{Constants.ClientRoutes.RouteBuses}/{routeId}");
            var items = response.Object.ToList();
            return items;
        }

        public async Task<bool> ReOpenBuss(Guid vehicleTripId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var model = new Manifest
            {
                VehicleTripRegistrationId = vehicleTripId
            };

            var response = await client.PutAsync<Manifest, bool>(Constants.ClientRoutes.OpenManifest, model);

            var items = response.Object;

            return items;
        }

        public async Task<IActionResult> CreateHire(Guid? id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Name");

            var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.AvailableVehicles);
            ViewBag.Vehicles = new SelectList(vehicles.Object, "Id", "Details");
            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name");

            var terminal = await client.GetAsync<TerminalModel>(Constants.ClientRoutes.LoginInTerminal);
            var userItem = terminal.Object;
            var userResponse = await client.GetAsync<List<HireBusDTO>>($"{Constants.ClientRoutes.HireABus}/GetByTerminalId/{userItem.Id}");
            var userItems = userResponse.Object.Where(h => h.IsManifestPrinted == false).ToList();
            ViewData["UserItems"] = userItems;
            
            if (id == null)
                return View();

            var response = await client.GetAsync<HireBusDTO>($"{Constants.ClientRoutes.HireABus}/Get/{id}");

            var item = response.Object;

            if (response.ValidationErrors.Count() > 0)
            {
                return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(item).WithSuccess(response.Code + ": " + response.ShortDescription);
        }

        [HttpPost]
        public async Task<IActionResult> CreateHire(HireBusDTO query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.PostAsJsonAsync<HireBusDTO, bool>($"{Constants.ClientRoutes.HireABus}/Add", query);

            var items = response.Object;

            if (response.ValidationErrors.Count() > 0)
            {
                return RedirectToAction("CreateHire").WithWarning(response.Code + ": " + response.ShortDescription);
                //return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            var terminal = await client.GetAsync<TerminalModel>(Constants.ClientRoutes.LoginInTerminal);
            var userItem = terminal.Object;
            var userResponse = await client.GetAsync<List<HireBusDTO>>($"{Constants.ClientRoutes.HireABus}/GetByTerminalId/{userItem.Id}");
            var userItems = userResponse.Object.Where(h => h.IsManifestPrinted == false).ToList();
            ViewData["UserItems"] = userItems;

            return RedirectToAction("CreateHire");
            //return View();
        }
        [Obsolete]
        public async Task<IActionResult> GetHireForUser()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var terminal = await client.GetAsync<TerminalModel>(Constants.ClientRoutes.LoginInTerminal);
            var item = terminal.Object;
            var response = await client.GetAsync<List<HireBusDTO>>($"{Constants.ClientRoutes.HireABus}/GetByTerminalId/{item.Id}");
            var items = response.Object;
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.Code + ": " + response.ShortDescription);
        }

        public async Task<IActionResult> HirePassengers(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            ViewBag.Id = id;
            var passengers = await client.GetAsync<List<HirePassengerDTO>>($"{Constants.ClientRoutes.HirePassenger}/Get/{id}");
            var items = passengers.Object;
            var hirebusResonse = await client.GetAsync<HireBusDTO>($"{Constants.ClientRoutes.HireABus}/Get/{id}");
            var hirebusPrinted = hirebusResonse.Object?.IsManifestPrinted;
            if(hirebusPrinted == true) 
            {
                ViewBag.IsManifestPrinted = true;
                return RedirectToAction("CreateHire");
            }
            if (passengers.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(passengers.Code + ": " + passengers.ShortDescription);
            }
            return View(items).WithSuccess(passengers.Code + ": " + passengers.ShortDescription);
        }

        [HttpPost]
        public async Task<IActionResult> HirePassengers(HirePassengerDTO query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var response = await client.PostAsJsonAsync<HirePassengerDTO, bool>($"{Constants.ClientRoutes.HirePassenger}/Add", query);
            var item = response.Object;
            ViewBag.Show = true;
            if (response.ValidationErrors.Count() > 0)
            {
                return RedirectToAction("HirePassengers").WithWarning(response.Code + ": " + response.ShortDescription);
                //return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return RedirectToAction("HirePassengers").WithSuccess(response.Code + ": " + response.ShortDescription);
            //return View(item).WithSuccess(response.Code + ": " + response.ShortDescription);
        }
        public async Task<DriverModel> GetDriver(string registrationNo)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var regValue = registrationNo.Split('(');

            var driverDetails = await client.GetAsync<DriverModel>(string.Format(Constants.ClientRoutes.GetDriverByVehicleId, registrationNo));

            var item = driverDetails.Object;

            item.Picture = string.Format("/images/{0}", item.Picture);

            return item;
        }

        public async Task<IActionResult> CreatePrintManifest(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var regValue = registrationNo.Split('(');

            var manifest = await client.GetAsync<HireManifestViewModel>($"{Constants.ClientRoutes.HirePassenger}/PrintManifest/{id}");

            if (!manifest.IsValid)
            {
                return View(new ManifestViewModel());
            }

            return View(manifest.Object);

        }

        public async Task<HirePassengerDTO> GetPassenger(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var regValue = registrationNo.Split('(');

            var passenger = await client.GetAsync<HirePassengerDTO>($"{Constants.ClientRoutes.HirePassenger}/GetPassenger/{id}");

            return passenger.Object;

        }

        public async Task<bool> UpdatePassenger(HirePassengerDTO query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.PutAsync<HirePassengerDTO, bool>($"{Constants.ClientRoutes.HirePassenger}/Update", query);

            return response.Object;
        }

        public async Task<bool> DeletePass(int Id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.DeleteAsync<bool>($"{Constants.ClientRoutes.HirePassenger}/DeletePassenger/{Id}");

            return response.Object;
        }
    }
}