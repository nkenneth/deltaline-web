﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Models.Enum;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;

namespace Transport.WEB.Areas.Booking.Controllers
{
    [Area("Booking")]
    [Route("TerminalBooking")]
    public class TerminalBookingController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IHostingEnvironment _appEnvironment;

        public TerminalBookingController(IHttpClientFactory httpClientFactory, IHostingEnvironment hostingEnvironment)
        {
            _httpClientFactory = httpClientFactory;
            _appEnvironment = hostingEnvironment;
        }
        [Route("Terminals")]
        public async Task<IActionResult> Terminals()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var result = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            var employeesterminals = await client.GetAsync<TerminalModel>(Constants.ClientRoutes.LoginInTerminal);

          return View(result.Object);
        }

        [Route("TerminalRoutes/{terminalId}")]
        public async Task<IActionResult> TerminalRoutes(int terminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var employeesterminals = await client.GetAsync<TerminalModel>(Constants.ClientRoutes.LoginInTerminal);
            var result = await client.GetAsync<List<RouteModel>>(string.Format(Constants.ClientRoutes.TerminalRoutes, employeesterminals?.Object?.Id ));
            return View(result?.Object);
        }

        [Route("RouteBuses/{routeId}")]
        public async Task<IActionResult> RouteBuses(int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<AvailabileTripDetail>>($"{Constants.ClientRoutes.RouteBuses}/{routeId}");
            ViewBag.RouteId = routeId;
            if (!response.IsValid)
            {
                return View(new List<AvailabileTripDetail>());
            }
            return View(response.Object);
        }

        [Route("TerminalRoutes")]
        public async Task<IActionResult> TerminalRoutes()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var result = await client.GetAsync<List<RouteModel>>(Constants.ClientRoutes.TerminalRoutes);
            //var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

            return View(result.Object ?? new List<RouteModel>());
        }

        [Route("RouteTrips/{routeId}")]
        public async Task<IActionResult> RouteBus(int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<AvailabileTripDetail>>($"{Constants.ClientRoutes.RouteBuses}/{routeId}");

            if (!response.IsValid)
            {
                return View(new List<AvailabileTripDetail>());
            }
            return View(response.Object);
        }


        //[Route("getterminals")]
        //public async Task<IActionResult> GetTerminals()
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

        //    return View(terminals.Object.Items ?? new List<TerminalModel>());
        //}

        //[Route("terminalroutes")]
        //public async Task<IActionResult> TerminalRoutes()
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

        //    return View(terminals.Object.Items ?? new List<TerminalModel>());
        //}

        //[Route("craetephysicalbus")]
        //[HttpPost]
        //public async Task<IActionResult> CreateBus(TerminalBookingModel model)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

        //    var response = await client.PostAsync(model, $"{ApiRoutes.VehicleTripRegistration}/createphysicalbus");


        //    if (!response.IsValid)
        //    {
        //        return new JsonResult(new
        //        {
        //            response.Code,
        //            Message = response.ShortDescription,
        //            RedirectTo = Url.Action(nameof(CreatePhysicalBus))
        //        }).WithSuccess(response.ShortDescription);
        //    }
        //    return RedirectToAction(nameof(CreatePhysicalBus)).WithSuccess("Physical Bus Created Successfully");

        //}

        [HttpGet]
        public async Task<IActionResult> CreateBus()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var routes = await client.GetAsync<List<RouteModel>>(Constants.ClientRoutes.StaffRoutes);

            ViewBag.Routes = new SelectList(routes.Object, "Id", "Name");

            var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.VehiclesInTerminal);
            ViewBag.Vehicles = new SelectList(vehicles.Object, "Id", "Details");

            var trips = await client.GetAsync<PagedRecordModel<TripModel>>(Constants.ClientRoutes.Trips);
            ViewBag.Trips = new SelectList(trips.Object.Items, "Id", "DepartureTime");

            var drivers =
              await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);

            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Name");

            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("pageNumber", "1"),
                new KeyValuePair<string, string>("pageSize", "0"),
                new KeyValuePair<string, string>("searchTerm", "*")
            };

            //var allCaptains =
            //        await _client.GetAsync<PagedServiceResponse<Captain>>($"{ApiRoutes.Captains}", parameters.ToArray());
            //ViewBag.AllCaptains = new SelectList(allCaptains.Object.Items.Where(c => c.CaptainType != Models.Enums.CaptainType.VirtualCaptain && c.Active).ToList(), "CaptainCode", "CaptainDetails");

            ViewBag.Title = "Create A Bus";

            return View(new TerminalBookingModel());
        }

        [AcceptVerbs("GET", "POST")]
        [Route("routetrips/{routeId}")]
        public async Task<IActionResult> GetRouteTrips(int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<TripModel>>($"{Constants.ClientRoutes.RouteTrips}/{routeId}");

            if (!response.IsValid)
            {
                return new JsonResult(new
                {
                    response.Code,
                    Message = response.ShortDescription,
                    IsSubmit = false,
                    RedirectTo = Url.Action(nameof(CreateBus))
                }).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return new JsonResult(new
            {
                Trips = response.Object
            });
        }


        [Route("CreateBusPost")]
        [HttpPost]
        public async Task<IActionResult> CreateBusPost(TerminalBookingModel terminalbooking)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.PostAsJsonAsync<TerminalBookingModel, bool>($"{Constants.ClientRoutes.VehicleTripRegistration}/CreateBus", terminalbooking);

            if (!response.IsValid)
            {
                return new JsonResult(new
                {
                    response.Code,
                    Message = response.ShortDescription,
                    RedirectTo = Url.Action(nameof(CreateBus))
                }).WithSuccess(response.ShortDescription);
            }
            return RedirectToAction(nameof(CreateBus)).WithSuccess("Physical Bus Created Successfully");

        }

        [Route("GetBusDriver")]
        [HttpGet]
        public async Task<VehicleDTO> GetBusDriver(string registrationNo)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var regValue = registrationNo.Split('(');

            var driverDetails = await client.GetAsync<VehicleDTO>(string.Format(Constants.ClientRoutes.GetVehicleByIdWithDriverName, registrationNo));

            var item = driverDetails.Object;

            return item;
        }

        [Route("GetDriver")]
        [HttpGet]
        public async Task<DriverModel> GetDriver(string registrationNo)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var regValue = registrationNo.Split('(');

            var driverDetails = await client.GetAsync<DriverModel>(string.Format(Constants.ClientRoutes.GetDriverByVehicleId, registrationNo));

            var item = driverDetails.Object;

            item.Picture = string.Format("/images/{0}", item.Picture);

            return item;
        }


        [Route("Booking")]
        public async Task<IActionResult> Booking(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            ViewBag.RouteId = manifest.RouteId;
            ViewBag.TicketToPrintId = manifest.MainBookerId;
           
            TicketSaleModel ticketSale = new TicketSaleModel()
            {
                Booking = new BookingDetailModel(),
                Manifest = new ManifestViewModel()
            };
            ticketSale.Manifest.Passengers = new List<SeatManagement>();

            //Subroutes
            SubRouteModel subroute = new SubRouteModel()
            {
                Id = null,
                Name = "Select Stop"
            };


            var Passporttype = await client.GetAsync<List<PassportTypeModel>>($"{Constants.ClientRoutes.Passport}");
            ViewBag.Passporttype = new SelectList(Passporttype.Object, "Id", "Name");



            var subrouteresponse = await client.GetAsync<List<SubRouteModel>>($"{Constants.ClientRoutes.Subroute}/GetByRouteId/{manifest.RouteId}");

            ticketSale.Booking.Subroutes = subrouteresponse.IsValid ? subrouteresponse.Object : new List<SubRouteModel>();
            ticketSale.Booking.Subroutes.Insert(0, subroute);
            ViewBag.TotalSeat = 0;
            if (ModelState.IsValid)
            {
                var response = await client.PostAsJsonAsync<Manifest, bool>($"{Constants.ClientRoutes.Manifests}/Add", manifest);

                if (response.IsValid)
                {
                    var manifestresponse = await client.GetAsync<ManifestViewModel>($"{Constants.ClientRoutes.Manifests}/GetByVehicleTripId/{manifest.VehicleTripRegistrationId}");

                    //if (manifestresponse.Object.ClashingSeats.Count > 0)
                    //{
                    //    int j = 0;
                    //    foreach (var seat in manifestresponse.Object.BookSeat)
                    //    {
                    //        if (manifestresponse.Object.ClashingSeats.Contains(seat))
                    //            manifestresponse.Object.SeatBooking.Add(new Tuple<int, int, bool>(seat, manifestresponse.Object.BookingTypes[j], manifestresponse.Object.TicketPrintStatus[j]));

                    //        j++;
                    //    }
                    //    if (!manifestresponse.IsValid)
                    //    {
                    //        return RedirectToAction(nameof(RouteBuses), new { manifest.RouteId }).WithError(manifestresponse.Code + ": " + manifestresponse.ShortDescription);
                    //    }
                    //    ticketSale.Manifest = manifestresponse.Object;
                    //    ticketSale.Booking.VehicleTripRegistrationId = manifestresponse.Object.VehicleTripRegistrationId;
                    //    ticketSale.Booking.RouteId = manifest.RouteId;
                    //    ticketSale.Booking.TotalSeats = manifestresponse.Object.TotalSeats;
                    //    ticketSale.Booking.ManifestPrinted = manifestresponse.Object.ManifestPrinted;
                    //    ticketSale.Booking.Amount = manifestresponse.Object.Amount;
                    //    ticketSale.Booking.Discount = (decimal)0.0;
                    //    ticketSale.Booking.RemainingSeat = manifestresponse.Object.RemainingSeat;
                    //    ticketSale.Booking.seatBooking = manifestresponse.Object.SeatBooking;
                    //    ticketSale.Booking.Email = "noemail@gmail.com";
                    //    ticketSale.Booking.Update = false;

                    //    return PartialView(nameof(ResolveSeatClash), ticketSale).WithError("The Manifest Contains Some Clashing Seats, Please Resolve to Continue!");
                    //}
                    //else
                    //{
                        int i = 0;
                        foreach (var seat in manifestresponse.Object.BookSeat)
                        {
                            manifestresponse.Object.SeatBooking.Add(new Tuple<int, int, bool>(seat, manifestresponse.Object.BookingTypes[i], manifestresponse.Object.TicketPrintStatus[i]));
                            i++;
                        }
                        if (!manifestresponse.IsValid)
                        {
                            return RedirectToAction(nameof(RouteBuses), new { manifest.RouteId }).WithError(manifestresponse.Code + ": " + manifestresponse.ShortDescription);
                        }
                        ticketSale.Manifest = manifestresponse.Object;
                        ticketSale.Booking.VehicleTripRegistrationId = manifestresponse.Object.VehicleTripRegistrationId;
                        ticketSale.Booking.RouteId = manifest.RouteId;
                        ticketSale.Booking.TotalSeats = manifestresponse.Object.TotalSeats;
                        ticketSale.Booking.ManifestPrinted = manifestresponse.Object.IsPrinted;
                        ticketSale.Booking.Amount = manifestresponse.Object.Amount;
                        ticketSale.Booking.Discount = (decimal)0.0;
                        ticketSale.Booking.RemainingSeat = manifestresponse.Object.RemainingSeat;
                        ticketSale.Booking.seatBooking = manifestresponse.Object.SeatBooking;
                        ticketSale.Booking.Email = "noemail@gmail.com";
                        ticketSale.Booking.Update = false;
                    //ticketSale.Manifest.CommisionSource = manifestresponse.Object.
                    //ticketSale.Booking.PassportId = manifest.;



                    //    return PartialView(nameof(Booking), ticketSale);
                    //}

                    return PartialView(nameof(Booking), ticketSale);
                }

                return PartialView(nameof(Booking), ticketSale).WithError(response.ShortDescription);
            }

            return PartialView(nameof(Booking), ticketSale);


        }

        [Route("swapvehicle")]
        public async Task<IActionResult> Swapvehicle()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<VehicleTripRegistration>>($"{Constants.ClientRoutes.VehicleTripRegistration}/GetTodayVehicleTripRegistrationsForterminal");

            if (!response.IsValid)
            {
                return View().WithError(response.Code + ": " + response.ShortDescription);
            }

            ViewBag.VehicleTrips = new SelectList(response.Object ?? new List<VehicleTripRegistration>(), "VehicleTripRegistrationId", "VehicleCaptaindetails");


            var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.VehiclesInTerminal);
            ViewBag.Vehicles = new SelectList(vehicles.Object, "Id", "Details");

            var driverResponse =
               await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);


            ViewBag.Drivers = new SelectList(driverResponse.Object.Items, "Code", "Name");

            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("pageNumber", "1"),
                new KeyValuePair<string, string>("pageSize", "0"),
                new KeyValuePair<string, string>("searchTerm", "*")
            };
            return View(new SwapVehicle());
        }



        [HttpPost, Route("swapvehicle")]
        public async Task<IActionResult> Swapvehicle(SwapVehicle swapvehicle)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<VehicleTripRegistration>>($"{Constants.ClientRoutes.VehicleTripRegistration}/GetTodayVehicleTripRegistrationsForterminal");

            if (!response.IsValid)
            {
                return View().WithError(response.Code + ": " + response.ShortDescription);
            }

            ViewBag.VehicleTrips = new SelectList(response.Object ?? new List<VehicleTripRegistration>(), "VehicleTripRegistrationId", "VehicleCaptaindetails");


            var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.VehiclesInTerminal);
            ViewBag.Vehicles = new SelectList(vehicles.Object, "Id", "Details");

            var driverResponse =
               await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);




            ViewBag.Drivers = new SelectList(driverResponse.Object.Items, "Code", "Name");


            var swapresponse = await client.PutAsync<SwapVehicle, bool>($"{Constants.ClientRoutes.Booking}/SwapVehicle", swapvehicle);


            if (!swapresponse.IsValid)
            {
                return RedirectToAction(nameof(Swapvehicle)).WithError(swapresponse.Code + ": " + swapresponse.ShortDescription);
            }

            return RedirectToAction(nameof(Swapvehicle)).WithSuccess("Swap Successful");

        }


        [Route("changeBus")]
        public async Task<IActionResult> changeBus(Guid vehicleTripRegistrationId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var manifestresponse = await client.GetAsync<Models.VehicleTripRegistration>($"{Constants.ClientRoutes.Manifests}/getvehicletrip/{vehicleTripRegistrationId}");

            if(manifestresponse.Object.CurrentModelId==null)
            {
                manifestresponse.Object.OriginalModelId = manifestresponse.Object.VehicleModelId;

            }
            else
            {
                manifestresponse.Object.OriginalModelId = manifestresponse.Object.CurrentModelId;

            }

            return PartialView(manifestresponse.Object);


         
        }
        [Route("changeRoute")]
        [HttpGet]
        public async Task<List<RouteModel>> changeRoute()
        {
            //Guid vehicleTripId = new Guid();
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var profile = await client.GetAsync<UserProfileModel>(Constants.ClientRoutes.AccountGetProfile);

            var terminalId = await client.GetAsync<int>(string.Format("{0}/{1}", Constants.ClientRoutes.GetEmployeeTerminalId, profile.Object.Email));

            var routesRequest = await client.GetAsync<List<RouteModel>>(string.Format(Constants.ClientRoutes.RoutesGetByTerminalId, terminalId.Object));
            //var manifestresponse = await client.GetAsync<Models.VehicleTripRegistration>($"{Constants.ClientRoutes.Manifests}/getvehicletrip/{vehicleTripRegistrationId}");
            //var routesRequest = await client.GetAsync<PagedRecordModel<RouteModel>>(Constants.ClientRoutes.Routes);
            //var getNewRoute = await client.GetAsync<RouteModel>(string.Format(Constants.ClientRoutes.GetNewRouteFareByModel, rou);
            //ViewBag.RouteId = new SelectList(routesRequest.Object, "Id", "Name");

            //var ticketresponse = await client.GetAsync<decimal?>($"{Constants.ClientRoutes.Manifests}/GetManifestMainTripFare/{vehicleTripId}");

            var items = routesRequest.Object;
            return items;

        }

        [Route("UpdateTrip")]
        [HttpGet]
        public async Task<bool> UpdateTrip(NewTripIdDTO newTrip)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var submit = await client.PutAsync<NewTripIdDTO, bool>($"{Constants.ClientRoutes.UpdateVehicleTripId}", newTrip);
            //var manifestUpdate = await client.GetAsync<ManifestDTO>(string.Format(Constants.ClientRoutes.UpdateManifest,newTrip.newTripId));

            var newFare = await GetFareForRoute(newTrip.RouteId, newTrip.newTripId);

            var updateTripFare = await client.PutAsync<NewTripIdDTO, bool>(string.Format(Constants.ClientRoutes.UpdateManifestAmount, newFare), newTrip);

            return submit.Object;
            //return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId });
        }

        [Route("GetNewTripDetails/routeId/modelId")]
        [HttpGet]
        public async Task<List<TripModel>> GetNewTripDetails(int routeId)
        {
            //int VehiclemodelId = 0;
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            //var models = await client.GetAsync<PagedResponse<VehicleModel>>(Constants.ClientRoutes.VehicleModels);
            //var model = models.Object.Items;
            //foreach (var item in model)
            //{
            //    if (item.VehicleModelName == modelId)
            //        VehiclemodelId = item.VehicleModelId;
            //}
            //var tripId = await client.GetAsync<TripModel>($"{Constants.ClientRoutes.RouteTrips ");

            //ViewBag.RouteId = new SelectList(routesRequest.Object, "Id", "Name");
            //var dateDropdown = await client.GetAsync<List<TripModel>>(string.Format(Constants.ClientRoutes.RouteTrips, ticketDetails.RouteId));
            var dateDropdown = await client.GetAsync<List<TripModel>>($"{Constants.ClientRoutes.RouteTrips}/{routeId}");
            var items = dateDropdown.Object;

            //var tripFare = await client.GetAsync<decimal?>(string.Format(Constants.ClientRoutes.GetNewRouteFareByModel, routeId, VehiclemodelId));
            List<TripModel> trips = new List<TripModel>();

            foreach (var item in items)
            {
                    trips.Add(item);
            }

            return trips;
        }

        [Route("GetNewFare")]
        [HttpGet]
        public async Task<decimal?> GetNewFare(GetTripFareDTO getTripFare)
        {
            var items = await GetFareForRoute(getTripFare.routeId, getTripFare.tripId);
            return items;
        }
      
        public async Task<decimal?> GetFareForRoute(int routeid, Guid tripid)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var dateDropdown = await client.GetAsync<List<TripModel>>($"{Constants.ClientRoutes.RouteTrips}/{routeid}");

            var obj = dateDropdown.Object;
            int? modelId = 0;

            foreach (var item in obj)
            {
                if (item.Id == tripid)
                    modelId = item.VehicleModelId;
            }
            var tripFare = await client.GetAsync<decimal?>(string.Format(Constants.ClientRoutes.GetNewRouteFareByModel, routeid, modelId));

            var items = tripFare.Object;
            return items;
        }


        //[Route("UpdateTripId")]
        //[HttpPut]
        //public async Task<IActionResult> UpdateTripId(VehicleTripRegistration vehicleTrip)
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


        //}

        [Route("UpdateBusType")]

        public async Task<IActionResult> UpdateBusType(VehicleTripRegistration manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var response = await client.PutAsync<VehicleTripRegistration, bool>($"{Constants.ClientRoutes.Manifests}/UpdateBusType", manifest);


           
            if (!response.IsValid)
            {

                return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError(response.Code + ": " + response.ShortDescription);

            }



            return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId });
        }



        [Route("getfare")]
        [HttpGet]
        public async Task<decimal?> GetFare(string subRoute)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            int? subrouteId = null;
            Guid vehicleTripId = new Guid();
            if (subRoute != null)
            {
                var subroute = subRoute.Split(',');
                subrouteId = Convert.ToInt32(subroute[0]);
                vehicleTripId = Guid.Parse(subroute[1]);

            }

            if (ModelState.IsValid)
            {

                var ticketresponse = await client.GetAsync<decimal?>($"{Constants.ClientRoutes.Manifests}/GetManifestTripFare/{subrouteId}/{vehicleTripId}");

                if (!ticketresponse.IsValid)
                {
                    return ticketresponse.Object;
                }

                return ticketresponse.Object;

            }

            return 0;
        }

        [Route("getmainfare")]
        [HttpGet]
        public async Task<decimal?> GetMainFare(string vehicletrip)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            Guid vehicleTripId = new Guid();
            if (vehicletrip != null)
            {

                vehicleTripId = Guid.Parse(vehicletrip);

            }

            if (ModelState.IsValid)
            {

                var ticketresponse = await client.GetAsync<decimal?>($"{Constants.ClientRoutes.Manifests}/GetManifestMainTripFare/{vehicleTripId}");

                if (!ticketresponse.IsValid)
                {
                    return ticketresponse.Object;
                }

                return ticketresponse.Object;

            }

            return 0;
        }


        [Route("getpassengerfare")]
        [HttpGet]
        public async Task<decimal?> GetPassengerFare(string passengerInfo)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            if (ModelState.IsValid)
            {

                var ticketresponse = await client.GetAsync<decimal?>($"{Constants.ClientRoutes.Manifests}/GetManifestPassengerFare/{passengerInfo}");

                if (!ticketresponse.IsValid)
                {
                    return ticketresponse.Object;
                }

                return ticketresponse.Object;

            }

            return 0;
        }


        [Route("getdetailsbyphone")]
        [HttpGet]
        public async Task<Customer> GetDetailsByPhone(string phone)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (ModelState.IsValid)
            {

                var ticketresponse = await client.GetAsync<Customer>($"{Constants.ClientRoutes.Booking}/getcustomer/{phone}");

                if (!ticketresponse.IsValid)
                {
                    return ticketresponse.Object;
                }

                return ticketresponse.Object;

            }

            return new Customer();
        }


        [Route("addrefcode")]
        [HttpPost]
        public async Task<IActionResult> AddRefcode(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            ViewBag.RouteId = manifest.RouteId;
            ViewBag.TripId = manifest.VehicleTripRegistrationId;
            var tripresponse = await client.GetAsync<VehicleTripRegistration>($"{Constants.ClientRoutes.VehicleTripRegistration}/{manifest.VehicleTripRegistrationId}");

            if (tripresponse != null)
            {
                manifest.RouteId = tripresponse.Object.RouteId;
                ViewBag.RouteId = tripresponse.Object.RouteId;
            }
            List<Seat> seats = new List<Seat>();
            TicketSaleModel ticketSale = new TicketSaleModel()
            {
                Booking = new BookingDetailModel(),
                Manifest = new ManifestViewModel()

            };

            SubRouteModel subroute = new SubRouteModel()
            {
                Id = null,
                Name = "Select Stop"
            };
            var subrouteresponse = await client.GetAsync<List<SubRouteModel>>($"{Constants.ClientRoutes.Subroute}/GetByRouteId/{manifest.RouteId}");

            ticketSale.Booking.Subroutes = subrouteresponse.IsValid ? subrouteresponse.Object : new List<SubRouteModel>();
            ticketSale.Booking.Subroutes.Insert(0, subroute);


            var refcoderesponse = await client.PutAsync<Manifest, bool>($"{Constants.ClientRoutes.Booking}/addrefcodetobooking", manifest);


            var manifestresponse = await client.GetAsync<ManifestViewModel>($"{Constants.ClientRoutes.Manifests}/GetByVehicleTripId/{manifest.VehicleTripRegistrationId}");

            if (!manifestresponse.IsValid)
            {
                return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError(manifestresponse.ShortDescription);
            }


            return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId });

        }


        [Route("AssignRouteBuses/{routeId}")]
        public async Task<IActionResult> AssignRouteBuses(int routeId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<AvailabileTripDetail>>($"{Constants.ClientRoutes.Route}/route/virtualbuses/{routeId}");

            if (!response.IsValid)
            {
                return RedirectToAction(nameof(Terminals)).WithError(response.Code + ": " + response.ShortDescription);
            }

            ViewBag.RouteTrips = response.Object;

            return View(response.Object);

        }




        [Route("AssignPost")]
        [HttpPost]
        public async Task<IActionResult> AssignPost(VehicleTripRegistration tripDetail)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.PutAsync<VehicleTripRegistration, bool>($"{Constants.ClientRoutes.VehicleTripRegistration}", tripDetail);

            if (!response.IsValid)
            {
                return RedirectToAction(nameof(AssignRouteBuses), new { tripDetail.RouteId });
            }

            return RedirectToAction(nameof(AssignRouteBuses), new { tripDetail.RouteId });
        }

        [Route("Assign")]
        [HttpGet]
        public async Task<IActionResult> Assign(Guid vehicleTripRegistrationId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<VehicleTripRegistration>($"{Constants.ClientRoutes.VehicleTripRegistration}/{vehicleTripRegistrationId}");

            var modelType = 0;

            if (!response.IsValid)
            {
                return RedirectToAction(nameof(Terminals)).WithError(response.Code + ": " + response.ShortDescription);
            }

            if (response.Object.VehicleModelId != null)
            {
                modelType = response.Object.VehicleModelId.GetValueOrDefault();
            }

            await PopulateVehiclesDropdownAsync(modelType);

            return PartialView(response.Object);
        }
        private async Task PopulateVehiclesDropdownAsync(int modelType)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.VehiclesInTerminal);
            ViewBag.Vehicles = new SelectList(vehicles.Object, "RegistrationNumber", "Details");

      
            var drivers =
              await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);

            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Name");
            var parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("pageNumber", "1"),
                new KeyValuePair<string, string>("pageSize", "0"),
                new KeyValuePair<string, string>("searchTerm", "*")
            };

           
            ViewBag.AllDrivers = new SelectList(drivers.Object.Items, "Code", "Name");
        }

        [Route("managerroute")]
        public async Task<IActionResult> ManagerRoute()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<RouteModel>>($"{Constants.ClientRoutes.Route}/terminals/managerroutes");

            if (!response.IsValid)
            {
                return RedirectToAction(nameof(Terminals)).WithError(response.Code + ": " + response.ShortDescription);
            }

            ViewBag.Routes = response.Object;

            return View(new List<TerminalBookingModel>());
        }

        [HttpPost, Route("createbooking")]
        public async Task<IActionResult> CreateBooking(BookingDetailModel booking)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var seatRegistrations = booking.VehicleTripRegistrationId + ":" + booking.SeatNumber;
            booking.SeatRegistrations = seatRegistrations;
            //if (booking.SubrouteId != null)
            //{
            //    routeId = booking.SubrouteId.GetValueOrDefault();
            //    isSub = true;
            //}
            //else
            //{
            //    routeId = booking.RouteId;
            //}

            //booking.RouteId = routeId;
            //booking.isSub = isSub;

            if (booking != null)
            {
                booking.BookingType = BookingType.Terminal;

                booking.BookingStatus = BookingStatus.Approved;
            }

            if (booking.Email == null)
            {
                booking.Email = "No Email";
            }

            if (booking.PassportType != null)
            {
                int id = Convert.ToInt32(booking.PassportType);
                var url = string.Format(Constants.ClientRoutes.PassportTypeGet, id);
                var result = await client.GetAsync<PassportTypeModel>(url);
                var items = result.Object.Name;
                booking.PassportType = items.ToString();
                booking.IsGhanaRoute = true;
            }


            if (!booking.Update)
            {
                var response = await client.PostAsJsonAsync<BookingDetailModel, BookingResultModel>($"{Constants.ClientRoutes.PostBookings}", booking);

                if (response.IsValid)
                {
                    return RedirectToAction(nameof(Booking), new { booking.VehicleTripRegistrationId, booking.RouteId, response.Object.MainBookerId });
                }

                return RedirectToAction(nameof(Booking), new { booking.VehicleTripRegistrationId, booking.RouteId }).WithError(response.Code + ": " + response.ShortDescription);

            }
            else
            {
                var response = await client.PutAsync<BookingDetailModel, bool>($"{Constants.ClientRoutes.UpdateBookings}", booking);

                if (response.IsValid)
                {

                    return RedirectToAction(nameof(Booking), new { booking.VehicleTripRegistrationId, booking.RouteId });

                }

                return RedirectToAction(nameof(Booking), new { booking.VehicleTripRegistrationId, booking.RouteId }).WithError(response.Code + ": " + response.ShortDescription);
            }
        }


        [Route("discountmodal")]
        [HttpGet]
        public async Task<IActionResult> DiscountModal()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            await client.GetAsync<bool>($"{Constants.ClientRoutes.Booking}/sendotp");
            return PartialView();

        }

        [Route("manifestotp")]
        [HttpGet]
        public async Task<IActionResult> ManifestOTP(Manifest manifest)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            await client.GetAsync<bool>($"{Constants.ClientRoutes.Booking}/sendmanifestotp");
            return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId });

        }


        [Route("verifyotp")]
        [HttpGet]
        public async Task<bool> VerifyOtp(string otp)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var verifyresponse = await client.GetAsync<bool>($"{Constants.ClientRoutes.Booking}/verifyotp/{otp}");
            // removeseat
            if (!verifyresponse.IsValid)
            {
                return verifyresponse.Object;

            }

            return verifyresponse.Object;

        }

        [Route("removeticket")]
        [HttpGet]
        public async Task<IActionResult> RemoveTicket(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            ViewBag.RouteId = manifest.RouteId;
            ViewBag.TripId = manifest.VehicleTripRegistrationId;
            var tripresponse = await client.GetAsync<VehicleTripRegistration>($"{Constants.ClientRoutes.VehicleTripRegistration}/{manifest.VehicleTripRegistrationId}");

            if (tripresponse != null)
            {
                manifest.RouteId = tripresponse.Object.RouteId;
                ViewBag.RouteId = tripresponse.Object.RouteId;
            }
            if (ModelState.IsValid)
            {

                var ticketresponse = await client.PutAsync<string, bool>($"{Constants.ClientRoutes.SeatManagement}/removeseat/{manifest.Id}", "");
          

                // removeseat
                if (!ticketresponse.IsValid)
                {
                    return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError(ticketresponse.ShortDescription);

                }

                return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId });


            }

            return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError("Unknown error has occurred");
        }


        [Route("updateticket")]
        [HttpGet]
        public async Task<IActionResult> UpdateTicket(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            ViewBag.RouteId = manifest.RouteId;
            ViewBag.TripId = manifest.VehicleTripRegistrationId;
            ViewBag.TotalSeat = 0;
            var tripresponse = await client.GetAsync<VehicleTripRegistration>($"{Constants.ClientRoutes.VehicleTripRegistration}/{manifest.VehicleTripRegistrationId}");

            var Passporttype = await client.GetAsync<List<PassportTypeModel>>($"{Constants.ClientRoutes.Passport}");
            ViewBag.Passporttype = new SelectList(Passporttype.Object, "Id", "Name");

            if (tripresponse != null)
            {
                manifest.RouteId = tripresponse.Object.RouteId;
                ViewBag.RouteId = tripresponse.Object.RouteId;
            }
            List<Seat> seats = new List<Seat>();
            TicketSaleModel ticketSale = new TicketSaleModel()
            {
                Booking = new BookingDetailModel(),
                Manifest = new ManifestViewModel()

            };

            SubRouteModel subroute = new SubRouteModel()
            {
                Id = null,
                Name = "Select Stop"
            };
            var subrouteresponse = await client.GetAsync<List<SubRouteModel>>($"{Constants.ClientRoutes.Subroute}/GetByRouteId/{manifest.RouteId}");

            ticketSale.Booking.Subroutes = subrouteresponse.IsValid ? subrouteresponse.Object : new List<SubRouteModel>();
            ticketSale.Booking.Subroutes.Insert(0, subroute);

            var manifestresponse = await client.GetAsync<ManifestViewModel>($"{Constants.ClientRoutes.Manifests}/GetByVehicleTripId/{manifest.VehicleTripRegistrationId}");

            if (!manifestresponse.IsValid)
            {
                return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError(manifestresponse.ShortDescription);
            }
            ViewBag.TotalSeat = manifestresponse.Object.TotalSeats;
            if (manifest.Id.GetValueOrDefault() > 0)
            {

                foreach (var passenger in manifestresponse.Object.Passengers)
                {
                    if (passenger.Id == manifest.Id.GetValueOrDefault())
                    {
                        ticketSale.Booking.VehicleTripRegistrationId = passenger.VehicleTripRegistrationId;
                        ticketSale.Booking.RouteId = passenger.RouteId;
                        ticketSale.Booking.Amount = passenger.Amount;
                        ticketSale.Booking.Discount = passenger.Discount;
                        ticketSale.Booking.PaymentMethod = passenger.PaymentMethod;
                        ticketSale.Booking.FirstName = passenger.FullName.Split(' ')[0];
                        ticketSale.Booking.LastName = passenger.FullName.Split(' ')[1];
                        ticketSale.Booking.PhoneNumber = passenger.PhoneNumber;
                        ticketSale.Booking.Gender = passenger.Gender;
                        ticketSale.Booking.NextOfKinName = passenger.NextOfKinName;
                        ticketSale.Booking.TicketUpdate = passenger.SeatManagementId;
                        ticketSale.Booking.BookingReference = passenger.BookingReferenceCode;
                        ticketSale.Booking.SeatNumber = passenger.SeatNumber;
                        ticketSale.Booking.NextOfKinPhone = passenger.NextOfKinPhoneNumber;
                        manifestresponse.Object.RemainingSeat.Insert(0, passenger.SeatNumber);
                        ticketSale.Booking.RemainingSeat = manifestresponse.Object.RemainingSeat;
                        ticketSale.Booking.Update = true;
                        ticketSale.Manifest = manifestresponse.Object;
                        ticketSale.Booking.Email = "noemail@gmail.com";
                        ticketSale.Booking.PassportId = passenger.PassportId;
                        ticketSale.Booking.IsGhanaRoute = passenger.IsGhanaRoute;
                        ticketSale.Booking.PassportType = passenger.PassportType;
                        ticketSale.Booking.PlaceOfIssue = passenger.PlaceOfIssue;
                        ticketSale.Booking.IssuedDate = passenger.IssuedDate;
                        ticketSale.Booking.ExpiredDate = passenger.ExpiredDate;

                        return PartialView(nameof(Booking), ticketSale);

                    }
                }
            }

            return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError("Unknown error has occurred");


        }


        [Route("rescheduleticket")]
        [HttpGet]
        public async Task<IActionResult> RescheduleTicket(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            ViewBag.RouteId = manifest.RouteId;
            ViewBag.TripId = manifest.VehicleTripRegistrationId;
            ViewBag.TotalSeat = 0;
            var tripresponse = await client.GetAsync<VehicleTripRegistration>($"{Constants.ClientRoutes.VehicleTripRegistration}/{manifest.VehicleTripRegistrationId}");

            if (tripresponse != null)
            {
                manifest.RouteId = tripresponse.Object.RouteId;
                ViewBag.RouteId = tripresponse.Object.RouteId;
            }
            if (ModelState.IsValid)
            {

                var ticketresponse = await client.PutAsync<string, bool>($"{Constants.ClientRoutes.SeatManagement}/rescheduleseat/{manifest.Id}", "");

                // removeseat
                if (!ticketresponse.IsValid)
                {
                    return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError(ticketresponse.ShortDescription);

                }

                return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId });


            }

            return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError("Unknown error has occurred");
        }


        [Route("upgradedowngradeticket")]
        [HttpGet]
        public async Task<IActionResult> UpgradeDowngradeTicket(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            ViewBag.RouteId = manifest.RouteId;
            ViewBag.TripId = manifest.VehicleTripRegistrationId;
            ViewBag.TotalSeat = 0;
            var tripresponse = await client.GetAsync<VehicleTripRegistration>($"{Constants.ClientRoutes.VehicleTripRegistration}/{manifest.VehicleTripRegistrationId}");

            if (tripresponse != null)
            {
                manifest.RouteId = tripresponse.Object.RouteId;
                ViewBag.RouteId = tripresponse.Object.RouteId;
            }
            if (ModelState.IsValid)
            {

  
                var ticketresponse = await client.PutAsync<Manifest, bool>($"{Constants.ClientRoutes.Booking}/upgradedowngradeticket", manifest);

                // removeseat
                if (!ticketresponse.IsValid)
                {
                    return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError(ticketresponse.ShortDescription);

                }

                return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId });


            }

            return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError("Unknown error has occurred");
        }

        [Route("printticket")]
        [HttpGet]
        public async Task<IActionResult> PrintTicket(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (ModelState.IsValid)
            {

                var ticketresponse = await client.GetAsync<SeatManagement>($"{Constants.ClientRoutes.SeatManagement}/getseat/{manifest.Id}/{true}");

                if (!ticketresponse.IsValid)
                {
                    return View(nameof(PrintTicket), new SeatManagement());
                }

                return View(nameof(PrintTicket), ticketresponse.Object);

            }

            return View(nameof(PrintTicket), new SeatManagement());
        }

        [Route("getcaptainbycode")]
        [HttpGet]
        public async Task<DriverModel> GetDriverByCode(string Code)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (ModelState.IsValid)
            {

                var driverresponse = await client.GetAsync<DriverModel>($"{Constants.ClientRoutes.Driver}/GetByCode/{Code}");

                if (!driverresponse.IsValid)
                {
                    return driverresponse.Object;
                }

                return driverresponse.Object;

            }

            return new DriverModel();
        }


        [Route("manifestprintwithoutamount")]
        [HttpGet]
        public async Task<IActionResult> ManifestPrintWithoutAmount(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            manifest.IsPrinted = true;
            manifest.ManifestPrinted = true;
            if (ModelState.IsValid)
            {
                var response = await client.PostAsJsonAsync<Manifest, bool>($"{Constants.ClientRoutes.Manifests}/Add", manifest);

                //await client.PostAsync(manifest, $"{Constants.ClientRoutes}");

                if (response.IsValid)
                {
                    var manifestresponse = await client.GetAsync<ManifestViewModel>($"{Constants.ClientRoutes.Manifests}/manifestPrint/{manifest.VehicleTripRegistrationId}");
                    manifestresponse.Object.WithAmount = false;
                    var vehicle = await client.GetAsync<VehicleModel>($"{Constants.ClientRoutes.Vehicle}/GetByRegNumber/{manifestresponse.Object.BusRegistrationNumber}");
                    //var vehicleAge = DateTime.Now.Date.Subtract(vehicle.Object.DateCreated.Date).Days;

                    var dateSpan = DateTimeSpan.CompareDates(vehicle.Object.DateCreated.GetValueOrDefault(), DateTime.Today);
                    //Console.WriteLine("Years: " + dateSpan.Years);
                    //Console.WriteLine("Months: " + dateSpan.Months);
                    //Console.WriteLine("Days: " + dateSpan.Days);
                    var year = dateSpan.Years;
                    var month = dateSpan.Months;
                    var week = (dateSpan.Days / 7);
                    var day = (dateSpan.Days % 7);
                    manifestresponse.Object.VehicleAge = year + "Year(s) " + month + "Month(s) " + week + "Week(s) and " + day + "Day(s)";


                    var driver = await GetDriverByCode(manifestresponse.Object.DriverCode);
                    manifestresponse.Object.DriverName = driver.DriverDetails;

                    
                    //CaptainName
                    if (!manifestresponse.IsValid)
                    {
                        return View(nameof(ManifestPrint), new ManifestViewModel());
                    }

                    return View(nameof(ManifestPrint), manifestresponse.Object);
                }


            }

            return View(nameof(ManifestPrint), new ManifestViewModel());
        }

        [Route("manifestprint")]
        [HttpGet]
        public async Task<IActionResult> ManifestPrint(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            manifest.IsPrinted = true;
            //manifest.ManifestPrinted = true;
            if (ModelState.IsValid)
            {
                var response = await client.PostAsJsonAsync<Manifest, bool>($"{Constants.ClientRoutes.Manifests}/Add", manifest);

                if (response.IsValid)
                {
                    var manifestresponse = await client.GetAsync<ManifestViewModel>($"{Constants.ClientRoutes.Manifests}/manifestPrint/{manifest.VehicleTripRegistrationId}");
                    manifestresponse.Object.WithAmount = true;
                    var driver = await GetDriverByCode(manifestresponse.Object.DriverCode);

                    var vehicle = await client.GetAsync<VehicleModel>($"{Constants.ClientRoutes.Vehicle}/GetByRegNumber/{manifestresponse.Object.BusRegistrationNumber}");
                    //var vehicleAge = DateTime.Now.Date.Subtract(vehicle.Object.DateCreated.Date).Days;
                  
                    var dateSpan = DateTimeSpan.CompareDates(vehicle.Object.DateCreated.GetValueOrDefault(), DateTime.Today);
                    //Console.WriteLine("Years: " + dateSpan.Years);
                    //Console.WriteLine("Months: " + dateSpan.Months);
                    //Console.WriteLine("Days: " + dateSpan.Days);
                    var year = dateSpan.Years;
                    var month = dateSpan.Months;
                    var week = (dateSpan.Days / 7);
                    var day = (dateSpan.Days % 7);
                    manifestresponse.Object.VehicleAge = year + "Year(s) " + month + "Month(s) " + week + "Week(s) and " + day + "Day(s)";


                    manifestresponse.Object.DriverName = driver.DriverDetails;
                    if (!manifestresponse.IsValid)
                    {
                        return View(nameof(ManifestPrint), new ManifestViewModel());
                    }

                    return View(nameof(ManifestPrint), manifestresponse.Object);
                }


            }

            return View(nameof(ManifestPrint), new ManifestViewModel());
        }

        //new 
        [Route("Dispatch")]
        public async Task<IActionResult> Dispatch(Guid vehicleTripRegistrationId, int passengers)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var manifestresponse = await client.GetAsync<Models.Manifest>($"{Constants.ClientRoutes.Manifests}/manifest/{vehicleTripRegistrationId}");
            var totalVAT = 200 * passengers;
            manifestresponse.Object.VAT = totalVAT;
            TempData["TotalVAT"] = totalVAT;
            var manifest = new Manifest()
            {
                VehicleTripRegistrationId = vehicleTripRegistrationId,
                RouteId = manifestresponse.Object.RouteId,
                ManifestManagementId = manifestresponse.Object.ManifestManagementId,
                BusRegNum= manifestresponse.Object.BusRegNum,
                Commision = manifestresponse.Object.Commision,
                Dispatch = manifestresponse.Object.Dispatch,
                Patrol= manifestresponse.Object.Patrol,
                Transload = manifestresponse.Object.Transload,
                VAT = manifestresponse.Object.VAT,
                NumberOfPassengers = passengers
            };
            return PartialView(manifest);
        }


        [Route("DispatchValidate")]
        [HttpGet]
        public async Task<Manifest> DispatchValidate(Guid vehicleTripRegistrationId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var manifestresponse = await client.GetAsync<Models.Manifest>($"{Constants.ClientRoutes.Manifests}/manifest/{vehicleTripRegistrationId}");
            var employeesterminals = await client.GetAsync<TerminalModel>(Constants.ClientRoutes.LoginInTerminal);

            var manifest = new Manifest()
            {
                VehicleTripRegistrationId = vehicleTripRegistrationId,
                RouteId = manifestresponse.Object.RouteId,
                ManifestManagementId = manifestresponse.Object.ManifestManagementId,
                BusRegNum = manifestresponse.Object.BusRegNum,
                Commision = manifestresponse.Object.Commision,
                Dispatch = manifestresponse.Object.Dispatch,
                IsCommision = employeesterminals.Object.IsCommision
            };
            return manifest;
        }

        [Route("UpdateDispatch")]
        public async Task<IActionResult> UpdateDispatch(Manifest manifest)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var v1 = TempData["TotalVAT"];

            manifest.VAT = manifest.NumberOfPassengers * 200;

            var response = await client.PutAsync<Manifest, bool>($"{Constants.ClientRoutes.Manifests}/UpdateDispatch", manifest);

            if (!response.IsValid)
            {
                return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId }).WithError(response.Code + ": " + response.ShortDescription);
            }
            return RedirectToAction(nameof(Booking), new { manifest.VehicleTripRegistrationId, manifest.RouteId });
        }

        [Route("GetNonIdAmount/routeId/fareId")]
        [HttpGet]
        public async Task<decimal> GetNonIdAmount(int routeId, int fareId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<PassportTypeModel>($"{Constants.ClientRoutes.GetPassportTypeById}/{routeId}/{fareId}");

            var items = response.Object;
            return items.AddOnFare;
        }

        //public async Task<decimal?> GetNonIdAmount(int routeid, Guid tripid)
        //{
            
        //}
    }
}