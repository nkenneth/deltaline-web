﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RouteController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public RouteController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Routes,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<RouteModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);

        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RouteDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await FetchRequiredDropDowns();

            return View();
        }

        private async Task FetchRequiredDropDowns(int? departureTerminal = null, int? destinationTerminal = null, int? parentRouteId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminalsRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            var routesRequest = await client.GetAsync<PagedRecordModel<RouteModel>>(Constants.ClientRoutes.Routes);

            ViewBag.ParentRouteId = new SelectList(routesRequest.Object.Items, "Id", "Name", parentRouteId);
            ViewBag.DepartureTerminalId = new SelectList(terminalsRequest.Object.Items, "Id", "Name", departureTerminal);
            ViewBag.DestinationTerminalId = new SelectList(terminalsRequest.Object.Items, "Id", "Name", destinationTerminal);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RouteModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<RouteModel, bool>(Constants.ClientRoutes.RouteCreate, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.DepartureTerminalId, model.DestinationTerminalId, model.ParentRouteId);

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RouteGet, id);
            var result = await client.GetAsync<RouteModel>(url);

            var model = result.Object;

            if (model != null) {
                await FetchRequiredDropDowns(model.DepartureTerminalId, model.DestinationTerminalId, model.ParentRouteId);
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, RouteModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.RouteUpdate, id);

                var result = await client.PutAsync<RouteModel, bool>(url, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.DepartureTerminalId, model.DestinationTerminalId, model.ParentRouteId);

            return View(model);
        }

        [Route("GetRoutesByTerminal/terminalId")]
        [HttpGet]
        public async Task<List<RouteModel>> GetRoutesByTerminal(int terminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var regValue = registrationNo.Split('(');
            var routesRequest = await client.GetAsync<List<RouteModel>>(string.Format(Constants.ClientRoutes.RoutesGetByTerminalId, terminalId));

            var item = routesRequest.Object;

            return item;
        }
    }
}