﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;
using System.Linq;
namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class EmployeeController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public EmployeeController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Create()
        {
            await FetchRequiredDropDowns();

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Employees,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<EmployeeModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EmployeeModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<EmployeeModel, bool>(Constants.ClientRoutes.EmployeeCreate, model);

                if (result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.RoleId, model.TerminalId);

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.EmployeeGet, id);
            var result = await client.GetAsync<EmployeeModel>(url);

            var model = result.Object;

            if (model != null) {
                await FetchRequiredDropDowns(model.RoleId, model.TerminalId);
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, EmployeeModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.EmployeeUpdate, id);

                var result = await client.PutAsync<EmployeeModel, bool>(url, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.RoleId, model.TerminalId);

            return View(model);
        }

        private async Task FetchRequiredDropDowns(int? selectedRoleId = null, int? selectedTerminalId = null, int? selectedDepartmentId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var stateRequest = await client.GetAsync<PagedRecordModel<RoleModel>>(Constants.ClientRoutes.Roles);
            var terminalRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

            ViewBag.RoleId = new SelectList(stateRequest.Object.Items, "Id", "Name", selectedRoleId);
            ViewBag.TerminalId = new SelectList(terminalRequest.Object.Items, "Id", "Name", selectedTerminalId);
        }

        public async Task<IActionResult> DeactivateActivate(int Id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

           // var results = await client.PostAsJsonAsync<int, bool>(Constants.ClientRoutes.activateaccount, Id);

            var response = await client.PostAsJsonAsync<int, bool>($"{Constants.ClientRoutes.activateaccount}/{Id}", Id);
            return RedirectToAction("index");
        }
    }
}