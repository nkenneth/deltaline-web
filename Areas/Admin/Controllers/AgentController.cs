﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")] 
    public class AgentController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public AgentController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", $"{Constants.ClientRoutes.Agents}/get",
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<AgentDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> Create()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var locations = await client.GetAsync<PagedRecordModel<AgentLocationDTO>>($"{Constants.ClientRoutes.AgentLocation}/get");

            ViewBag.DropDown = new SelectList(locations.Object.Items, "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AgentDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (ModelState.IsValid)
            {
                var result = await client.PostAsJsonAsync<AgentDTO, bool>($"{Constants.ClientRoutes.Agents}/add", model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            var locations = await client.GetAsync<PagedRecordModel<AgentLocationDTO>>($"{Constants.ClientRoutes.AgentLocation}/get");
            ViewBag.DropDown = new SelectList(locations.Object.Items, "Id", "Name");

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = $"{Constants.ClientRoutes.Agents}/get/{id}";
            var result = await client.GetAsync<AgentDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                var locations = await client.GetAsync<PagedRecordModel<AgentLocationDTO>>($"{Constants.ClientRoutes.AgentLocation}/get");
                ViewBag.DropDown = new SelectList(locations.Object.Items, "Id", "Name");
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, AgentDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (ModelState.IsValid)
            {
                var url = $"{Constants.ClientRoutes.Agents}/update/{id}";

                var result = await client.PutAsync<AgentDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            var locations = await client.GetAsync<PagedRecordModel<AgentLocationDTO>>($"{Constants.ClientRoutes.AgentLocation}/get");
            ViewBag.DropDown = new SelectList(locations.Object.Items, "Id", "Name");

            return View(model);
        }
        public async Task <IActionResult> GetTransactions(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var agenturl = $"{Constants.ClientRoutes.Agents}/get/{id}";
            var results = await client.GetAsync<AgentDTO>(agenturl);
            ViewBag.Name = results.Object.FirstName + " " + results.Object.LastName;
            var url = $"{Constants.ClientRoutes.Agents}/GetWalBal/{id}";

            var result = await client.GetAsync<decimal>(url);
            var res = result.Object;
            ViewBag.Bal = res;
            ViewBag.Id = id; 
            return View();
        }

        public async Task<IActionResult> PayAgent(int id, decimal amount)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = $"{Constants.ClientRoutes.Agents}/PayAgent/{id}/{amount}";

            var result = await client.GetAsync<decimal>(url);

            var res = result.Object;

            return Redirect("/Agent/GetTransactions/" + id);

        }

        [HttpGet]
        public async Task<IActionResult> GetTrans(int id, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", $"{Constants.ClientRoutes.Agents}/getAgentTransaction", id,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<AgentTransactionDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }
        public IActionResult AgentApplicants()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> GetApplicants(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", $"{Constants.ClientRoutes.Agents}/GetAgentApplicants",
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<AgentDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }
        public async Task<IActionResult> Approve(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = $"{Constants.ClientRoutes.Agents}/Approve/{id}";

            var result = await client.GetAsync<bool>(url);
            var res = result.Object;
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Deny(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = $"{Constants.ClientRoutes.Agents}/Deny/{id}";

            var result = await client.GetAsync<bool>(url);
            var res = result.Object;
            return RedirectToAction("AgentApplicants");
        }


        [HttpPost]
        public async Task<List<AgentDTO>> GetAgentDetail(int AgentId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var url = $"{Constants.ClientRoutes.Agents}/GetAgentBooking/{AgentId}";
            var result = await client.GetAsync<List<AgentDTO>>(url);
            var items = result.Object ?? new List<AgentDTO> {    };
            if (result.ValidationErrors.Count() > 0)
            {
                return items;
            }
            return items;
        }
    }
}