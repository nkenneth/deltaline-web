﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class FeedbackController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public FeedbackController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Feedbacks,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<FeedbackModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }
        public async Task<IActionResult> GetFeedbackById(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var getById = await client.GetAsync<FeedbackModel>(string.Format(Constants.ClientRoutes.FeedbackGet, id));
            var model = getById.Object;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> GetFeedbackById(int id, FeedbackModel model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.UpdateFeedback, id);
            var result = await client.PutAsync<FeedbackModel, bool>(url, model);

            if (result.IsValid && result.Object)
            {
                return RedirectToAction("index");
            }
            else
            {
                foreach (var item in result.ValidationErrors)
                {
                    ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                }

                ModelState.AddModelError("", result.ShortDescription);
            }
            return RedirectToAction("Index");
        }
    }
}