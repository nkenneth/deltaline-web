﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;

namespace Transport.WEB.Areas.Admin.Controllers
{

    [Area("Admin")]
    public class VehicleAllocationController : Controller
    {
        private readonly IHttpClientFactory _HttpClientFactory;

        public VehicleAllocationController(IHttpClientFactory HttpClientFactory)
        {
            _HttpClientFactory = HttpClientFactory;
        }


        public async Task<IActionResult> Index()
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);
            var Terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminal = new SelectList(Terminals.Object.Items, "Id", "Name");
            return View();
        }


        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.TerminalToAllocate,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<TerminalModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public ActionResult GetVechicleByTerminal(int id)
        {
            //return RedirectToAction("Index", "VehicleAllocationDetail", new { Id = id });
            //TempData["Tget"] = id;
            HttpContext.Session.SetString("TerminalID", id.ToString());
            return RedirectToAction("Index", new RouteValueDictionary(
            new { controller = "VehicleAllocationDetail", action = "GetData" }));
        }

        [HttpGet]
        public async Task<IActionResult> VehicleAllocationReport()
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);
            var terminalsRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.GetVirtualAndPhysicalTerminals);

            var routeItems = terminalsRequest.Object.Items;

            List<VehicleAllocationDTO> vehicleList = new List<VehicleAllocationDTO>();
            foreach (var item in routeItems)
            {
                if (item.Name != "Lagos (Default)")
                {
                    var veh = await client.GetAsync<List<VehicleModel>>(string.Format(Constants.ClientRoutes.VehiclesByTerminal, item.Id));
                    var vehi = new VehicleAllocationDTO
                    {
                        VehicleModel = veh.Object,
                        RouteName = item.Name,
                        RouteId = item.RouteId
                    };
                    vehicleList.Add(vehi);
                }
            }

            return View(vehicleList);
        }

        public async Task<IActionResult> GetDataByTerminalLocation()
        {
            return View();
        }

        public async Task<IActionResult> getDataForTerminalLocation(IDataTablesRequest request)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);

            var profile = await client.GetAsync<UserProfileModel>(Constants.ClientRoutes.AccountGetProfile);

            var terminalId = await client.GetAsync<int>(string.Format("{0}/{1}", Constants.ClientRoutes.GetEmployeeTerminalId, profile.Object.Email));

            var url = string.Format("{0}/{1}", Constants.ClientRoutes.VehicleInTerminal, terminalId.Object);

            var result = await client.GetAsync<PagedRecordModel<VehicleModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        public async Task<IActionResult> AllocateBuses(AllocationVehicleModel query, IFormCollection form)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);
            string Terminalls = form["Name"];
            if (Terminalls != null)
            {
                query.Type = 5;
            }
            if (!ModelState.IsValid)
                return Redirect("Index");           
            string strDDLValue = form["RegistrationNumber"];
            var LTermId = HttpContext.Session.GetString("TerminalID");
            int vLTermId = Convert.ToInt32(LTermId);
            query.LocationId = vLTermId;
            query.RegistrationNumber = strDDLValue as string;
            query.Terminals = Terminalls as string;
            if (query.TerminalId == null)
            {
                query.TerminalId = 0; 
             };
            var reset = await client.DeleteAsync<bool>(string.Format(Constants.ClientRoutes.DeleteFromVehicleAllocationConfirm, query.TerminalId));
            var returns = reset.Object;
            var url = string.Format(Constants.ClientRoutes.Allocatebuses);
            var response = await client.PutAsync<AllocationVehicleModel, bool>(url, query);         
            var items = response.Object;
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View("Index", items);

        }

        [HttpGet]
        public async Task<IActionResult> GetAllAllocatedUnsedBusesInTerminal()
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);
            var terminalsRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

            var routeItems = terminalsRequest.Object.Items;

            List<VehicleAllocationDTO> vehicleList = new List<VehicleAllocationDTO>();
            foreach (var item in routeItems)
            {
                if (item.Name != "Edo (Benin)")
                {
                    var veh = await client.GetAsync<List<VehicleModel>>(string.Format(Constants.ClientRoutes.UnusedInTerminals, item.Id));
                    var vehi = new VehicleAllocationDTO
                    {
                        VehicleModel = veh.Object,
                        RouteName = item.Name,
                        RouteId = item.RouteId
                    };
                    vehicleList.Add(vehi);
                }
            }

            return View(vehicleList);
        }

    }
}