﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;
using Microsoft.AspNetCore.Http;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class DriverController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IHostingEnvironment _appEnvironment;


        public DriverController(IHttpClientFactory httpClientFactory, IHostingEnvironment hostingEnvironment)
        {
            _httpClientFactory = httpClientFactory;
            _appEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Drivers,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<DriverModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);

        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.DriverDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DriverDTO model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                //var files = HttpContext.Request.Form.Files;
                var fileName = "";

                if (model.Picture != null)
                {
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "images");

                    fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(model.Picture.FileName).ToString();
                    using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                    {
                        await model.Picture.CopyToAsync(fileStream);
                        //model.Picture = fileName;
                    }
                }

                var driverModel = new DriverModel {
                    Code = model.Code,
                    Name = model.Name,
                    ResidentialAddress = model.ResidentialAddress,
                    Phone1 = model.Phone1,
                    Phone2 = model.Phone2,
                    NextOfKin = model.NextOfKin,
                    NextOfKinNumber = model.NextOfKinNumber,
                    DateOfEmployment = model.DateOfEmployment,
                    Designation=model.Designation,
                    AssignedDate = model.AssignedDate,
                    BankAccount = model.BankAccount,
                    BankName = model.BankName,
                    Picture = fileName,
                    LicenseDate = model.LicenseDate,
                    DuePeriod = model.DuePeriod,
                    Email = model.Email,
                    Alias = model.Alias
                };

                var result = await client.PostAsJsonAsync<DriverModel, bool>(Constants.ClientRoutes.DriverCreate, driverModel);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.DriverGet, id);
            var result = await client.GetAsync<DriverModel>(url);

            var model = result.Object;

            if (model != null) {
                var item = new DriverDTO
                {
                    Id = model.Id,
                    Name = model.Name,
                    Code=model.Code,
                    DateOfEmployment = model.DateOfEmployment,
                    AssignedDate = model.AssignedDate,
                    Phone1 = model.Phone1,
                    Phone2 = model.Phone2,
                    BankAccount = model.BankAccount,
                    BankName = model.BankName,
                    Designation = model.Designation,
                    NextOfKin = model.NextOfKin,
                    NextOfKinNumber = model.NextOfKinNumber,
                    ResidentialAddress = model.ResidentialAddress,
                    Picture = null,
                    LicenseDate = model.LicenseDate,
                    DuePeriod = model.DuePeriod,
                    UserId = model.UserId,
                    Email = model.Email
                };
                return View(item);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, DriverModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.DriverUpdate, id);

                var result = await client.PutAsync<DriverModel, bool>(url, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


    }
}