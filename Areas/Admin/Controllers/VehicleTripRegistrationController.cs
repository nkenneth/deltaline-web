﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]

    public class VehicleTripRegistrationController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public VehicleTripRegistrationController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        [HttpPost]
        public async Task<List<VehicleTripRegistration>> VehicleTripsByDriverCode(DriverSalaryQueryModel query)
        {
            var items = new List<VehicleTripRegistration>();
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                query.EndDate = query.EndDate ?? DateTime.Now;
                query.StartDate = query.StartDate ?? DateTime.Now.Date;
                var response = await client.PostAsJsonAsync<DriverSalaryQueryModel, List<VehicleTripRegistration>>($"{Constants.ClientRoutes.GetVehicleTripsByDriverCode}", query);

                items = response.Object ?? new List<VehicleTripRegistration> { };


                if (response.ValidationErrors.Count() > 0)
                {
                    return items;
                }
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }

        }
    }
}