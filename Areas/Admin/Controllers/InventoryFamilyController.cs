﻿using System;
using System.Collections.Generic;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class InventoryFamilyController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public InventoryFamilyController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllItemFamilies(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.InventorySetupFamilies,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<ItemFamilyDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ItemFamilyDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<ItemFamilyDTO, bool>(Constants.ClientRoutes.InventorySetupFamiliesCreate, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupFamiliesGet, id);
            var result = await client.GetAsync<ItemFamilyDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, ItemFamilyDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.InventorySetupFamiliesUpdate, id);

                var result = await client.PutAsync<ItemFamilyDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.InventorySetupFamiliesDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }
    }
}
