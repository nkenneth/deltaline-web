﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Transport.WEB.Models;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]

    public class JourneyController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger _logger;


        public JourneyController(IHttpClientFactory httpClientFactory, ILogger<JourneyController> logger)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }

        public async Task<IServiceResponse<bool>> Receive(Guid JourneymanagementId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var response = await client.GetAsync<bool>($"{Constants.ClientRoutes.ReceiveTrips}/{JourneymanagementId}");
            return response;
        }

        public async Task<IServiceResponse<bool>> Approve(Guid JourneymanagementId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var response = await client.GetAsync<bool>($"{Constants.ClientRoutes.ApproveTrips}/{JourneymanagementId}");
            return response;
        }
        public async Task<IActionResult> Incoming(DateTime? StartSate = null, DateTime? EndDate = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var query = new JourneyQueryModel
            {
                EndDate = EndDate ?? DateTime.Now,
                StartDate = StartSate ?? DateTime.Now.Date
            };
            var response = await client.PostAsJsonAsync<JourneyQueryModel, List<JourneyModel>>($"{Constants.ClientRoutes.InboundTrips}", query);
            var items = response.Object ?? new List<JourneyModel> { };
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }
        public async Task<IActionResult> Outgoing(DateTime? StartSate = null, DateTime? EndDate = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var query = new JourneyQueryModel
            {
                EndDate = EndDate ?? DateTime.Now,
                StartDate = StartSate ?? DateTime.Now.Date
            };
            var response = await client.PostAsJsonAsync<JourneyQueryModel, List<JourneyModel>>($"{Constants.ClientRoutes.OutboundTrips}", query);
            var items = response.Object ?? new List<JourneyModel> { };
            //var items = response.Object.Select(x => x.VehicleTripRegistrationId).Distinct().ToList().AsEnumerable();
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        public async Task<IActionResult> BlowJourneys()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var resposne = await client.GetAsync<List<VehicleTripRegistration>>(Constants.ClientRoutes.BlowJourneys);

            var items = resposne.Object;
            if (resposne.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(resposne.Code + ": " + resposne.ShortDescription);
            }

            return View(items).WithSuccess(resposne.ShortDescription);
        }
        
    }
}