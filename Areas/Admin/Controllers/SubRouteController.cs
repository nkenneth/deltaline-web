﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SubRouteController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        
        public SubRouteController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.SubRoutes,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<SubRouteModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);

        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.SubRouteDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await FetchRequiredDropDowns();

            return View();
        }

        private async Task FetchRequiredDropDowns(int? routeId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var routesRequest = await client.GetAsync<PagedRecordModel<RouteModel>>(Constants.ClientRoutes.Routes);

            ViewBag.RouteId = new SelectList(routesRequest.Object.Items, "Id", "Name", routeId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubRouteModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<SubRouteModel, bool>(Constants.ClientRoutes.SubRouteCreate, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.RouteId);

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.SubRouteGet, id);
            var result = await client.GetAsync<SubRouteModel>(url);

            var model = result.Object;

            if (model != null) {
                await FetchRequiredDropDowns(model.RouteId);
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, SubRouteModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.SubRouteUpdate, id);

                var result = await client.PutAsync<SubRouteModel, bool>(url, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.RouteId);

            return View(model);
        }
    }
}