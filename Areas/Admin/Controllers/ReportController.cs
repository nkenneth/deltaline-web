﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Models.Enum;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;
using Transport.WEB.Utils.Helpers;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ReportController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger _logger;
        private IHostingEnvironment _hostingEnvironment;

        public ReportController(IHttpClientFactory httpClientFactory, ILogger<ReportController> logger, IHostingEnvironment hostingEnvironment)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> Index(int PageIndex = 1,
         int PageSize = 10, int? TerminalId = null, DateTime? StartDate = null, DateTime? EndDate = null)
        {

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            await FetchRequiredDropDowns(departureTerminal: TerminalId.HasValue ? TerminalId : 0);

            ViewBag.StartDate = StartDate;
            ViewBag.EndDate = EndDate;
            ViewBag.Terminal = TerminalId;

            BookingReportQueryModel query = new BookingReportQueryModel
            { StartDate = StartDate ?? DateTime.Now.Date, EndDate = EndDate ?? DateTime.Now, PageIndex = PageIndex, PageSize = PageSize, TerminalId = TerminalId };

            var response = await client.PostAsJsonAsync<BookingReportQueryModel, List<BookingReportModel>>($"{Constants.ClientRoutes.BookingReports}", query);
            ViewBag.TotalCount = response?.Object?.FirstOrDefault()?.TotalCount ?? 0;
            ViewBag.TotalPages = response?.Object?.FirstOrDefault()?.TotalCount / query.PageSize;

            if (response?.Object?.FirstOrDefault()?.TotalCount % query.PageSize > 0) ViewBag.TotalPages++;
            var items = response.Object ?? new List<BookingReportModel> { };

            ViewBag.Page = query.PageIndex;

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpPost]
        public IActionResult Index(BookingReportQueryModel query)
        {
            if (!ModelState.IsValid)
                return Redirect("Index");

            ViewBag.StartDate = query.StartDate;
            ViewBag.EndDate = query.EndDate;
            ViewBag.Terminal = query.TerminalId;

            return RedirectToAction("Index", new { terminalId = query.TerminalId, query.StartDate, query.EndDate });
        }

        [HttpPost]
        public async Task<IActionResult> BookingTrips(BookedTripsQueryModel query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            int? vTermId = query.TerminalId;


            //if (!ModelState.IsValid)
            //    return Redirect("Index");

            query.EndDate = query.EndDate ?? DateTime.Now;
            query.StartDate = query.StartDate ?? DateTime.Now.Date;

            ViewBag.StartDate = query.StartDate;
            ViewBag.EndDate = query.EndDate;
            ViewBag.PhysicalBusRegisterationNumber = query.PhysicalBusRegisterationNumber;
            ViewBag.BookingType = query.BookingType;
            ViewBag.IsPrinted = query.IsPrinted;
            ViewBag.DepartureTerminal = query.DepartureTerminalId;
            await FetchRequiredDropDownsForTrips(vehicleRegistrationNumber: query.PhysicalBusRegisterationNumber ?? "");

            var response = await client.PostAsJsonAsync<BookedTripsQueryModel, List<BookedTripsModel>>($"{Constants.ClientRoutes.BookedTrips}", query);

            IEnumerable<BookedTripsModel> items = null;
            if (vTermId.HasValue && vTermId != 0)
            {
                var vitems = response.Object ?? new List<BookedTripsModel> { };
                items = vitems.Where(x => x.DepartureTerminal == vTermId && x.IsPrinted.Equals(true));
            }
            else
            {
                items = response.Object ?? new List<BookedTripsModel> { };
            }
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }
        [HttpGet]
        public async Task<IActionResult> BookingTrips()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var query = new BookedTripsQueryModel
            {
                PhysicalBusRegisterationNumber = null,
                BookingType = null,
                EndDate = DateTime.Now,
                StartDate = DateTime.Now.Date,
                IsPrinted = true,
                DepartureTerminalId = null
            };
            await FetchRequiredDropDownsForTrips(vehicleRegistrationNumber: query.PhysicalBusRegisterationNumber ?? null);
            var response = await client.PostAsJsonAsync<BookedTripsQueryModel, List<BookedTripsModel>>($"{Constants.ClientRoutes.BookedTrips}", query);
            var items = response.Object = new List<BookedTripsModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }


        [HttpGet]
        public async Task<IActionResult> BookingSales(int? TerminalId = null, DateTime? StartDate = null, DateTime? EndDate = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var query = new BookingSalesReportQueryModel
            {
                EndDate = DateTime.Now,
                StartDate = DateTime.Now.Date
            };


            var response = await client.PostAsJsonAsync<BookingSalesReportQueryModel, List<BookingSalesReportModel>>($"{Constants.ClientRoutes.BookedSales}", query);
            var items = response.Object = new List<BookingSalesReportModel> { };
            await FetchRequiredDropDowns();

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(items).WithSuccess(response.ShortDescription);
        }
        [HttpPost]
        public async Task<IActionResult> BookingSales(BookingSalesReportQueryModel query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (!ModelState.IsValid)
            {
                await FetchRequiredDropDowns(state: query.StateId, route: query.RouteId, departureTerminal: query.TerminalId);
                return View();
            }
            if (query == null)
                query = new BookingSalesReportQueryModel { StartDate = DateTime.Now.Date, EndDate = DateTime.Now };

            ViewBag.StartDate = query.StartDate ?? DateTime.Now.Date;
            ViewBag.EndDate = query.EndDate ?? DateTime.Now;
            ViewBag.CreatedBy = query.CreatedBy;

            await FetchRequiredDropDowns(state: query.StateId, route: query.RouteId, departureTerminal: query.TerminalId);
            var response = await client.PostAsJsonAsync<BookingSalesReportQueryModel, List<BookingSalesReportModel>>($"{Constants.ClientRoutes.BookedSales}", query);

            var items = response.Object ?? new List<BookingSalesReportModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpGet]
        public async Task<IActionResult> PassengerReport(string keyword = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var query = new PassengerReportQueryModel
            {
                EndDate = DateTime.Now,
                StartDate = DateTime.Now.Date
            };
            var response = await client.PostAsJsonAsync<PassengerReportQueryModel, List<PassengerReportModel>>($"{Constants.ClientRoutes.PassengerReport}", query);
            var items = response.Object ?? new List<PassengerReportModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpPost]
        public async Task<IActionResult> PassengerReport(PassengerReportQueryModel query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (!ModelState.IsValid)
            {
                return View();
            }

            query.EndDate = query.EndDate ?? DateTime.Now;
            query.StartDate = query.StartDate ?? DateTime.Now.Date;

            ViewBag.StartDate = query.StartDate;
            ViewBag.EndDate = query.EndDate;
            ViewBag.Keyword = query.Keyword;

            var response = await client.PostAsJsonAsync<PassengerReportQueryModel, List<PassengerReportModel>>($"{Constants.ClientRoutes.PassengerReport}", query);
            var items = response.Object ?? new List<PassengerReportModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }



        [HttpGet]
        public async Task<IActionResult> SalesPerBus()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var query = new SalesPerBusQueryModel
            {
                EndDate = DateTime.Now,
                StartDate = DateTime.Now.Date
            };
            var response = await client.PostAsJsonAsync<SalesPerBusQueryModel, List<SalesPerBusModel>>($"{Constants.ClientRoutes.SalesPerBus}", query);
            var items = response.Object ?? new List<SalesPerBusModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }


        [HttpPost]
        public async Task<IActionResult> SalesPerBus(SalesPerBusQueryModel query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (!ModelState.IsValid)
            {
                return View();
            }

            query.EndDate = query.EndDate ?? DateTime.Now;
            query.StartDate = query.StartDate ?? DateTime.Now.Date;

            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;

            var response = await client.PostAsJsonAsync<SalesPerBusQueryModel, List<SalesPerBusModel>>($"{Constants.ClientRoutes.SalesPerBus}", query);
            var items = response.Object ?? new List<SalesPerBusModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpGet]
        public async Task<IActionResult> DriverSalary()
        {
            ViewBag.DriverSalaryQueryModel = new DriverSalaryQueryModel();
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var query = new DriverSalaryQueryModel
            {
                EndDate = DateTime.Now,
                StartDate = DateTime.Now.Date
            };
            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers?.Object?.Items, "Code", "Details");
            var response = await client.PostAsJsonAsync<DriverSalaryQueryModel, List<DriverSalaryModel>>($"{Constants.ClientRoutes.DriverSalary}", query);
            var items = response.Object ?? new List<DriverSalaryModel> { };

            //Initializing the following
            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.DriverCode = query.DriverCode;

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpPost]
        public async Task<IActionResult> DriverSalary(DriverSalaryQueryModel query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (!ModelState.IsValid)
            {
                return View();
            }

            query.EndDate = query.EndDate ?? DateTime.Now;
            query.StartDate = query.StartDate ?? DateTime.Now.Date;

            //to pass the query params back to the view
            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.DriverCode = query.DriverCode;

            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Details");

            var response = await client.PostAsJsonAsync<DriverSalaryQueryModel, List<DriverSalaryModel>>($"{Constants.ClientRoutes.DriverSalary}", query);
            var items = response.Object ?? new List<DriverSalaryModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }
        public async Task<IActionResult> ExportDriverSalary(DateTime? StartDate, DateTime? EndDate)
        {
            //var exportedFile = @"D:\\passengerreport.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"DriverSalary.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }

                EndDate = EndDate ?? DateTime.Now;
                StartDate = StartDate ?? DateTime.Now.Date;

                var query = new DriverSalaryQueryModel
                {
                    StartDate = StartDate,
                    EndDate = EndDate
                };

                var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
                ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Details");

                var response = await client.PostAsJsonAsync<DriverSalaryQueryModel, List<DriverSalaryModel>>($"{Constants.ClientRoutes.DriverSalary}", query);
                var items = response.Object ?? new List<DriverSalaryModel> { };

                foreach (var x in items)

                    using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                    {
                        IWorkbook workbook = new XSSFWorkbook();

                        ISheet passengersheet = workbook.CreateSheet("Sheet1");
                        var rowIndex = 0;
                        IRow row = passengersheet.CreateRow(rowIndex);
                        row.CreateCell(0).SetCellValue("Start Date");
                        row.CreateCell(1).SetCellValue("End Date");
                        row.CreateCell(2).SetCellValue("Driver Code");
                        row.CreateCell(3).SetCellValue("Name");
                        row.CreateCell(4).SetCellValue("Bank Name");
                        row.CreateCell(5).SetCellValue("Bank Account");
                        row.CreateCell(6).SetCellValue("Total Amount");
                        row.CreateCell(7).SetCellValue("Number of Trips");

                        foreach (var passenger in items)
                        {
                            rowIndex++;
                            var passengerRow = passengersheet.CreateRow(rowIndex);
                            passengerRow.CreateCell(0).SetCellValue(passenger.StartDate.ToString() ?? "No value");
                            passengerRow.CreateCell(1).SetCellValue(passenger.EndDate.ToString() ?? "No value");
                            passengerRow.CreateCell(2).SetCellValue(passenger.DriverCode ?? "No value");
                            passengerRow.CreateCell(3).SetCellValue(passenger.Name ?? "No value");
                            passengerRow.CreateCell(4).SetCellValue(passenger.BankName ?? "No value");
                            passengerRow.CreateCell(5).SetCellValue(passenger.BankAccount ?? "No value");
                            passengerRow.CreateCell(6).SetCellValue(passenger.DriverFee.ToString() ?? "No value");
                            passengerRow.CreateCell(7).SetCellValue(passenger.NoofTrips.ToString() ?? "No value");
                        }
                        workbook.Write(fs);
                    }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                //log
                return RedirectToAction("DriverSalary");
            }

        }
        [HttpGet]
        public async Task<IActionResult> HireRequests(int? pageIndex = 1,
      int? pageSize = 10, DateTime? startDate = null, DateTime? endDate = null, string keyword = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var query = new HireRequestQueryModel
            {
                PageSize = pageSize,
                PageIndex = pageIndex,
                EndDate = DateTime.Now,
                StartDate = DateTime.Now.Date,
                Keyword = keyword
            };

            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.Keyword = query.Keyword;
            ViewBag.PageIndex = query.PageIndex;
            ViewBag.PageSize = query.PageSize;
            var response = await client.PostAsJsonAsync<HireRequestQueryModel, List<HireRequestModel>>($"{Constants.ClientRoutes.HiredTrips}", query);
            var items = response.Object ?? new List<HireRequestModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpGet]
        public async Task<IActionResult> HireTravelled(HireBusDTO hire, int? pageIndex = 1, int? pageSize = 10, DateTime? startDate = null, DateTime? endDate = null, string keyword = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name");
            var query = new HireRequestQueryModel
            {
                EndDate = endDate,
                StartDate = startDate,
                Keyword = keyword,
                DepartureTerminalId = hire.DepartureTerminalId
            };

            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.Keyword = query.Keyword;
            ViewBag.depId = query.DepartureTerminalId;

            var response = await client.PostAsJsonAsync<HireRequestQueryModel, List<HireBusDTO>>($"{Constants.ClientRoutes.HireTravelled}", query);
            var items = response.Object ?? new List<HireBusDTO> { };
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }
        public async Task<IActionResult> ExportHireTravelled(DateTime? startDate, DateTime? endDate, string keyword = null)
        {
            //var exportedFile = @"D:\\passengerreport.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"HireTravelled.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var query = new HireRequestQueryModel
                {
                    EndDate = endDate,
                    StartDate = startDate,
                    Keyword = keyword
                };

                ViewBag.EndDate = query.EndDate;
                ViewBag.StartDate = query.StartDate;
                ViewBag.Keyword = query.Keyword;

                var response = await client.PostAsJsonAsync<HireRequestQueryModel, List<HireBusDTO>>($"{Constants.ClientRoutes.HireTravelled}", query);
                var items = response.Object ?? new List<HireBusDTO> { };

                foreach (var x in items)

                    using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                    {
                        IWorkbook workbook = new XSSFWorkbook();

                        ISheet passengersheet = workbook.CreateSheet("Sheet1");
                        var rowIndex = 0;
                        IRow row = passengersheet.CreateRow(rowIndex);
                        row.CreateCell(0).SetCellValue("Registration No");
                        row.CreateCell(1).SetCellValue("Departure Address");
                        row.CreateCell(2).SetCellValue("Destination Address");
                        row.CreateCell(3).SetCellValue("Departure Date");
                        row.CreateCell(4).SetCellValue("Driver Name");
                        row.CreateCell(5).SetCellValue("Amount");

                        foreach (var passenger in items)
                        {
                            rowIndex++;
                            var passengerRow = passengersheet.CreateRow(rowIndex);
                            passengerRow.CreateCell(0).SetCellValue(passenger.RegistrationNo ?? "No value");
                            passengerRow.CreateCell(1).SetCellValue(passenger.DepartureAddress ?? "No value");
                            passengerRow.CreateCell(2).SetCellValue(passenger.DestinationAddress ?? "No value");
                            passengerRow.CreateCell(3).SetCellValue(passenger.DepartureDate.ToString("dddd, dd MMMM yyyy") ?? "No value");
                            passengerRow.CreateCell(4).SetCellValue(passenger.DriverName ?? "No value");
                            passengerRow.CreateCell(5).SetCellValue(passenger.Amount.ToString() ?? "No value");
                        }
                        workbook.Write(fs);
                    }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                //log
                return RedirectToAction("HireTravelled");
            }

        }

        [HttpGet]
        public async Task<List<HirePassengerDTO>> GetHirePassengers(Guid Id)
        {
            var items = new List<HirePassengerDTO>();
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var res = await client.GetAsync<List<HirePassengerDTO>>(string.Format(Constants.ClientRoutes.HireTravelledPass, Id));

                items = res.Object ?? new List<HirePassengerDTO> { };


                if (res.ValidationErrors.Count() > 0)
                {
                    return items;
                }
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }

        [HttpPost]
        public async Task<IActionResult> HireRequests(HireRequestQueryModel query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (!ModelState.IsValid)
            {
                return View();
            }

            query.EndDate = query.EndDate ?? DateTime.Now;
            query.StartDate = query.StartDate ?? DateTime.Now.Date;

            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.Keyword = query.Keyword;
            ViewBag.PageIndex = query.PageIndex;
            ViewBag.PageSize = query.PageSize;


            var response = await client.PostAsJsonAsync<HireRequestQueryModel, List<HireRequestModel>>($"{Constants.ClientRoutes.HiredTrips}", query);
            var items = response.Object ?? new List<HireRequestModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpGet]
        public async Task<IActionResult> JourneyCharts(DateTime? startDate = null, DateTime? endDate = null,
            int? regionId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var query = new JourneyChartQueryModel
            {
                EndDate = endDate ?? DateTime.Now,
                StartDate = startDate ?? DateTime.Now.Date,
                RegionId = regionId

            };

            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.RegionId = query.RegionId;

            var response = await client.PostAsJsonAsync<JourneyChartQueryModel, List<JourneyChartModel>>
                ($"{Constants.ClientRoutes.JourneyCharts}", query);
            var items = response.Object ?? new List<JourneyChartModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpPost]
        public async Task<IActionResult> JourneyCharts(JourneyChartQueryModel query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (!ModelState.IsValid)
            {
                return View();
            }

            query.EndDate = query.EndDate ?? DateTime.Now;
            query.StartDate = query.StartDate ?? DateTime.Now.Date;
            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.RegionId = query.RegionId;


            var response = await client.PostAsJsonAsync<JourneyChartQueryModel, List<JourneyChartModel>>
                ($"{Constants.ClientRoutes.JourneyCharts}", query);
            var items = response.Object ?? new List<JourneyChartModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }


        [HttpGet]
        public async Task<IActionResult> TerminalJourneyCharts(DateTime? startDate = null, DateTime? endDate = null,
        int? stateId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var query = new JourneyChartQueryModel
            {
                EndDate = endDate ?? DateTime.Now,
                StartDate = startDate ?? DateTime.Now.Date,
                StateId = stateId
            };

            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.StateId = query.StateId;

            var response = await client.PostAsJsonAsync<JourneyChartQueryModel, List<JourneyChartModel>>
                ($"{Constants.ClientRoutes.JourneyChartsForStateTerminals}", query);
            var items = response.Object ?? new List<JourneyChartModel> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }
        [HttpGet]
        public async Task<List<JourneyDetailDisplayDTO>> JourneyChartsDetailsByState(int? stateId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            DateTime? startDate = null;
            DateTime? endDate = null;
            var query = new JourneyChartQueryModel
            {
                EndDate = endDate ?? DateTime.Now,
                StartDate = startDate ?? DateTime.Now.Date,
                StateId = stateId
            };

            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.StateId = query.StateId;

            var response = await client.PostAsJsonAsync<JourneyChartQueryModel, List<JourneyDetailDisplayDTO>>
                ($"{Constants.ClientRoutes.journeyChartsDetailsByState}", query);
            var items = response.Object ?? new List<JourneyDetailDisplayDTO> { };

            if (response.ValidationErrors.Count() > 0)
            {
                return items;
            }
            return items;
        }
        public async Task<IActionResult> ExportPassengersToExcelAsync(PassengerReportQueryModel query)
        {
            //var exportedFile = @"D:\\passengerreport.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"bookingsales.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }
                //var file = Path.Combine(Directory.GetCurrentDirectory(),
                //            "MyStaticFiles", "images", exportedFile);

                query.EndDate = query.EndDate ?? DateTime.Now;
                query.StartDate = query.StartDate ?? DateTime.Now.Date;

                var response = await client.PostAsJsonAsync<PassengerReportQueryModel, List<PassengerReportModel>>($"{Constants.ClientRoutes.PassengerReport}", query);
                var items = response.Object ?? new List<PassengerReportModel> { };


                using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();

                    ISheet passengersheet = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = passengersheet.CreateRow(rowIndex);
                    row.CreateCell(0).SetCellValue("Number of Tickets");
                    row.CreateCell(1).SetCellValue("Full Name");
                    row.CreateCell(2).SetCellValue("Phone Numer");

                    foreach (var passenger in items)
                    {
                        rowIndex++;
                        var passengerRow = passengersheet.CreateRow(rowIndex);
                        passengerRow.CreateCell(0).SetCellValue(passenger?.NoOfTickets.ToString() ?? "No value");
                        passengerRow.CreateCell(1).SetCellValue(passenger.FullName ?? "No value");
                        passengerRow.CreateCell(2).SetCellValue(passenger.PhoneNumber ?? "No value");
                    }
                    workbook.Write(fs);
                }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                //log
                return View();
            }

        }

        public async Task<IActionResult> ExportBookingSalesToExcelAsync(BookingSalesReportQueryModel query)
        {
            //var exportedFile = @"D:\\bookingsales.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"bookingsales.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }
                //var file = Path.Combine(Directory.GetCurrentDirectory(),
                //            "MyStaticFiles", "images", exportedFile);

                query.EndDate = query.EndDate ?? DateTime.Now;
                query.StartDate = query.StartDate ?? DateTime.Now.Date;
                var response = await client.PostAsJsonAsync<BookingSalesReportQueryModel, List<BookingSalesReportModel>>($"{Constants.ClientRoutes.BookedSales}", query);

                var items = response.Object ?? new List<BookingSalesReportModel> { };


                using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();

                    ISheet passengersheet = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = passengersheet.CreateRow(rowIndex);
                    row.CreateCell(0).SetCellValue("Amount");
                    row.CreateCell(1).SetCellValue("Created By");
                    row.CreateCell(2).SetCellValue("Departure Terminal");
                    row.CreateCell(3).SetCellValue("Region");
                    row.CreateCell(4).SetCellValue("Route");
                    row.CreateCell(5).SetCellValue("State");


                    foreach (var item in items)
                    {
                        rowIndex++;
                        var passengerRow = passengersheet.CreateRow(rowIndex);
                        passengerRow.CreateCell(0).SetCellValue(item?.Amount.ToString() ?? "No value");
                        passengerRow.CreateCell(1).SetCellValue(item.CreatedBy ?? "No value");
                        passengerRow.CreateCell(2).SetCellValue(item.DepartureTerminal ?? "No value");
                        passengerRow.CreateCell(3).SetCellValue(item.Region ?? "No value");
                        passengerRow.CreateCell(4).SetCellValue(item.Route ?? "No value");
                        passengerRow.CreateCell(5).SetCellValue(item.State ?? "No value");
                    }
                    workbook.Write(fs);
                }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                return View();
                //log
            }

        }

        public async Task<IActionResult> ExportBookingTripsToExcelAsync(BookedTripsQueryModel query)
        {
            //var exportedFile = @"D:\\bookingtrips.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"bookingtrips.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }
                //var file = Path.Combine(Directory.GetCurrentDirectory(),
                //            "MyStaticFiles", "images", exportedFile);

                query.EndDate = query.EndDate ?? DateTime.Now;
                query.StartDate = query.StartDate ?? DateTime.Now.Date;
                var response = await client.PostAsJsonAsync<BookedTripsQueryModel, List<BookedTripsModel>>($"{Constants.ClientRoutes.BookedTrips}", query);

                var items = response.Object ?? new List<BookedTripsModel> { };


                using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();

                    ISheet passengersheet = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = passengersheet.CreateRow(rowIndex);
                    row.CreateCell(0).SetCellValue("Bus Number");
                    row.CreateCell(1).SetCellValue("Revenue");
                    row.CreateCell(2).SetCellValue("Route");
                    row.CreateCell(3).SetCellValue("Seats Booked");
                    row.CreateCell(4).SetCellValue("Departure Date");
                    row.CreateCell(5).SetCellValue("Departure Time");
                    row.CreateCell(6).SetCellValue("Departure Time");



                    foreach (var item in items)
                    {
                        rowIndex++;
                        var passengerRow = passengersheet.CreateRow(rowIndex);
                        passengerRow.CreateCell(0).SetCellValue(item?.PhysicalBusRegistrationNumber.ToString() ?? "No value");
                        passengerRow.CreateCell(1).SetCellValue(item?.Revenue.ToString() ?? "No value");
                        passengerRow.CreateCell(2).SetCellValue(item?.RouteName ?? "No value");
                        passengerRow.CreateCell(3).SetCellValue(item?.SeatsBooked.ToString() ?? "No value");
                        passengerRow.CreateCell(4).SetCellValue(item?.DepartureDate.ToString() ?? "No value");
                        passengerRow.CreateCell(5).SetCellValue(item?.DepartureTime ?? "No value");
                        passengerRow.CreateCell(6).SetCellValue(item?.AvailableSeats.ToString() ?? "No value");

                    }
                    workbook.Write(fs);
                }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                return View();
                //log
            }
        }

        public async Task<IActionResult> ExportSalesPerBusToExcelAsync(SalesPerBusQueryModel query)
        {
            //var exportedFile = @"D:\\salesperbus.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"salesperbus.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }

                query.EndDate = query.EndDate ?? DateTime.Now;
                query.StartDate = query.StartDate ?? DateTime.Now.Date;
                var response = await client.PostAsJsonAsync<SalesPerBusQueryModel, List<SalesPerBusModel>>($"{Constants.ClientRoutes.SalesPerBus}", query);

                var items = response.Object ?? new List<SalesPerBusModel> { };


                using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();

                    ISheet passengersheet = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = passengersheet.CreateRow(rowIndex);
                    row.CreateCell(0).SetCellValue("Bus Number");
                    row.CreateCell(1).SetCellValue("Terminal Booking Sales");
                    row.CreateCell(2).SetCellValue("Route OnlineBooking Sales");
                    row.CreateCell(3).SetCellValue("Advanced Booking Sales");
                    row.CreateCell(4).SetCellValue("Total Sales");
                    row.CreateCell(5).SetCellValue("Departure Date");

                    foreach (var item in items)
                    {
                        rowIndex++;
                        var passengerRow = passengersheet.CreateRow(rowIndex);
                        passengerRow.CreateCell(0).SetCellValue(item?.PhysicalBusRegistrationNumber.ToString() ?? "No value");
                        passengerRow.CreateCell(1).SetCellValue(item?.TerminalBookingSales.ToString() ?? "No value");
                        passengerRow.CreateCell(2).SetCellValue(item?.OnlineBookingSales.ToString() ?? "No value");
                        passengerRow.CreateCell(3).SetCellValue(item?.AdvancedBookingSales.ToString() ?? "No value");
                        passengerRow.CreateCell(4).SetCellValue(item?.TotalSales.ToString() ?? "No value");
                        passengerRow.CreateCell(5).SetCellValue(item?.DepartureDate.ToString() ?? "No value");

                    }
                    workbook.Write(fs);
                }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                return View();
                //log
            }
        }

        public async Task<IActionResult> ExportBookingReportAsync(BookingReportQueryModel query)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"bookingreport.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }

                query.EndDate = query.EndDate ?? DateTime.Now;
                query.StartDate = query.StartDate ?? DateTime.Now.Date;
                var response = await client.PostAsJsonAsync<BookingReportQueryModel, List<BookingReportModel>>($"{Constants.ClientRoutes.BookingReports}", query);

                var items = response.Object ?? new List<BookingReportModel> { };
                using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();

                    ISheet passengersheet = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = passengersheet.CreateRow(rowIndex);
                    row.CreateCell(0).SetCellValue("Full Name");
                    row.CreateCell(1).SetCellValue("Booking Reference Code");
                    row.CreateCell(2).SetCellValue("Phone Number");
                    row.CreateCell(3).SetCellValue("Destination");
                    row.CreateCell(4).SetCellValue("Total Sales");
                    row.CreateCell(5).SetCellValue("Employee Email");
                    row.CreateCell(6).SetCellValue("Employee Name");
                    row.CreateCell(7).SetCellValue("Gender");
                    row.CreateCell(8).SetCellValue("Next Of Kin Name");
                    row.CreateCell(9).SetCellValue("Reroute Fee Diff");
                    row.CreateCell(10).SetCellValue("Seat Number");
                    row.CreateCell(11).SetCellValue("Terminal Name");
                    row.CreateCell(12).SetCellValue("Total Discounted Sales");

                    foreach (var item in items)
                    {
                        rowIndex++;
                        var passengerRow = passengersheet.CreateRow(rowIndex);
                        passengerRow.CreateCell(0).SetCellValue(item?.FullName ?? "No value");
                        passengerRow.CreateCell(1).SetCellValue(item?.BookingReferenceCode ?? "No value");
                        passengerRow.CreateCell(2).SetCellValue(item?.CustomerPhoneNumber ?? "No value");
                        passengerRow.CreateCell(3).SetCellValue(item?.Destination ?? "No value");
                        passengerRow.CreateCell(4).SetCellValue(item?.TotalSales.ToString() ?? "No value");
                        passengerRow.CreateCell(5).SetCellValue(item?.EmployeeEmail ?? "No value");
                        passengerRow.CreateCell(6).SetCellValue(item?.EmployeeName ?? "No value");
                        passengerRow.CreateCell(7).SetCellValue(item?.Gender ?? "No value");
                        passengerRow.CreateCell(8).SetCellValue(item?.NextOfKinName ?? "No value");
                        passengerRow.CreateCell(9).SetCellValue(item?.RerouteFeeDiff.ToString() ?? "No value");
                        passengerRow.CreateCell(10).SetCellValue(item?.SeatNumber.ToString() ?? "No value");
                        passengerRow.CreateCell(11).SetCellValue(item?.TerminalName ?? "No value");
                        passengerRow.CreateCell(12).SetCellValue(item?.TotalDiscountedSales.ToString() ?? "No value");
                    }
                    workbook.Write(fs);
                }

                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        private async Task FetchRequiredDropDowns(int? departureTerminal = null, int? state = null, int? route = null, int? region = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminalsRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            var routesRequest = await client.GetAsync<PagedRecordModel<RouteModel>>(Constants.ClientRoutes.Routes);
            var statesRequest = await client.GetAsync<PagedRecordModel<StateModel>>(Constants.ClientRoutes.States);
            var regionRequest = await client.GetAsync<PagedRecordModel<RegionModel>>(Constants.ClientRoutes.Regions);

            ViewBag.TerminalId = new SelectList(terminalsRequest.Object.Items, "Id", "Name", departureTerminal);
            ViewBag.StateId = new SelectList(statesRequest.Object.Items, "Id", "Name", state);
            ViewBag.RouteId = new SelectList(routesRequest.Object.Items, "Id", "Name", route);
            ViewBag.RegionId = new SelectList(regionRequest.Object.Items, "Id", "Name", region);
        }

        private async Task FetchRequiredDropDownsForTrips(int? departureTerminal = null, string vehicleRegistrationNumber = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var vehiclesRequest = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.Vehicles);
            var terminalsRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

            ViewBag.VehicleId = new SelectList(vehiclesRequest.Object.Items, "RegistrationNumber", "RegistrationNumber", vehicleRegistrationNumber);
            ViewBag.TerminalId = new SelectList(terminalsRequest.Object.Items, "Id", "Name", departureTerminal);

            List<SelectListItem> isPrinted = new List<SelectListItem>();
            isPrinted.Add(new SelectListItem { Text = "Closed", Value = bool.TrueString, Selected = true });
            isPrinted.Add(new SelectListItem { Text = "Open", Value = bool.FalseString });

            ViewBag.IsPrinted = isPrinted;
        }

        private async Task fetchRequiredDropdownForRescheduleBooking(DateTime departureDate, int departureTerminalId, int destinationTerminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var rescheduleSearch = new VehicleTripRouteSearchDTO
            {
                DestinationTerminalId = destinationTerminalId,
                DepartureDate = departureDate,
                DepartureTerminalId = departureTerminalId
            };

            var dropdownData = await client.PostAsJsonAsync<VehicleTripRouteSearchDTO, GroupedTripsDetailDTO>($"{Constants.ClientRoutes.BookingReports}", rescheduleSearch);

            var items = new SelectList(dropdownData.Object.Departures, "", "");


            //var timeRequest = await client.PostAsync<TicketRavescheduleDTO, List
        }

        //public ActionResult BookingDetail()
        //{
        //    var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

        //    var response = new BookingDetailModel {
        //        BookingReference = null
        //    };

        //    return View();
        //}

        [HttpGet]
        public async Task<IActionResult> BookingDetail(string refCode)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (string.IsNullOrWhiteSpace(refCode))
                return View().WithWarning("Refcode or Phone is required.");

            var res = await client.GetAsync<IEnumerable<SeatManagementDTO>>(
                            string.Format(Constants.ClientRoutes.GetTripHistory, refCode));

            return View(res.Object)
                   .WithSuccess(res.ShortDescription);

            //var refType = Regex.IsMatch(refCode, @"^\d+$");

            //if (refType)
            //{
            //    var res = await client.GetAsync<IEnumerable<SeatManagementDTO>>(string.Format(Constants.ClientRoutes.GetTripHistory, refCode));

            //    var item = res.Object;

            //    if (res.ValidationErrors.Count() > 0)
            //    {
            //        return View(item).WithWarning(res.Code + ": " + res.ShortDescription);
            //    }
            //    return View(item).WithSuccess(res.ShortDescription);
            //}

            //var response = await client.GetAsync<List<SeatManagementDTO>>(string.Format(Constants.ClientRoutes.BookingDetails, refCode));

            //var items = response.Object;

            //if (response.ValidationErrors.Count() > 0)
            //{
            //    return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            //}
            //return View(items).WithSuccess(response.ShortDescription);
        }

        [HttpGet]
        public async Task<IActionResult> TicketReschedule(string bookingRefCode, string Type)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (Type == "Allocate")
            {
                ViewData["Type"] = Type;
                ViewData["PageTitle"] = "Allocate Seat Information";
            }
            else
            {
                ViewData["PageTitle"] = "Ticket Reschedule";
            }
            var response = await client.GetAsync<SeatManagementDTO>(string.Format(Constants.ClientRoutes.GetBookingDetails, bookingRefCode));

            var item = response.Object;

            if (response.ValidationErrors.Count() > 0)
            {
                return View(response).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(item).WithSuccess(response.ShortDescription);
        }


        [HttpPost]
        public async Task<IActionResult> TicketReschedule(SeatManagementDTO seatManagement)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var rescheduleStatus = seatManagement.PhoneNumber == "Waive" ? RescheduleStatus.Pending : RescheduleStatus.PayAtTerminal;

            var rescheduleDetails = new RescheduleDTO
            {
                SeatManagementId = seatManagement.Id,
                RescheduleMode = RescheduleMode.Admin,
                SeatNumber = seatManagement.SeatNumber,
                VehicleTripRegistrationId = seatManagement.VehicleTripRegistrationId,
                BookingReferenceCode = seatManagement.BookingReferenceCode,
                vType = seatManagement.vType,
                RescheduleStatus = rescheduleStatus
            };

            var response = await client.PostAsJsonAsync<RescheduleDTO, string>(Constants.ClientRoutes.RescheduleBooking, rescheduleDetails);

            var item = response.Object;

            return RedirectToAction("BookingDetail", new { refCode = seatManagement.BookingReferenceCode });
        }

        public async Task<List<TripModel>> RescheduleBookingGetDate(RescheduleDetails ticketDetails)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            //var dateDropdown = await client.GetAsync<List<TripModel>>(string.Format(Constants.ClientRoutes.RouteTrips, ticketDetails.RouteId));
            var dateDropdown = await client.GetAsync<List<TripModel>>($"{Constants.ClientRoutes.RouteTrips}/{ticketDetails.RouteId}");
            var items = dateDropdown.Object;

            List<TripModel> trips = new List<TripModel>();

            trips.AddRange(items);

            foreach (var dtaeValue in items)
            {
                if (dtaeValue.ParentRouteId.HasValue)
                {
                    var newdropValue = await client.GetAsync<List<TripModel>>($"{Constants.ClientRoutes.RouteTrips}/{dtaeValue.ParentRouteId}");
                    foreach (var values in newdropValue.Object)
                    {
                        if (values.DepartureTime == dtaeValue.ParentDepartureTime)
                        {
                            trips.Add(values);
                        }
                    }

                }
            }

            return trips;
        }

        public async Task<AvailabileTripDetail> RescheduleBookingGetSeats(RescheduleDetails ticketDetails)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var dto = new VehicleTripRouteSearchDTO
            {
                DepartureTerminalId = ticketDetails.DepartureTerminalId,
                DestinationTerminalId = ticketDetails.DestinationTerminalId,
                DepartureDate = ticketDetails.DepartureDate
            };

            if (ticketDetails.DepartureDate.Date == DateTime.UtcNow.Date)
            {
                var todaysDeparture = await client.GetAsync<List<AvailabileTripDetail>>($"{Constants.ClientRoutes.RouteBuses}/{ticketDetails.RouteId}");

                var itemstoday = todaysDeparture.Object;

                foreach (var todayitem in itemstoday)
                {
                    if (todayitem.DepartureTime == ticketDetails.NewDepartureTime)
                    {
                        var manifestresponse = await client.GetAsync<ManifestDTO>($"{Constants.ClientRoutes.Manifests}/GetByVehicleTripId/{todayitem.VehicleTripRegistrationId}");

                        var availableSeats = new AvailabileTripDetail
                        {
                            AvailableSeats = manifestresponse.Object.RemainingSeat,
                            VehicleTripRegistrationId = todayitem.VehicleTripRegistrationId,
                            AdultFare = manifestresponse.Object.Amount ?? 0
                        };

                        return availableSeats;
                    }
                }
            }
            var url = string.Format(Constants.ClientRoutes.RouteGet, ticketDetails.RouteId);

            var result = await client.GetAsync<RouteModel>(url);

            var Det = new VehicleTripRouteSearchDTO
            {
                DepartureTerminalId = result.Object.DepartureTerminalId,
                DestinationTerminalId = result.Object.DestinationTerminalId,
                DepartureDate = ticketDetails.DepartureDate,
                BookingReferenceCode = ticketDetails.BookingReferenceCode
            };

            var seatDropDown = await client.PostAsJsonAsync<VehicleTripRouteSearchDTO, GroupedTripsDetail>(Constants.ClientRoutes.Bookings, Det);

            var items = seatDropDown.Object.Departures;

            foreach (var item in items)
            {
                if (ticketDetails.NewDepartureTime == item.DepartureTime)
                {
                    return item;
                }
            }

            return new AvailabileTripDetail();
        }


        [HttpGet]
        public async Task<IActionResult> TicketReroute(string bookingRefCode)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<SeatManagementDTO>(string.Format(Constants.ClientRoutes.GetBookingDetails, bookingRefCode));

            var item = response.Object;
            await FetchRequiredDropDowns();

            if (response.ValidationErrors.Count() > 0)
            {
                return View(response).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(item).WithSuccess(response.ShortDescription);
        }

        [HttpPost]
        public async Task<IActionResult> TicketReroute(string bookingRefCode, DateTime newDate)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //you need to change this from journey chart 
            var response = await client.GetAsync<SeatManagementDTO>(string.Format(Constants.ClientRoutes.GetBookingDetails, bookingRefCode));

            var item = response.Object;
            item.NewDate = newDate;

            if (response.ValidationErrors.Count() > 0)
            {
                return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return RedirectToAction("BookingDetail");
        }

        public async Task<List<TripModel>> RerouteBookingGetDate(RescheduleDetails ticketDetails)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var dateDropdown = await client.GetAsync<List<TripModel>>($"{Constants.ClientRoutes.RouteTrips}/{ticketDetails.RouteId}");
            var items = dateDropdown.Object;

            List<TripModel> trips = new List<TripModel>();

            trips.AddRange(items);

            foreach (var dtaeValue in items)
            {
                if (dtaeValue.ParentRouteId.HasValue)
                {
                    var newdropValue = await client.GetAsync<List<TripModel>>($"{Constants.ClientRoutes.RouteTrips}/{dtaeValue.ParentRouteId}");
                    foreach (var values in newdropValue.Object)
                    {
                        if (values.DepartureTime == dtaeValue.ParentDepartureTime)
                        {
                            trips.Add(values);
                        }
                    }

                }
            }

            return trips;
        }



        [HttpPost]
        public async Task<IActionResult> TicketRerouteUpdate(RescheduleDetails ticketDetails)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var reroutestat = ticketDetails.reroutestatus == "Waive" ? RerouteStatus.Pending : RerouteStatus.PayAtTerminal;

            var seatmgt = await client.GetAsync<SeatManagementDTO>(string.Format(Constants.ClientRoutes.GetBookingDetails, ticketDetails.BookingReferenceCode));
            var items = seatmgt.Object;
            var query = new RescheduleDTO
            {
                SeatManagementId = ticketDetails.SeatManagementId,
                SeatNumber = ticketDetails.SeatNumber,
                BookingReferenceCode = ticketDetails.BookingReferenceCode,
                VehicleModel = items.VehicleModelId,
                RouteId = ticketDetails.RouteId,
                VehicleTripRegistrationId = ticketDetails.VehicleTripRegistrationId,
                DepartureTime = ticketDetails.NewDepartureTime,
                PreviousRouteAmount = items.Amount,
                RerouteStatus = reroutestat,
                newRouteFee = ticketDetails.newRouteFee
            };

            var response = await client.PostAsJsonAsync<RescheduleDTO, string>($"{Constants.ClientRoutes.Booking}/Reroutebooking", query);
            var item = response.Object;

            return RedirectToAction("BookingDetail");
        }

        public IActionResult GetTravelledCustomerDetail()
        {

            return View();
        }

        public async Task<IActionResult> GetData(string startdate, string enddate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var date = Convert.ToDateTime(startdate);
            var dateb = Convert.ToDateTime(enddate);

            var model = new DateModel
            {
                StartDate = date,
                EndDate = date.AddDays(1)
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetTraveledCustomers,
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<DateModel, PagedRecordModel<SeatManagementDTO>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> ExportCustDetails(string startdate)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<SeatManagementDTO>>($"{Constants.ClientRoutes.GetAllTraveledCustomers + "/" + startdate}");

            var items = response.Object ?? new List<SeatManagementDTO> { };

            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"Customerdetail.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Customerdetail");
                int i = 0;
                foreach (var item in items)
                {
                    IRow row = excelSheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(item.FullName);
                    row.CreateCell(1).SetCellValue(item.PhoneNumber);
                    row.CreateCell(2).SetCellValue(item.RouteName);
                    row.CreateCell(3).SetCellValue(item.VehicleName);
                    i++;
                }
                workbook.Write(fs);
            }
            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }

        public async Task<IActionResult> TripExportTableToExcel(BookedTripsQueryModel query, string itemdata)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            string vTermId = "";
            string subtest = itemdata.Split(';')[1];
            string termtest = itemdata.Split(';')[4];
            query.PhysicalBusRegisterationNumber = itemdata.Split(';')[0];

            if (string.IsNullOrEmpty(subtest)) { query.BookingType = 0; }
            else { query.BookingType = int.Parse(itemdata.Split(';')[1]); }

            query.StartDate = Convert.ToDateTime(itemdata.Split(';')[2]);
            query.EndDate = Convert.ToDateTime(itemdata.Split(';')[3]);
            if (string.IsNullOrEmpty(termtest)) { }
            else
            {
                query.TerminalId = Convert.ToInt32(itemdata.Split(';')[4]);
                vTermId = itemdata.Split(';')[4];
            }

            var response = await client.PostAsJsonAsync<BookedTripsQueryModel, List<BookedTripsModel>>($"{Constants.ClientRoutes.BookedTrips}", query);

            IEnumerable<BookedTripsModel> items = null;
            if (vTermId != "")
            {
                var vitems = response.Object ?? new List<BookedTripsModel> { };
                items = vitems.Where(x => x.DepartureTerminal == query.TerminalId && x.IsPrinted.Equals(true));
            }
            else { items = response.Object ?? new List<BookedTripsModel> { }; }

            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"TerminalTripsReport.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);

            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("TerminalTripsReport");
                int i = 0;
                IRow rows = excelSheet.CreateRow(i);
                rows.CreateCell(0).SetCellValue("Route");
                rows.CreateCell(1).SetCellValue("Bus Registration Number");
                rows.CreateCell(2).SetCellValue("Seats Booked");
                rows.CreateCell(3).SetCellValue("Departure Date");
                rows.CreateCell(4).SetCellValue("Departure Time");
                //rows.CreateCell(5).SetCellValue("Manifest Printed Time");
                rows.CreateCell(5).SetCellValue("Manifest Printed Time");
                foreach (var item in items)
                {
                    i++;
                    var row = excelSheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(item.RouteName);
                    row.CreateCell(1).SetCellValue(item.PhysicalBusRegistrationNumber);
                    row.CreateCell(2).SetCellValue(item.SeatsBooked);
                    row.CreateCell(3).SetCellValue(item.DepartureDate.ToShortDateString());
                    row.CreateCell(4).SetCellValue(item.DepartureTime);
                    //row.CreateCell(5).SetCellValue(item.ManifestPrintedTime.ToString("h:mm tt"));
                    row.CreateCell(5).SetCellValue(item.ManifestPrintedTime != DateTime.MinValue ? item.ManifestPrintedTime.ToString("h:mm tt") : "N-A");
                }
                workbook.Write(fs);
            }
            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }

        public IActionResult GetBlowData()
        {
            return View();
        }

        public async Task<IActionResult> GetBlownData(string startdate, string enddate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var date = Convert.ToDateTime(startdate);
            var enddates = Convert.ToDateTime(enddate);

            var model = new DateModel
            {
                StartDate = date,
                EndDate = enddates.AddHours(23)
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetBlownBuses,
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<DateModel, PagedRecordModel<VehicleTripRegistration>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> Blow()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name");

            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Name");

            var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.AvailableVehicles);
            ViewBag.Vehicles = new SelectList(vehicles.Object, "Id", "Details");

            if (!User.HasClaim("Permission", "manageroute"))
            {
                var managersterminals = await client.GetAsync<TerminalModel>(Constants.ClientRoutes.LoginInTerminal);

                List<TerminalModel> terminalModels = new List<TerminalModel>();

                terminalModels.Add(managersterminals.Object);

                ViewBag.DepartureTerminal = new SelectList(terminalModels, "Id", "Name");
            }

            return View();
        }

        [HttpGet]
        public IActionResult IncomingBlow()
        {
            return View();
        }

        public async Task<IActionResult> GetIncomingBlowData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetBlownBuses,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VehicleTripRegistration>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        public async Task<IActionResult> Blow(TerminalBookingModel blowDetails)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var response = await client.PostAsJsonAsync<Blow, bool>(Constants.ClientRoutes.BlowVehicle, blow);
            //var responses = await client.PostAsJsonAsync<TerminalBookingModel, GroupedTripsDetail>(Constants.ClientRoutes.Bookings, blowDetails);
            var responses = await client.GetAsync<List<TerminalModel>>(string.Format(Constants.ClientRoutes.RouteIdByDestinationAndTerminalId, blowDetails.DepartureTerminalId, blowDetails.DestinationTerminalId));

            var items = responses.Object;

            var getTripId = await client.GetAsync<List<TripModel>>($"{Constants.ClientRoutes.RouteTrips}/{items[0].RouteId}");
            var tripId = getTripId.Object;

            if (tripId == null)
            {
                return View().WithWarning(responses.Code + ": " + responses.ShortDescription);
            }

            var vehicleDetails = await client.GetAsync<VehicleModel>(string.Format(Constants.ClientRoutes.VehicleGet, blowDetails.VehicleId));

            var newBlowDetails = new VehicleTripRegistration
            {
                TripId = tripId[0].Id,
                DriverCode = blowDetails.DriverCode,
                PhysicalBusRegistrationNumber = vehicleDetails.Object.RegistrationNumber
            };

            var response = await client.PostAsJsonAsync<VehicleTripRegistration, bool>(Constants.ClientRoutes.CreateBlow, newBlowDetails);

            var item = response.Object;
            //var item = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return RedirectToAction("GetBlowData");
        }

        public async Task<IActionResult> MtuReports(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            //populate vehicle dropdown
            //var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.AvailableVehicles);
            //ViewBag.Vehicles = new SelectList(vehicles.Object, "Id", "Details");

            var vehiclesRequest = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.Vehicles);
            ViewBag.Vehicles = new SelectList(vehiclesRequest.Object.Items, "Id", "Details");
            //populate driver dropdown
            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Name");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> MtuReports(int id, MtuReportModel model)
        {
            var fileName = "";
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            List<MtuPhoto> models = new List<MtuPhoto>();

            var vehicle = await client.GetAsync<VehicleDTO>(string.Format(Constants.ClientRoutes.VehicleGet, model.VehicleId));

            model.RegistrationNumber = vehicle.Object.RegistrationNumber;

            var uploadss = Path.Combine(_hostingEnvironment.WebRootPath, "images/mtureports");

            if (model.Pictures != null)
            {
                foreach (var pics in model.Pictures)
                {
                    fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(pics.FileName).ToString();
                    using (var fileStream = new FileStream(Path.Combine(uploadss, fileName), FileMode.Create))
                    {
                        await pics.CopyToAsync(fileStream);
                        models.Add(new MtuPhoto
                        {
                            FileName = fileName.ToString()
                        });
                        //imgnames.Add(fileName);
                    }
                }
            }
            else
            {

            }

            model.PicturePath = fileName;

            var newModel = new MtuReportModelDTO
            {
                VehicleId = model.VehicleId,
                Picture = fileName,
                RegistrationNumber = model.RegistrationNumber,
                DriverCode = model.DriverCode,
                Status = model.Status,
                Notes = model.Notes,
                MtuPhotos = models
            };

            var response = await client.PostAsJsonAsync<MtuReportModelDTO, bool>($"{Constants.ClientRoutes.AddReport}", newModel);

            ViewBag.Keyword = model.Keyword;

            if (!response.IsValid)
            {
                return new JsonResult(new
                {
                    response.Code,
                    Message = response.ShortDescription,
                }).WithSuccess(response.ShortDescription);
            }
            return RedirectToAction("GetReports");
        }

        public async Task<IActionResult> GetReports(DateModel report)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var employees = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);

            ViewBag.Employee = new SelectList(employees.Object.Items, "Id", "FullName");
            return View();
        }
        public async Task<IActionResult> GetMtuReports(string startdate, string enddate, string name, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var date = Convert.ToDateTime(startdate);
            var enddates = Convert.ToDateTime(enddate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            var model = new DateModel
            {
                StartDate = date,
                EndDate = enddates,
                Keyword = name
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.SearchReport,
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<DateModel, PagedRecordModel<MtuReportModelDTO>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }
        public async Task<IActionResult> GetMtuById(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var getById = await client.GetAsync<MtuReportModelDTO>(string.Format(Constants.ClientRoutes.GetMTUById, id));
            var model = getById.Object;

            var list = model.MtuPhotos;

            foreach (var item in list)
            {
                model.Picture = item.FileName;
            }

            return View(model);
        }
        public async Task<IActionResult> JourneyDetailReport(string journeyType, int terminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            JourneyChartQueryModel model = new JourneyChartQueryModel();
            if (journeyType == "outgoingLoaded")
            {
                model = new JourneyChartQueryModel
                {
                    StartDate = DateTime.Now.Date,
                    EndDate = DateTime.Now,
                    JourneyStatus = JourneyStatus.InTransit,
                    JourneyType = JourneyType.Loaded,
                    DepartureTerminalId = terminalId
                };

                ViewBag.journeyType = "Outgoing Vehicle Loaded";
            }
            if (journeyType == "incomingLoaded")
            {
                model = new JourneyChartQueryModel
                {
                    StartDate = DateTime.Now.Date,
                    EndDate = DateTime.Now,
                    JourneyStatus = JourneyStatus.InTransit,
                    JourneyType = JourneyType.Loaded,
                    DestinationTerminalId = terminalId
                };
                ViewBag.journeyType = "Incoming Vehicle Loaded";
            }
            if (journeyType == "outgoingBlown")
            {
                model = new JourneyChartQueryModel
                {
                    StartDate = DateTime.Now.Date,
                    EndDate = DateTime.Now,
                    JourneyStatus = JourneyStatus.InTransit,
                    JourneyType = JourneyType.Blown,
                    DepartureTerminalId = terminalId
                };
                ViewBag.journeyType = "Outgoing Vehicle Blown";
            }
            if (journeyType == "incomingBlown")
            {
                model = new JourneyChartQueryModel
                {
                    StartDate = DateTime.Now.Date,
                    EndDate = DateTime.Now,
                    JourneyStatus = JourneyStatus.InTransit,
                    JourneyType = JourneyType.Blown,
                    DestinationTerminalId = terminalId
                };
                ViewBag.journeyType = "Incoming Blown Vehicle";
            }
            var response = await client.PostAsJsonAsync<JourneyChartQueryModel, List<JourneyDetailDisplayDTO>>(Constants.ClientRoutes.journeyChartsForTerminals, model);

            var items = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(items);
        }

        public async Task<IActionResult> FleetHistory()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            //populate vehicle dropdown
            var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.AvailableVehicles);
            var vehiclesRequest = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.Vehicles);
            ViewBag.vehics = new SelectList(vehicles.Object, "Id", "RegistrationNumber");
            ViewBag.Vehicles = new SelectList(vehiclesRequest.Object.Items, "Id", "RegistrationNumber");
            //populate driver dropdown
            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Details");
            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name");
            return View();
        }

        public async Task<IActionResult> GetFleetHistory(string startdate, string enddate, string driver, string vehicle, int? departureTerminalId, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var date = Convert.ToDateTime(startdate);
            var enddates = Convert.ToDateTime(enddate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            var model = new SearchGuidDTO
            {
                StartDate = date,
                EndDate = enddates,
                Driver = driver,
                PhysicalBusRegistrationNumber = vehicle,
                DepartureTerminalId = departureTerminalId
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetFleetHistory,
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<SearchGuidDTO, PagedRecordModel<FleetHistoryDTO>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> ExportMTUReports(string startdate, string enddate)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<List<SeatManagementDTO>>($"{Constants.ClientRoutes.SearchReport + "/" + startdate}");

            var items = response.Object ?? new List<SeatManagementDTO> { };

            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"MTUReport.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Customerdetail");
                int i = 0;
                foreach (var item in items)
                {
                    IRow row = excelSheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(item.FullName);
                    row.CreateCell(1).SetCellValue(item.PhoneNumber);
                    row.CreateCell(2).SetCellValue(item.RouteName);
                    row.CreateCell(3).SetCellValue(item.VehicleName);
                    i++;
                }
                workbook.Write(fs);
            }
            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }

        //[HttpGet]
        //public IActionResult TotalSalesReport()
        //{
        //    return View();
        //}

        [HttpGet]
        public async Task<IActionResult> TotalSalesReport(DateTime? StartDate, DateTime? EndDate, int? stateId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var stateRequest = await client.GetAsync<PagedRecordModel<StateModel>>(Constants.ClientRoutes.States);

            ViewBag.StateId = new SelectList(stateRequest.Object.Items, "Id", "Name", stateId);

            if (StartDate == null || EndDate == null || stateId == 0)
            {
                return View();
            }

            var query = new DateModel
            {
                StartDate = StartDate,
                EndDate = EndDate,
                Id = stateId
            };

            ViewBag.StartDate = StartDate;
            ViewBag.EndDate = EndDate;

            var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TotalSalesReportPerState, query);

            var items = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(items).WithSuccess(response.Code + ": " + response.ShortDescription);
        }
        [HttpGet]
        public async Task<IActionResult> TotalRevenueReport(DateTime? StartDate, DateTime? endDate, int? stateId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);


            var stateRequest = await client.GetAsync<PagedRecordModel<StateModel>>(Constants.ClientRoutes.States);

            ViewBag.StateId = new SelectList(stateRequest.Object.Items, "Id", "Name", stateId);

            if (StartDate == null || endDate == null || stateId == 0)
            {
                return View();
            }
            //End of day
            var EndDate = Convert.ToDateTime(endDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var query = new DateModel
            {
                StartDate = StartDate,
                EndDate = EndDate,
                Id = stateId
            };

            ViewBag.StartDate = StartDate;
            ViewBag.EndDate = EndDate;

            var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TotalRevenueReportPerState, query);

            var items = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(items).WithSuccess(response.Code + ": " + response.ShortDescription);
        }
        public async Task<IActionResult> ExportTotalRevenueReport(DateTime? StartDate, DateTime? endDate, int? stateId)
        {
            //var exportedFile = @"D:\\passengerreport.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"TotalRevenue.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }
                //var file = Path.Combine(Directory.GetCurrentDirectory(),
                //            "MyStaticFiles", "images", exportedFile);

                var EndDate = Convert.ToDateTime(endDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                var query = new DateModel
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    Id = stateId
                };

                ViewBag.StartDate = StartDate;
                ViewBag.EndDate = EndDate;

                var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TotalRevenueReportPerState, query);

                var items = response.Object ?? new List<SalesReportDTO> { };
                foreach (var x in items)
                    x.TotalRevenue = x.TotalOnlineRevenue + x.TotalAdvancedRevenue + x.TotalTerminalRevenue;

                using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();

                    ISheet passengersheet = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = passengersheet.CreateRow(rowIndex);
                    row.CreateCell(0).SetCellValue("State");
                    row.CreateCell(1).SetCellValue("Total Revenue");
                    row.CreateCell(2).SetCellValue("Total Terminal Revenue");
                    row.CreateCell(3).SetCellValue("Total Online Revenue");
                    row.CreateCell(4).SetCellValue("Total Advanced Revenue");
                    row.CreateCell(5).SetCellValue("Total Agent Revenue");

                    foreach (var passenger in items)
                    {
                        rowIndex++;
                        var passengerRow = passengersheet.CreateRow(rowIndex);
                        passengerRow.CreateCell(0).SetCellValue(passenger.Name ?? "No value");
                        passengerRow.CreateCell(1).SetCellValue(passenger.TotalRevenue.ToString() ?? "No value");
                        passengerRow.CreateCell(2).SetCellValue(passenger.TotalTerminalRevenue.ToString() ?? "No value");
                        passengerRow.CreateCell(3).SetCellValue(passenger.TotalOnlineRevenue.ToString() ?? "No value");
                        passengerRow.CreateCell(4).SetCellValue(passenger.TotalAdvancedRevenue.ToString() ?? "No value");
                        passengerRow.CreateCell(5).SetCellValue(passenger.TotalAgentRevenue.ToString() ?? "No value");
                    }
                    workbook.Write(fs);
                }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                //log
                return View();
            }

        }

        [HttpGet]
        public async Task<IActionResult> TotalTerminalSalesPerStateReport(DateTime? startDate, DateTime? endDate, int? stateId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (startDate == null || endDate == null || stateId == 0)
            {
                return View();
            }

            var query = new DateModel
            {
                StartDate = startDate,
                EndDate = endDate,
                Id = stateId
            };

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;

            var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TotalTerminalSalesInState, query);

            var items = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(items).WithSuccess(response.Code + ": " + response.ShortDescription);
        }
        [HttpGet]
        public async Task<IActionResult> TotalTerminalRevenuePerStateReport(DateTime? startDate, DateTime? endDate, int? stateId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (startDate == null || endDate == null || stateId == 0)
            {
                return View();
            }
            var EndDate = Convert.ToDateTime(endDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var query = new DateModel
            {
                StartDate = startDate,
                EndDate = EndDate,
                Id = stateId
            };

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.StateId = stateId;

            var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TotalTerminalRevenueInState, query);

            var items = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(items).WithSuccess(response.Code + ": " + response.ShortDescription);
        }

        public async Task<IActionResult> ExportTotalTerminalRevenuePerStateReport(DateTime? startDate, DateTime? endDate, int? stateId)
        {
            //var exportedFile = @"D:\\passengerreport.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"TotalTerminalRevenuePerState.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }
                var EndDate = Convert.ToDateTime(endDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                var query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = EndDate,
                    Id = stateId
                };

                ViewBag.StartDate = startDate;
                ViewBag.EndDate = endDate;

                var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TotalTerminalRevenueInState, query);

                var items = response.Object ?? new List<SalesReportDTO> { };
                foreach (var x in items)
                    x.TotalRevenue = x.TotalOnlineRevenue + x.TotalAdvancedRevenue + x.TotalTerminalRevenue;

                using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();

                    ISheet passengersheet = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = passengersheet.CreateRow(rowIndex);
                    row.CreateCell(0).SetCellValue("State");
                    row.CreateCell(1).SetCellValue("Total Revenue");
                    row.CreateCell(2).SetCellValue("Total Terminal Revenue");
                    row.CreateCell(3).SetCellValue("Total Online Revenue");
                    row.CreateCell(4).SetCellValue("Total Advanced Revenue");
                    row.CreateCell(5).SetCellValue("Total Agent Revenue");

                    foreach (var passenger in items)
                    {
                        rowIndex++;
                        var passengerRow = passengersheet.CreateRow(rowIndex);
                        passengerRow.CreateCell(0).SetCellValue(passenger.Name ?? "No value");
                        passengerRow.CreateCell(1).SetCellValue(passenger.TotalRevenue.ToString() ?? "No value");
                        passengerRow.CreateCell(2).SetCellValue(passenger.TotalTerminalRevenue.ToString() ?? "No value");
                        passengerRow.CreateCell(3).SetCellValue(passenger.TotalOnlineRevenue.ToString() ?? "No value");
                        passengerRow.CreateCell(4).SetCellValue(passenger.TotalAdvancedRevenue.ToString() ?? "No value");
                        passengerRow.CreateCell(5).SetCellValue(passenger.TotalAgentRevenue.ToString() ?? "No value");
                    }
                    workbook.Write(fs);
                }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                //log
                return View();
            }

        }

        [HttpGet]
        public async Task<IActionResult> TotalTerminalSalesSummary(DateTime? startDate, DateTime? endDate, int bookingType, int? terminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (startDate == null || endDate == null || terminalId == 0)
            {
                return View();
            }

            DateModel query = new DateModel();

            if (bookingType == 0)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Id = terminalId,
                    BookingType = BookingTypes.Terminal
                };
            if (bookingType == 1)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Id = terminalId,
                    BookingType = BookingTypes.Advanced
                };
            if (bookingType == 2)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Id = terminalId,
                    BookingType = BookingTypes.Online
                };
            if (bookingType == 4)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Id = terminalId,
                    BookingType = BookingTypes.All
                };
            if (bookingType == 5)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Id = terminalId,
                    BookingType = BookingTypes.Agent
                };
            ViewBag.BookingType = bookingType;

            var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TerminalSalesSummary, query);

            var items = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(items).WithSuccess(response.Code + ": " + response.ShortDescription);
        }

        [HttpGet]
        public async Task<IActionResult> TotalTerminalRevenueSummary(DateTime? startDate, DateTime? endDate, int bookingType, int? terminalId)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            if (startDate == null || endDate == null || terminalId == 0)
            {
                return View();
            }
            var EndDate = Convert.ToDateTime(endDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            DateModel query = new DateModel();

            if (bookingType == 0)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = EndDate,
                    Id = terminalId,
                    BookingType = BookingTypes.Terminal
                };
            if (bookingType == 1)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = EndDate,
                    Id = terminalId,
                    BookingType = BookingTypes.Advanced
                };
            if (bookingType == 2)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = EndDate,
                    Id = terminalId,
                    BookingType = BookingTypes.Online
                };
            if (bookingType == 4)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = EndDate,
                    Id = terminalId,
                    BookingType = BookingTypes.All
                };
            if (bookingType == 5)
                query = new DateModel
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Id = terminalId,
                    BookingType = BookingTypes.Agent
                };
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.BookingType = bookingType;
            ViewBag.TerminalId = terminalId;

            var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TerminalRevenueSummary, query);

            var items = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return View(items).WithSuccess(response.Code + ": " + response.ShortDescription);
        }
        public async Task<IActionResult> ExportTotalTerminalRevenueSummary(DateTime? startDate, DateTime? endDate, int bookingType, int? terminalId)
        {
            //var exportedFile = @"D:\\passengerreport.xlsx";
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"TotalTerminalRevenueSummary.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                if (!ModelState.IsValid)
                {
                    await Task.CompletedTask;
                }
                var EndDate = Convert.ToDateTime(endDate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                DateModel query = new DateModel();

                if (bookingType == 0)
                    query = new DateModel
                    {
                        StartDate = startDate,
                        EndDate = EndDate,
                        Id = terminalId,
                        BookingType = BookingTypes.Terminal
                    };
                if (bookingType == 1)
                    query = new DateModel
                    {
                        StartDate = startDate,
                        EndDate = EndDate,
                        Id = terminalId,
                        BookingType = BookingTypes.Advanced
                    };
                if (bookingType == 2)
                    query = new DateModel
                    {
                        StartDate = startDate,
                        EndDate = EndDate,
                        Id = terminalId,
                        BookingType = BookingTypes.Online
                    };
                if (bookingType == 4)
                    query = new DateModel
                    {
                        StartDate = startDate,
                        EndDate = EndDate,
                        Id = terminalId,
                        BookingType = BookingTypes.All
                    };
                if (bookingType == 5)
                    query = new DateModel
                    {
                        StartDate = startDate,
                        EndDate = endDate,
                        Id = terminalId,
                        BookingType = BookingTypes.Agent
                    };
                ViewBag.BookingType = bookingType;

                var response = await client.PostAsJsonAsync<DateModel, List<SalesReportDTO>>(Constants.ClientRoutes.TerminalRevenueSummary, query);

                var items = response.Object ?? new List<SalesReportDTO> { };


                using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();

                    ISheet passengersheet = workbook.CreateSheet("Sheet1");
                    var rowIndex = 0;
                    IRow row = passengersheet.CreateRow(rowIndex);
                    row.CreateCell(0).SetCellValue("Total Amount");
                    row.CreateCell(1).SetCellValue("Bus Number");
                    row.CreateCell(2).SetCellValue("Route");

                    foreach (var passenger in items)
                    {
                        rowIndex++;
                        var passengerRow = passengersheet.CreateRow(rowIndex);
                        passengerRow.CreateCell(0).SetCellValue(passenger.AmountTotal.ToString() ?? "No value");
                        passengerRow.CreateCell(1).SetCellValue(passenger.PhysicalBusRegistrationNumber ?? "No value");
                        passengerRow.CreateCell(2).SetCellValue(passenger.RouteName.ToString() ?? "No value");
                    }
                    workbook.Write(fs);
                }
                using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            }
            catch (Exception ex)
            {
                //log
                return View();
            }

        }

        public async Task<List<PassengerViewModel>> GetManifestByVehicleTripId(Guid id, int bookingtype)
        {
            var items = new List<PassengerViewModel>();
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var url = $"{Constants.ClientRoutes.ManifestByVehicleTrip}/{id}/{bookingtype}";
            var response = await client.GetAsync<List<PassengerViewModel>>(url);
            items = response.Object ?? new List<PassengerViewModel> { };
            if (response.ValidationErrors.Count() > 0)
            {
                return items;
            }
            return items;
        }
        public async Task<List<PassengerViewModel>> GetManifestByVehicleTripIdWithoutDispatch(Guid id, int bookingtype)
        {
            var items = new List<PassengerViewModel>();
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var url = $"{Constants.ClientRoutes.ManifestByVehicleTripWithoutDispatch}/{id}/{bookingtype}";
            var response = await client.GetAsync<List<PassengerViewModel>>(url);
            items = response.Object ?? new List<PassengerViewModel> { };
            if (response.ValidationErrors.Count() > 0)
            {
                return items;
            }
            return items;
        }

        public async Task<DriverModel> GetDriver(string registrationNo)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //var regValue = registrationNo.Split('(');

            var driverDetails = await client.GetAsync<DriverModel>(string.Format(Constants.ClientRoutes.GetDriverByVehicleId, registrationNo));

            var item = driverDetails.Object;

            item.Picture = string.Format("/images/{0}", item.Picture);

            return item;
        }

        public async Task<IActionResult> Transload()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.GetVirtualAndPhysicalTerminals);

            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name");

            var routesRequest = await client.GetAsync<PagedRecordModel<RouteModel>>(Constants.ClientRoutes.Routes);

            ViewBag.RouteId = new SelectList(routesRequest.Object.Items ?? new List<RouteModel>(), "Id", "Name");

            //var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.AvailableVehicles);

            //ViewBag.Vehicles = new SelectList(vehicles.Object, "RegistrationNumber", "Details");

            var vehiclesRequest = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.Vehicles);

            ViewBag.Vehicles = new SelectList(vehiclesRequest.Object.Items, "RegistrationNumber", "Details");
            //populate driver dropdown
            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Details");


            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Transload(Guid Id, Transload transload)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //Get vehicletripDetailByVehicleTripRegistrationId
            var PhysicalBus = await client.GetAsync<VehicleTripRegistration>(string.Format(Constants.ClientRoutes.GetVehicleDetails, transload.VehicleTripRegistrationId));
            var results = PhysicalBus.Object;
            //New Info
            transload.PhysicalBusRegistrationNumber = transload.TransloadedVehicle;
            transload.PilotCode = transload.TransloadedPilot;
            //Old Info
            transload.TransloadedVehicle = PhysicalBus.Object.PhysicalBusRegistrationNumber;
            transload.TransloadedPilot = results.DriverCode;

            var response = await client.PostAsJsonAsync<Transload, bool>($"{Constants.ClientRoutes.Transload}", transload);

            return View(transload);
        }
        public async Task<IActionResult> Workshop()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            //populate vehicle dropdown
            var vehicles = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.AvailableVehicles);

            ViewBag.Vehicles = new SelectList(vehicles.Object, "Id", "Details");
            //All vehicles 
            var vehiclesRequest = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.Vehicles);

            ViewBag.Allvehicles = new SelectList(vehiclesRequest.Object.Items, "Id", "Details");

            ViewBag.NotInWorkshopVehicles = new SelectList(vehiclesRequest.Object.Items.Where(v => v.Status != VehicleStatus.InWorkshop.ToString()), "Id", "Details");

            //var inWorkShopVeh = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.GetVehiclesInWorkshop);

            ViewBag.inWorkShopVeh = new SelectList(vehiclesRequest.Object.Items.Where(v => v.Status == VehicleStatus.InWorkshop.ToString()), "Id", "Details");

            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.GetVirtualAndPhysicalTerminals);

            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name");

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Workshop(MtuReportModelDTO workshop)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var vehicle = await client.GetAsync<VehicleDTO>(string.Format(Constants.ClientRoutes.VehicleGet, workshop.VehicleId));

            workshop.RegistrationNumber = vehicle.Object.RegistrationNumber;

            var response = await client.PostAsJsonAsync<MtuReportModelDTO, bool>($"{Constants.ClientRoutes.Workshop}", workshop);

            WorkshopDTO model = new WorkshopDTO();
            if (workshop.Type == 1)//1 indicates "send to workshop"
            {
                //Create details for a bus moved to workshop
                //model.CreationTime = DateTime.Now;//would be passed at the api
                //model.CreatorUserId would be passed at the api
                //model.LastModificationTime = DateTime.Now;//would be passed at the api
                //model.LastModifierUserId would be passed at the api
                model.VehicleStatus = VehicleStatus.InWorkshop;
                model.VehicleId = Convert.ToInt32(workshop.VehicleId);
                model.VehicleLocationId = (int)(vehicle.Object.LocationId ?? 0);
                model.WorkshopLocationId = Convert.ToInt32(workshop.TerminalId);
                model.WorkshopStatus = true;
                model.ReleaseNote = workshop.Notes;

                //next, pass the model to the api
                var ws = await client.PostAsJsonAsync<WorkshopDTO, int>(Constants.ClientRoutes.WorkshopBusCreate, model);
            }
            else if (workshop.Type == 2)//2 indicates "release from workshop"
            {
                try
                {
                    var releaseResponse = await client.GetAsync<bool>(string.Format(Constants.ClientRoutes.WorkshopBusRelease, workshop.VehicleId, workshop.TerminalId, workshop.Notes));
                }
                catch (Exception ex)
                {

                    throw;
                }

            }
            return RedirectToAction("Workshop");
        }
        public async Task<IActionResult> VehicleByStatus()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var vehiclesRequest = await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.Vehicles);

            ViewBag.Allvehicles = new SelectList(vehiclesRequest.Object.Items, "Id", "Details");
            return View();
        }

        public async Task<IActionResult> GetVehicleByStatus(VehicleStatus vehStatus, int? id, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var model = new DateModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                status = vehStatus,
                Id = id
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetVehicleByStatus,
                page, request.Length, request.Search.Value);
            var result = await client.PostAsJsonAsync<DateModel, PagedRecordModel<VehicleDTO>>(url, model);
            var pagedData = result.Object;
            foreach (var item in result.Object.Items.ToList())
            {
                if (item.LicenseDate == null)
                {
                    item.DateLicense = "License Date Empty";
                    item.DateLicenseExpired = "Expired License Date Empty";
                }
                else
                {
                    item.DateLicense = item.LicenseDate?.ToString(Constants.HumanDateFormat);
                    item.DateLicenseExpired = item.ExpiredLicenseDate?.ToString(Constants.HumanDateFormat);
                }
            }
            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateVehicleStatus(int vehiclestatusinputid, VehicleStatus? status)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            int id = vehiclestatusinputid;
            var response = await client.GetAsync(string.Format($"{Constants.ClientRoutes.UpdateVehicleStatus}", id, status));
            return RedirectToAction("VehicleByStatus");
        }
        public async Task<IActionResult> GetTransloaded()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var employees = await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees);

            ViewBag.Employee = new SelectList(employees.Object.Items, "Id", "FullName");
            return View();
        }
        public async Task<IActionResult> GetTransloadedHistory(string startdate, string enddate, int Id, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var startdates = Convert.ToDateTime(startdate);
            var enddates = Convert.ToDateTime(enddate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            var model = new SearchDTO
            {
                StartDate = startdates,
                EndDate = enddates,
                Id = Id
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetTransload,
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<SearchDTO, PagedRecordModel<Transload>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);
        }
        public async Task<IActionResult> CancelTicket(string bookingRefCode)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var query = new SeatManagementDTO
            {
                BookingReferenceCode = bookingRefCode
            };

            var res = await client.GetAsync<bool>(string.Format(Constants.ClientRoutes.CancelSeat, bookingRefCode));

            //var response = await client.PostAsJsonAsync<int, bool>($"{Constants.ClientRoutes.activateaccount}/{Id}", Id);
            return RedirectToAction("BookingDetail", new { refCode = bookingRefCode });

        }

        public async Task<IActionResult> AgentReport()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            //populate agent dropdown
            var agentRequest = await client.GetAsync<PagedRecordModel<AgentDTO>>($"{Constants.ClientRoutes.Agents}/get");
            ViewBag.agents = new SelectList(agentRequest.Object.Items, "Id", "FullName");
            return View();
        }
        public async Task<IActionResult> GetAgentHistory(string startdate, string enddate, int AgentId, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var date = Convert.ToDateTime(startdate);
            var enddates = Convert.ToDateTime(enddate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            var model = new SearchDTO
            {
                StartDate = date,
                EndDate = enddates,
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", ($"{Constants.ClientRoutes.Agents}/AgentReports/{AgentId}"),
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<SearchDTO, PagedRecordModel<AgentDTO>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);
        }
        public IActionResult DriverDetails()
        {
            return View();
        }
        public async Task<IActionResult> GetDriverDetails(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.DriverDetails,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<DriverModel>>(url);

            var pagedData = result.Object;
            foreach (var item in result.Object.Items.ToList())
            {
                if (item.LicenseDate == null || item.VehicleLicenseDate == null)
                {
                    item.DateDriverLicense = "";
                    item.DateDriverLicenseExpired = "";
                    item.DateVehicleLicense = "";
                    item.DateVehicleLicenseExpired = "";
                }
                else
                {
                    if (item.ExpiredLicenseDate < DateTime.Now)
                    {
                        item.IsExpired = true;
                        item.DateDriverLicense = item.LicenseDate?.ToString(Constants.HumanDateFormat);
                        item.DateDriverLicenseExpired = item.ExpiredLicenseDate?.ToString(Constants.HumanDateFormat);
                        item.DateVehicleLicense = item.VehicleLicenseDate?.ToString(Constants.HumanDateFormat);
                        item.DateVehicleLicenseExpired = item.VehicleExpiredLicenseDate?.ToString(Constants.HumanDateFormat);
                    }
                    else
                    {
                        item.DateDriverLicense = item.LicenseDate?.ToString(Constants.HumanDateFormat);
                        item.DateDriverLicenseExpired = item.ExpiredLicenseDate?.ToString(Constants.HumanDateFormat);
                        item.DateVehicleLicense = item.VehicleLicenseDate?.ToString(Constants.HumanDateFormat);
                        item.DateVehicleLicenseExpired = item.VehicleExpiredLicenseDate?.ToString(Constants.HumanDateFormat);
                    }
                }
            }

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetDriverTransaction(string code)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var url = $"{Constants.ClientRoutes.Driver}/GetByCode/{code}";
            var response = await client.GetAsync<DriverDTO>(url);

            var WalletBalanceAfterDeduction = await client.GetAsync<decimal>(string.Format(Constants.ClientRoutes.GetDeductedWalletBalance, code));//wallet balance after debit deduction

            ViewBag.bal = response?.Object?.WalletBalance;//wallet balance before debit deduction
            ViewBag.Id = response?.Object?.Id;
            ViewBag.driver = response?.Object?.Name;
            ViewBag.code = code;
            ViewBag.WalletBalanceAfterDeduction = WalletBalanceAfterDeduction.Object;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SendAllForApproval()
        {
            //get all drivers
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);

            if (drivers.ValidationErrors.Count > 0)
            {
                return View(drivers.Object.Items).WithWarning(drivers.Code + ": " + drivers.ShortDescription);
            }

            //foreach driver wallet transaction, send the data for approval
            foreach (var driver in drivers.Object.Items)
            {
                var url = string.Format(Constants.ClientRoutes.UpdateDriverPayment, driver.Code);
                var result = await client.GetAsync<bool>(url);
            }

            return View(drivers.Object.Items).WithSuccess(drivers.Code + ": " + drivers.ShortDescription);
        }

        public async Task<IActionResult> ApproveAll()
        {
            return default;
        }

        public async Task<IActionResult> GetDriverTransactions(string code, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", ($"{Constants.ClientRoutes.Driver}/GetDriverTransaction/{code}"),
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<WalletTransactionDTO>>(url);
            var pagedData = result.Object;

            DataTablesResponse response;
            if (pagedData == null) { response = DataTablesResponse.Create(request, default, default, default); }
            else
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            }
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> UpdateWallet(string code, decimal amount)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var res = await client.GetAsync<bool>(string.Format(Constants.ClientRoutes.UpdateDriverWallet, code, amount));

            var items = res.Object;
            if (res.ValidationErrors.Count() > 0)
            {
                return RedirectToAction("GetDriverTransaction", new { code = code });
            }
            return RedirectToAction("GetDriverTransaction", new { code = code });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateAllWallet(DriverSalaryQueryModel query)
        {
            //STEP 1: get all driver details (work interval, bank account and number, total amount etc.)
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (!ModelState.IsValid) { return View();/*return some sort of error response*/}

            //just in case the data wasn't passed  
            query.EndDate = query.EndDate ?? DateTime.Now;
            query.StartDate = query.StartDate ?? DateTime.Now.Date;

            //pass the query back to the driver salary query
            ViewBag.EndDate = query.EndDate;
            ViewBag.StartDate = query.StartDate;
            ViewBag.DriverCode = query.DriverCode;

            //pass the driver data to the driver code dropdown in the form
            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Details");

            //get driver salary detail
            var driverSalaryQueryResponse = await client.PostAsJsonAsync<DriverSalaryQueryModel, List<DriverSalaryModel>>($"{Constants.ClientRoutes.DriverSalary}", query);
            var items = driverSalaryQueryResponse.Object ?? new List<DriverSalaryModel> { };

            if (driverSalaryQueryResponse.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(driverSalaryQueryResponse.Code + ": " + driverSalaryQueryResponse.ShortDescription);
            }

            //STEP 2: update each driver wallet
            foreach (var item in items)
            {
                var updateDriverWalletResponse = await client.GetAsync<bool>(string.Format(Constants.ClientRoutes.UpdateDriverWallet, item.DriverCode, item.DriverFee));
            }

            return View(items).WithSuccess(driverSalaryQueryResponse.ShortDescription);
        }
        public async Task<IActionResult> PayDriver(int Id, decimal amount, TransactionType transType, PayTypeDescription payTypeDisc, string notes, string payDate)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            DateTime? TransDate = Convert.ToDateTime(payDate);

            var model = new WalletTransactionDTO
            {
                DriverId = Id,
                TransactionAmount = amount,
                TransactionType = transType,
                PayTypeDiscription = payTypeDisc,
                TransDescription = notes,
                TransactionDate = TransDate.GetValueOrDefault()
            };
            var response = await client.PostAsJsonAsync<WalletTransactionDTO, bool>(Constants.ClientRoutes.PayDriver, model);
            var item = response.Object;
            //var item = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return RedirectToAction("DriverSalary");
        }
        public async Task<IActionResult> EditDriverTransactions(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.GetDriverWalletTransactionById, id);
            var result = await client.GetAsync<WalletTransactionDTO>(url);

            var model = result.Object;

            return Json(new
            {
                data = model
            });
        }

        [HttpPost]
        public async Task<IActionResult> WalletModifyUpdate(Guid Id, WalletTransactionDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.UpdateDriverWalletTransaction, model.Id);
                var result = await client.PutAsync<WalletTransactionDTO, bool>(url, model);
                return RedirectToAction("GetDriverTransaction", new { code = model.Code });
            }
            return RedirectToAction("GetDriverTransaction", new { code = model.Code });
        }

        public async Task<IActionResult> UpdateDriverPayment(string code)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.UpdateDriverPayment, code);
            var result = await client.GetAsync<bool>(url);

            var res = result.Object;

            return RedirectToAction("GetDriverTransaction", new { code = code });
        }

        [HttpPost]
        public async Task<IActionResult> RemovePayment(Guid Id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RemovePayment, Id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateHireBus(Guid Id, DateTime ArrivalDate, int ReceivedLocation)
        {
            if (ArrivalDate == null || Id == null) return BadRequest("Invalid values passed!!");

            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var hireBusUrl = string.Format("{0}/{1}/{2}", Constants.ClientRoutes.HireABus, "Get", Id);
            var hireBusResponse = await client.GetAsync<HireBusDTO>(hireBusUrl);
            var hirebus = hireBusResponse.Object;

            if (hirebus == null) return BadRequest("The choosen hire bus doesn't exist!!");

            hirebus.ArrivalDate = ArrivalDate;
            hirebus.ReceivedLocationId = ReceivedLocation;

            var updateHireBusUrl = string.Format("{0}/{1}", Constants.ClientRoutes.HireABus, "Update");
            var response = await client.PostAsJsonAsync<HireBusDTO, bool>(updateHireBusUrl, hirebus);
            if (response.Object)
            {
                return Ok();
            }
            return BadRequest("An error occured while trying to save input!");
        }
        [HttpGet]
        public async Task<IActionResult> WorkshopSummary(DateTime? StartDate, DateTime? EndDate, int? VehicleId)
        {
            var s = TempData["hmm"] as DateModel;
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            List<WorkshopSummaryModel> workshopSummary = new List<WorkshopSummaryModel>();
            List<WorkshopDTO> workshopResponse = new List<WorkshopDTO>();
            IServiceResponse<PagedRecordModel<WorkshopDTO>> workshop = new ServiceResponse<PagedRecordModel<WorkshopDTO>>();
            if (StartDate != null && EndDate != null)
            {
                if (StartDate > EndDate) { return BadRequest("Invalid date range, make sure that the start date isn't ahead of the end date!"); }
                StartDate = StartDate.Value.Date;
                EndDate = EndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var query = new DateModel
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    Id = VehicleId
                };
                //Searched Workshop
                workshop = (await client.PostAsJsonAsync<DateModel, PagedRecordModel<WorkshopDTO>>($"{Constants.ClientRoutes.WorkshopBusPostSearchModel}", query));
                workshopResponse = workshop.Object.Items.ToList();
            }
            else
            {
                //All workshop
                workshop = (await client.GetAsync<PagedRecordModel<WorkshopDTO>>($"{Constants.ClientRoutes.WorkshopBusGet}"));
                workshopResponse = workshop.Object.Items.ToList();
            }
            //All vehicles 
            var vehiclesResponse = (await client.GetAsync<PagedRecordModel<VehicleModel>>(Constants.ClientRoutes.Vehicles)).Object.Items.ToList();
            ViewBag.VehicleId = new SelectList(vehiclesResponse, "Id", "RegistrationNumber");
            //All employees
            var employees = (await client.GetAsync<PagedRecordModel<EmployeeModel>>(Constants.ClientRoutes.Employees)).Object.Items.ToList();
            foreach (var w in workshopResponse)
            {
                var vehicleResponseRegistrationNumber = vehiclesResponse.Where(v => v.Id == w.VehicleId).FirstOrDefault().RegistrationNumber;
                var receivedBy = employees.Where(e => e.UserId == w.CreatorUserId.ToString())?.FirstOrDefault()?.FullName;
                var releasedBy = employees.Where(e => e.UserId == w.ReleaseUserId.ToString())?.FirstOrDefault()?.FullName;
                WorkshopSummaryModel row = new WorkshopSummaryModel
                {
                    Vehicle = vehicleResponseRegistrationNumber,
                    WorkshopReason = w.WorkshopNote,
                    ReceivedBy = receivedBy,
                    ReceivedDate = w.CreationTime,
                    ReleasedBy = releasedBy,
                    ReleasedDate = w.ReleaseDate
                };
                workshopSummary.Add(row);
            }
            StartDate = null; EndDate = null; VehicleId = null;
            if (workshop.ValidationErrors.Count > 0)
            {
                return View("WorkshopSummary", workshopSummary).WithWarning(workshop.Code + ": " + workshop.ShortDescription);
            }
            return View("WorkshopSummary", workshopSummary).WithSuccess(workshop.Code + ": " + workshop.ShortDescription);
            //ViewBag.StartDate = StartDate;
            //ViewBag.EndDate = EndDate;
        }
        public async Task<IActionResult> PilotApproval()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            //populate agent dropdown
            //var approvepilot = await client.GetAsync<PagedRecordModel<WalletTransactionDTO>>($"{Constants.ClientRoutes.PilotApproval}");
            //ViewBag. = new SelectList(.Object.Items, "Id", "FullName");
            return View();
        }

        public async Task<IActionResult> PilotApprovals(TransactionStatus status, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", ($"{Constants.ClientRoutes.Driver}/pilotApproval/{status}"),
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<WalletTransactionDTO>>(url);
            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<List<WalletTransactionDTO>> GetPilotTransactionDetail(string code, int type)
        {
            var items = new List<WalletTransactionDTO>();
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var response = await client.GetAsync<List<WalletTransactionDTO>>($"{Constants.ClientRoutes.GetPilotTransactionDetail}/{code}/{type}");

                items = response.Object ?? new List<WalletTransactionDTO> { };


                if (response.ValidationErrors.Count() > 0)
                {
                    return items;
                }
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }

        }

        [HttpGet]
        public async Task<IActionResult> VerifyPilotTransaction(string code)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VerifyPilotTransaction, code);

            var result = await client.GetAsync<bool>(url);

            return RedirectToAction("PilotApproval", new { code = code });
        }

        [HttpGet]
        public async Task<IActionResult> VerifyAllPilotTransaction()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VerifyAllPilotTransaction);

            var result = await client.GetAsync<bool>(url);

            return RedirectToAction("PilotApproval");
            //return View();
        }

        [HttpGet]
        public async Task<IActionResult> ApprovePilotTransaction(string code)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ApprovePilotTransaction, code);

            var result = await client.GetAsync<bool>(url);

            return RedirectToAction("PilotApproval", new { code = code });
        }

        [HttpGet]
        public async Task<IActionResult> ApproveAllPilotTransaction()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.ApproveAllPilotTransaction);

            var result = await client.GetAsync<bool>(url);

            return RedirectToAction("PilotApproval");
            //return View();
        }

        [HttpGet]
        public async Task<IActionResult> PilotPaySlip()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Details");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> PilotPaySlip(DriverSalaryQueryModel query)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            if (!ModelState.IsValid)
            {
                return View();
            }
            //get pilot name
            var driver = await client.GetAsync<DriverDTO>($"{Constants.ClientRoutes.Driver}/GetByCode/{query.Code}");
            ViewBag.DriverName = driver.Object.Name;

            //for details on slip page
            var firstDayOfMonth = new DateTime(query.StartDate.GetValueOrDefault().Year, query.StartDate.GetValueOrDefault().Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            ViewBag.StartDate = firstDayOfMonth.ToString("dddd, dd MMMM yyyy");
            ViewBag.EndDate = lastDayOfMonth.ToString("dddd, dd MMMM yyyy");
            ViewBag.Code = query.Code;

            //for query
            query.EndDate = query.EndDate ?? DateTime.Now;
            query.StartDate = query.StartDate ?? DateTime.Now.Date;

            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Details");

            var response = await client.PostAsJsonAsync<DriverSalaryQueryModel, List<WalletTransactionDTO>>($"{Constants.ClientRoutes.PilotPaySlip}", query);
            var items = response.Object ?? new List<WalletTransactionDTO> { };
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View(items).WithSuccess(response.ShortDescription);
        }
        [HttpGet]
        public IActionResult PilotPaySchedule()
        {
            return View();
        }
        public async Task<IActionResult> GetPilotPaySchedules(string startdate, string enddate, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var date = Convert.ToDateTime(startdate);
            var enddates = Convert.ToDateTime(enddate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            var model = new SearchDTO
            {
                StartDate = date,
                EndDate = enddates,
            };
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", ($"{Constants.ClientRoutes.Driver}/PilotPaySchedule"),
                page, request.Length, request.Search.Value);
            var result = await client.PostAsJsonAsync<SearchDTO, PagedRecordModel<WalletTransactionDTO>>(url, model);
            var pagedData = result.Object;

            DataTablesResponse response;
            if (pagedData == null) { response = DataTablesResponse.Create(request, default, default, default); }
            else
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            }
            return new DataTablesJsonResult(response, true);
        }
        public async Task<string> ResetWalletBalance(string code, decimal amount)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var response = await client.GetAsync<decimal>(string.Format(Constants.ClientRoutes.ResetWalletBalance, code, amount));

            var item = response.Object;
            if (response.ValidationErrors.Count() > 0)
            {
                return response.Code + ": " + response.ShortDescription;
            }
            return item.ToString();

        }
        public IActionResult GeneralTransaction()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            return View();
        }
        public async Task<IActionResult> GetGeneralTransaction(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", ($"{Constants.ClientRoutes.GetAllGeneralTransaction}"),
                page, request.Length, request.Search.Value);
            var result = await client.GetAsync<PagedRecordModel<GeneralTransactionDTO>>(url);
            var pagedData = result.Object;

            DataTablesResponse response;
            if (pagedData == null) { response = DataTablesResponse.Create(request, default, default, default); }
            else
            {
                response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            }
            return new DataTablesJsonResult(response, true);
        }
        public async Task<IActionResult> AddGeneralTransaction(decimal amount, TransactionType transType, PayTypeDescription payTypeDisc, string notes, string date)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var Trandate = Convert.ToDateTime(date);

            var model = new GeneralTransactionDTO
            {
                TransactionAmount = amount,
                TransactionType = transType,
                PayTypeDiscription = payTypeDisc,
                TransDescription = notes,
                TransactionDate = Trandate
            };
            var response = await client.PostAsJsonAsync<GeneralTransactionDTO, bool>(Constants.ClientRoutes.AddGeneralTransaction, model);
            var item = response.Object;
            //var item = response.Object;

            if (response.ValidationErrors.Count > 0)
            {
                return View(item).WithWarning(response.Code + ": " + response.ShortDescription);
            }

            return RedirectToAction("GeneralTransaction");
        }
        public async Task<IActionResult> EditGeneralTransaction(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var response = await client.GetAsync<GeneralTransactionDTO>(string.Format(Constants.ClientRoutes.GetGeneralTransactionById, id));

            var item = response.Object;
            return View(item);
        }
        public async Task<IActionResult> PostEditGeneralTransaction(GeneralTransactionDTO model)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.EditGeneralTransaction, model.id);

            var result = await client.PutAsync<GeneralTransactionDTO, bool>(url, model);

            var item = result.Object;

            if (result.ValidationErrors.Count > 0)
            {
                return RedirectToAction("GeneralTransaction").WithWarning(result.Code + ": " + result.ShortDescription);
            }

            return RedirectToAction("GeneralTransaction");
        }
        public async Task<IActionResult> DeleteGeneralTransaction(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.DeactivateGeneralTransaction, id);

            var result = await client.DeleteAsync<bool>(url);

            var item = result.Object;

            if (result.ValidationErrors.Count > 0)
            {
                return RedirectToAction("GeneralTransaction").WithWarning(result.Code + ": " + result.ShortDescription);
            }

            return RedirectToAction("GeneralTransaction");
        }
        public async Task<IActionResult> PilotApprovalReport(DateTime? startDate = null, DateTime? endDate = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers.Object.Items, "Code", "Details");

            return View();
        }
        public async Task<IActionResult> GetPilotApprovalReport(string startdate, string enddate, string driver, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            DateTime? date = Convert.ToDateTime(startdate);
            DateTime? enddates = Convert.ToDateTime(enddate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            var model = new SearchDTO
            {
                StartDate = date.GetValueOrDefault(),
                EndDate = enddates.GetValueOrDefault(),
                Code = driver,
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetPilotApproval,
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<SearchDTO, PagedRecordModel<WalletTransactionDTO>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);

            return new DataTablesJsonResult(response, true);
        }
        public async Task<List<WalletTransactionDTO>> GetPilotApprovalDetails(DateTime startdate, DateTime enddate, string code)
        {
            var items = new List<WalletTransactionDTO>();
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var model = new SearchDTO
                {
                    StartDate = startdate,
                    EndDate = enddate,
                    Code = code
                };

                var response = await client.PostAsJsonAsync<SearchDTO, List<WalletTransactionDTO>>(Constants.ClientRoutes.GetPilotApprovalDetail, model);


                items = response.Object ?? new List<WalletTransactionDTO> { };


                if (response.ValidationErrors.Count() > 0)
                {
                    return items;
                }
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }

        }


        public async Task<IActionResult> GetExpense()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminals = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminals.Object.Items ?? new List<TerminalModel>(), "Id", "Name");

            return View();
        }

        public async Task<IActionResult> GetManifestReports(DateTime? startdate, DateTime? enddate, int departureTerminalId, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
            //var date = Convert.ToDateTime(startdate);
            //var enddates = Convert.ToDateTime(enddate).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            startdate = startdate.Value.Date;
            enddate = enddate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);

            var model = new DateModel
            {
                StartDate = startdate,
                EndDate = enddate,
                Id = departureTerminalId
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetManifestExpenses2,
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<DateModel, PagedRecordModel<ManifestDTO>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> PilotScheduleDetail(SearchDTO date)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var drivers = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            ViewBag.Drivers = new SelectList(drivers?.Object?.Items, "Id", "Details");

            return View();
        }

        public async Task<IActionResult> GetPilotsCapturedPayment(DateTime? startdate, DateTime? enddate, int WalletId, IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var date = Convert.ToDateTime(startdate);
            var enddates = Convert.ToDateTime(enddate);

            var model = new DateModel
            {
                StartDate = date,
                EndDate = enddates.AddHours(23),
                Id = WalletId,
            };

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.GetPilotsCapturedPayment,
                page, request.Length, request.Search.Value);

            var result = await client.PostAsJsonAsync<DateModel, PagedRecordModel<WalletTransactionDTO>>(url, model);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }
        [HttpGet]
        public async Task<IActionResult> RevertToCapturedTransaction(string code)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RevertToCapturedTransaction, code);

            var result = await client.GetAsync<bool>(url);

            return RedirectToAction("PilotApproval", new { code = code });
        }
        [HttpGet]
        public async Task<IActionResult> RevertToUncapturedTransaction(string code)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.RevertToUncapturedTransaction, code);

            var result = await client.GetAsync<bool>(url);

            return RedirectToAction("PilotApproval", new { code = code });
        }

        [HttpGet]
        public async Task<IActionResult> ChannelSales()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminalRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminalRequest.Object.Items ?? new List<TerminalModel>(), "Id", "Name");
            ViewBag.Channels = new SelectList(MvcUtilities.GenerateEnumSelectList(typeof(BookingType)), "Value", "Text");
            ViewBag.Status = new SelectList(MvcUtilities.GenerateEnumSelectList(typeof(BookingStatus)), "Value", "Text");

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Bookings()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminalRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminalRequest.Object.Items ?? new List<TerminalModel>(), "Id", "Name");
            ViewBag.Channels = new SelectList(MvcUtilities.GenerateEnumSelectList(typeof(BookingType)), "Value", "Text");
            ViewBag.Status = new SelectList(MvcUtilities.GenerateEnumSelectList(typeof(BookingStatus)), "Value", "Text");

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> AdvancedBooking()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var terminalRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            ViewBag.Terminals = new SelectList(terminalRequest.Object.Items ?? new List<TerminalModel>(), "Id", "Name");
            ViewBag.Status = new SelectList(MvcUtilities.GenerateEnumSelectList(typeof(BookingStatus)), "Value", "Text");

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> BookingsReport(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var search = JsonConvert.DeserializeObject<BookingSearchViewModel>(Convert.ToString(request.AdditionalParameters["data"]));
            var page = (request.Start / request.Length) + 1;

            search.PageIndex = page;
            search.Pagesize = request.Length;

            var url = string.Format("{0}?{1}", Constants.ClientRoutes.BookingSales, search.ToQueryString());
            var result = await client.GetAsync<BookingSalesSummaryDto<BookingReportViewModel>>(url);

            var pagedData = result.Object.Records;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items,
                new Dictionary<string, object>()
                {
                    ["summary"] = JsonConvert.SerializeObject(
                    new
                    {
                        TotalDiscountedSales = result.Object.TotalDiscountedSales,
                        TotalSales = result.Object.TotalSales
                    })
                });
            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public async Task<IActionResult> AdvancedBookingsReport(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var search = JsonConvert.DeserializeObject<AdvancedBookingSearchViewModel>(Convert.ToString(request.AdditionalParameters["data"]));
            var page = (request.Start / request.Length) + 1;

            search.PageIndex = page;
            search.Pagesize = request.Length;

            var url = string.Format("{0}?{1}", Constants.ClientRoutes.AdvancedBookingSales, search.ToQueryString());
            var result = await client.GetAsync<AdvancedBookingSalesSummaryDto<AdvancedBookingReportViewModel>>(url);

            var pagedData = result.Object.Records;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items,
                new Dictionary<string, object>()
                {
                    ["summary"] = JsonConvert.SerializeObject(new
                    {
                        result.Object.PosSales,
                        result.Object.CashSales,
                        result.Object.TotalSales
                    })
                });

            return new DataTablesJsonResult(response, true);
        }

        [HttpGet]
        public async Task<IActionResult> ChannelBookingsReport(ChannelSalesSearchViewModel request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format("{0}?{1}", Constants.ClientRoutes.BookingByChannelReports, request.ToQueryString());
            var result = await client.GetAsync<List<ChannelBookingSalesSummaryViewModel>>(url);

            var pagedData = result.Object;

            return Json(pagedData);
        }
    }
}