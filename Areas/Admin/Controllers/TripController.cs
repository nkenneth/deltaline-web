﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TripController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public TripController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Trips,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<TripModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);

        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.TripDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await FetchRequiredDropDowns();

            return View(new TripModel());
        }

        private async Task FetchRequiredDropDowns(int? selectedRouteId = null, int? selectedparentRouteId = null, int? selectedVehicleModelId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var routeRequest = await client.GetAsync<PagedRecordModel<RouteModel>>(Constants.ClientRoutes.Routes);
            var vehicleModelRequest = await client.GetAsync<PagedRecordModel<VehicleMModel>>(Constants.ClientRoutes.VehicleModels);

            ViewBag.RouteId = new SelectList(routeRequest.Object.Items, "Id", "Name", selectedRouteId);
            ViewBag.ParentRouteId = new SelectList(routeRequest.Object.Items, "Id", "Name", selectedparentRouteId);
            ViewBag.VehicleModelId = new SelectList(vehicleModelRequest.Object.Items, "Id", "Name", selectedVehicleModelId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TripModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<TripModel, bool>(Constants.ClientRoutes.TripCreate, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.RouteId, model.ParentRouteId, model.VehicleModelId);

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.TripGet, id);
            var result = await client.GetAsync<TripModel>(url);

            var model = result.Object;

            if (model != null) {
                await FetchRequiredDropDowns(model.RouteId, model.ParentRouteId, model.VehicleModelId);
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(Guid id, TripModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.TripUpdate, id);

                var result = await client.PutAsync<TripModel, bool>(url, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.RouteId, model.ParentRouteId, model.VehicleModelId);

            return View(model);
        }
    }
}