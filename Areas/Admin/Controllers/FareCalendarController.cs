﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("admin")]
    public class FareCalendarController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public FareCalendarController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.FareCalendars,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<FareCalendarModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);

        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.FareCalendarDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await FetchRequiredDropDowns();
            var model = new FareCalendarModel();
            return View(model);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FareCalendarModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<FareCalendarModel, bool>(Constants.ClientRoutes.FareCalendarCreate, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.RouteId);

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.FareCalendarGet, id);
            var result = await client.GetAsync<FareCalendarModel>(url);

            var model = result.Object;

            if (model != null)
            {
                await FetchRequiredDropDowns(model.RouteId);
                return View(model);
            }

            return NotFound();
        }
        private async Task FetchRequiredDropDowns(int? selectedRouteId = null, int? selectedVehicleModel = null, int? selectedTerminalId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var routeRequest = await client.GetAsync<PagedRecordModel<RouteModel>>(Constants.ClientRoutes.Routes);
            var vehicleModelsRequest = await client.GetAsync<PagedRecordModel<VehicleMModel>>(Constants.ClientRoutes.VehicleModels);
            var terminalRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);

            ViewBag.TerminalId = new SelectList(terminalRequest.Object.Items, "Id", "Name", selectedTerminalId);

            ViewBag.VehicleModelId = new SelectList(vehicleModelsRequest.Object.Items, "Id", "Name", selectedVehicleModel);

            ViewBag.RouteId = new SelectList(routeRequest.Object.Items, "Id", "Name", selectedRouteId);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, FareCalendarModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.FareCalendarUpdate, id);

                var result = await client.PutAsync<FareCalendarModel, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            await FetchRequiredDropDowns(model.RouteId, model.VehicleModelId, model.TerminalId);

            return View(model);
        }
    }
}
