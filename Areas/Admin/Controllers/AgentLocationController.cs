﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AgentLocationController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public AgentLocationController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", $"{Constants.ClientRoutes.AgentLocation}/get",
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<AgentLocationDTO>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);

        }

        [HttpGet]
        public IActionResult Create()
        {
            //await FetchRequiredDropDowns();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AgentLocationDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<AgentLocationDTO, bool>($"{Constants.ClientRoutes.AgentLocation}/add", model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            //await FetchRequiredDropDowns(model.StateId);
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = $"{Constants.ClientRoutes.AgentLocation}/get/{id}";
            var result = await client.GetAsync<AgentLocationDTO>(url);

            var model = result.Object;

            if (model != null)
            {
                //await FetchRequiredDropDowns(model.StateId);
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, AgentLocationDTO model)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = $"{Constants.ClientRoutes.AgentLocation}/update/{id}";

                var result = await client.PutAsync<AgentLocationDTO, bool>(url, model);

                if (result.IsValid && result.Object)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    foreach (var item in result.ValidationErrors)
                    {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }
            //await FetchRequiredDropDowns(model.StateId);

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = $"{Constants.ClientRoutes.AgentLocation}/delete/{id}";

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetCommissions()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var commission = await client.GetAsync<AgentCommissionDTO>($"{Constants.ClientRoutes.AgentCommission}/getCommission");

            var result = commission.Object;

            if (commission.ValidationErrors.Count > 0)
            {
                return View(result).WithWarning(commission.Code + ": " + commission.ShortDescription);
            }

            return View(result).WithSuccess(commission.Code + ": " + commission.ShortDescription);
        }
        [HttpGet]
        public async Task<IActionResult> UpdateCommission()
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var commission = await client.GetAsync<AgentCommissionDTO>($"{Constants.ClientRoutes.AgentCommission}/getCommission");

            var result = commission.Object;

            if (commission.ValidationErrors.Count > 0)
            {
                return View(result).WithWarning(commission.Code + ": " + commission.ShortDescription);
            }

            return View(result).WithSuccess(commission.Code + ": " + commission.ShortDescription);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateCommission(AgentCommissionDTO comm)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var commission = await client.PutAsync<AgentCommissionDTO, bool>($"{Constants.ClientRoutes.AgentCommission}/Update/{comm.Id}", comm);
                       
            if (commission.ValidationErrors.Count > 0)
            {
                return View(comm).WithWarning(commission.Code + ": " + commission.ShortDescription);
            }

            return RedirectToAction("GetCommissions");
        }
    }
}