﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class VehicleController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public VehicleController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var page = (request.Start / request.Length) + 1;
            var url = string.Format("{0}/{1}/{2}/{3}", Constants.ClientRoutes.Vehicles,
                page, request.Length, request.Search.Value);

            var result = await client.GetAsync<PagedRecordModel<VehicleModel>>(url);

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VehicleDelete, id);

            var result = await client.DeleteAsync<bool>(url);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await FetchRequiredDropDowns();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VehicleModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var result = await client.PostAsJsonAsync<VehicleModel, bool>(Constants.ClientRoutes.VehicleCreate, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            await FetchRequiredDropDowns(model.LocationId, model.VehicleModelId, model.DriverId, model.FranchizeId);

            return View(model);
        }

        private async Task FetchRequiredDropDowns(
            int? selectedLocationId = null,
            int? selectedVehicleModelId = null,
            int? selectedDriverId = null,
            int? selectedFranchiseId = null)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var routeRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            var vehicleModelRequest = await client.GetAsync<PagedRecordModel<VehicleMModel>>(Constants.ClientRoutes.VehicleModels);
            var driverRequest = await client.GetAsync<PagedRecordModel<DriverModel>>(Constants.ClientRoutes.Drivers);
            var FranchiseRequest = await client.GetAsync<PagedRecordModel<FranchiseModel>>(Constants.ClientRoutes.Franchises);

            ViewBag.LocationId = new SelectList(routeRequest.Object.Items, "Id", "Name", selectedLocationId);
            ViewBag.VehicleModelId = new SelectList(vehicleModelRequest.Object.Items, "Id", "Name", selectedVehicleModelId);
            ViewBag.DriverId = new SelectList(driverRequest.Object.Items, "Id", "Name", selectedDriverId);
            ViewBag.FranchiseId = new SelectList(FranchiseRequest.Object.Items, "Id", "FirstName", selectedFranchiseId);
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = string.Format(Constants.ClientRoutes.VehicleGet, id);
            var result = await client.GetAsync<VehicleModel>(url);

            var model = result.Object;

            if (model != null) {
                await FetchRequiredDropDowns(model.LocationId, model.VehicleModelId, model.DriverId, model.FranchizeId);
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, VehicleModel model)
        {
            if (ModelState.IsValid) {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);

                var url = string.Format(Constants.ClientRoutes.VehicleUpdate, id);

                var result = await client.PutAsync<VehicleModel, bool>(url, model);

                if (result.IsValid && result.Object) {
                    return RedirectToAction("index");
                }
                else {
                    foreach (var item in result.ValidationErrors) {
                        ModelState.AddModelError(item.Key, string.Join(",", item.Value));
                    }

                    ModelState.AddModelError("", result.ShortDescription);
                }
            }

            await FetchRequiredDropDowns(model.LocationId, model.VehicleModelId, model.DriverId, model.FranchizeId);

            return View(model);
        }
    }
}