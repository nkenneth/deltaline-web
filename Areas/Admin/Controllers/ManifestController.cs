﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;
using static Transport.WEB.Utils.Constants;

namespace Transport.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ManifestController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ManifestController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<List<PassengerViewModel>> GetManifestByVehicleTripId(Guid id)
        {
            var items = new List<PassengerViewModel>();
            try
            {
                var client = _httpClientFactory.CreateClient(Constants.ClientWithToken);
                var url = $"{ClientRoutes.ManifestByVehicleTrip}/{id}";
                var response = await client.GetAsync<List<PassengerViewModel>>(url);
                items = response.Object ?? new List<PassengerViewModel> { };
                if (response.ValidationErrors.Count() > 0)
                {
                    return items;
                }
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }

        }

    }
}
