﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Net.Http;
using System.Threading.Tasks;
using Transport.WEB.Models;
using Transport.WEB.Utils;
using Transport.WEB.Utils.Alerts;

namespace Transport.WEB.Areas.Admin.Controllers
{

    [Area("Admin")]
    public class VehicleAllocationDetailController : Controller
    {
        private readonly IHttpClientFactory _HttpClientFactory;

        public VehicleAllocationDetailController(IHttpClientFactory HttpClientFactory)
        {
            _HttpClientFactory = HttpClientFactory;
        }


        public async Task<IActionResult> Index()
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);
            var routeRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            var vehicleRequest = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.Vehiclesdropdown);
            var drivers = await client.GetAsync<List<DriverModel>>(Constants.ClientRoutes.GetAvailableDriverAsync);
            ViewBag.LocationId = new SelectList(routeRequest.Object.Items, "Id", "Name");
            ViewBag.VehicleId = new SelectList(vehicleRequest.Object, "Id", "RegistrationNumber");
            ViewBag.AllDrivers = new SelectList(drivers.Object, "Id", "Name");

            var LTermId = HttpContext.Session.GetString("TerminalID");
            var terminalName = await client.GetAsync<TerminalModel>(string.Format(Constants.ClientRoutes.TerminalGet, LTermId));
            var temNm = terminalName.Object;
            int vLTermId = Convert.ToInt32(LTermId);
            ViewData["TermName"] = vLTermId;
            TempData["TermNameB"] = temNm.Name;
            ViewBag.TerminalId = vLTermId;
            return View();
        }



        [HttpGet]
        public async Task<IActionResult> GetData(IDataTablesRequest request)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);
            var LocationId = HttpContext.Session.GetString("TerminalID");
            var page = (request.Start / request.Length) + 1;

            var result = await client.GetAsync<PagedRecordModel<VehicleModel>>($"{Constants.ClientRoutes.VechicleToAllocate}/GetVehiclesByTerminalHeader/{LocationId}");

            var pagedData = result.Object;

            var response = DataTablesResponse.Create(request, pagedData.Count, pagedData.TotalItemCount, pagedData.Items);
            return new DataTablesJsonResult(response, true);
        }



        [HttpPost]
        public async Task<IActionResult> AllocateBuses(AllocationVehicleModel query, IFormCollection form)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);

            if (!ModelState.IsValid)
                return Redirect("Index");

            string strDDLValue = form["RegistrationNumber"];
            var LTermId = HttpContext.Session.GetString("TerminalID");
            int vLTermId = Convert.ToInt32(LTermId);
            query.LocationId = vLTermId;
            query.RegistrationNumber = strDDLValue as string;

            var url = string.Format(Constants.ClientRoutes.Allocatebuses);
            var response = await client.PutAsync<AllocationVehicleModel, bool>(url, query);

            var items = response.Object;
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View("Index", items);

        }

       [HttpGet]
       public async Task <bool> Confirm(VehicleAllocationConfirmation vehicle)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);

            var url = await client.GetAsync<bool>(string.Format(Constants.ClientRoutes.VehicleAllocationConfirm, vehicle.VehicleId));

            var item = url.Object;

            return item;
        }


        [HttpPost]
        public async Task<IActionResult> AllocateMultipleBuses(AllocationVehicleModel query, IFormCollection form)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);

            if (!ModelState.IsValid)
                return Redirect("Index");

            string strDDLValue = form["RegistrationNumber"];
            string TermId = form["Name"];
            int vLTermId = Convert.ToInt32(TermId);

            query.LocationId = vLTermId;
            query.RegistrationNumber = strDDLValue.ToString();
    
            var url = string.Format(Constants.ClientRoutes.Allocatebuses);
            var response = await client.PutAsync<AllocationVehicleModel, bool>(url, query);

            var items = response.Object;
            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
            return View("Index", items);
        }

        private async Task FetchRequiredDropDowns(
        int? selectedLocationId = null,
        int? selectedVehicleId = null)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);

            var routeRequest = await client.GetAsync<PagedRecordModel<TerminalModel>>(Constants.ClientRoutes.Terminals);
            var vehicleRequest = await client.GetAsync<List<VehicleModel>>(Constants.ClientRoutes.Vehiclesdropdown);

            ViewBag.LocationId = new SelectList(routeRequest.Object.Items, "Id", "Name", selectedLocationId);
            ViewBag.VehicleModelId = new SelectList(vehicleRequest.Object, "Id", "RegistrationNumber");
        }

        //public async Task<IActionResult> Index()
        //{
        //    await GetData();
        //    return View();
        //}


        //public async Task<IActionResult> GetData()
        //{
        //    ViewData["message"] = TempData["Tget"].ToString();        
        //    int LocationId = Convert.ToInt32(ViewData["message"]);           
        //    var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);
        //    var response = await client.GetAsync<List<VehicleModel>>($"{Constants.ClientRoutes.VechicleToAllocate}/GetVehiclesByTerminalHeader/{LocationId}");        
        //    return View("Index", response.Object);
        //}

        [HttpPost]
        public async Task<IActionResult> Updatedriver(ChangeDriverDTO changeDriver)
        {
            var client = _HttpClientFactory.CreateClient(Constants.ClientWithToken);

            var vehicle = await client.GetAsync<VehicleModel>(string.Format(Constants.ClientRoutes.VehicleByRegNum, changeDriver.RegistrationNumber));
            //var driver = await client.GetAsync<DriverModel>(string.Format(Constants.ClientRoutes.DriverGet, driverId));

            var newDriverDetails = new VehicleModel
            {
                Id = vehicle.Object.Id,
                RegistrationNumber = vehicle.Object.RegistrationNumber,
                ChasisNumber = vehicle.Object.ChasisNumber,
                EngineNumber = vehicle.Object.EngineNumber,
                VehicleModelId = vehicle.Object.VehicleModelId,
                VehicleStatus = vehicle.Object.VehicleStatus,
                DriverId = changeDriver.DriverId,
                LocationId = vehicle.Object.LocationId,
                FranchizeId = vehicle.Object.FranchizeId,
                DriverName = changeDriver.DriverName
            };

            var response = await client.PutAsync<VehicleModel, bool>(string.Format(Constants.ClientRoutes.VehicleUpdate, vehicle.Object.Id), newDriverDetails);

            var items = response.Object;

            if (response.ValidationErrors.Count() > 0)
            {
                return View(items).WithWarning(response.Code + ": " + response.ShortDescription);
            }
    
            return RedirectToAction("Index");
        }

    }
}